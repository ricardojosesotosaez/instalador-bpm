import React from 'react'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { withStyles} from '@material-ui/core/styles'
import axios from 'axios'


const styles = theme => ({
    input: {
        display: 'none',
    },   
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
        marginLeft: theme.spacing.unit,
      },      
      iconSmall: {
        // fontSize: 20,
      },
});

const ESTABLECE_CONEXION = 'Establecer conexión'
const CAMBIA_COLOR_PRIMARY = 'primary'
const CAMBIA_COLOR_SECONDARY = 'secondary'
const CONEXION_EXITOSA = 'Conexión exitosa'
const CONEXION_ERROR = 'Error conexión'

export default withStyles(styles)(class extends React.Component {
    constructor(props){
    super(props);
    this.state = {
        hostname: '',
        database: '',
        username: '',
        password: '',
        lblsubmit: ESTABLECE_CONEXION,
        colorsubmit: CAMBIA_COLOR_PRIMARY,
        isEnabledBtnInstall: false,
        disabledBtnConnection: false         
    }    
    
    this.handleChange = this.handleChange.bind(this);
}

    handleChange = name => ({ target: {value} }) => {
            
        this.setState({
            [name]: value,            
            lblsubmit: ESTABLECE_CONEXION,
            colorsubmit: CAMBIA_COLOR_PRIMARY,            
            disabledBtnConnection: false
        })      

        if (this.state.isEnabledBtnInstall) {
            this.props.disableAction()
            this.setState({isEnabledBtnInstall: false}) 
        }
    }

    submit = event => {
        event.preventDefault();

        var data = {
            hostname: this.state.hostname,
            database: this.state.database,
            username: this.state.username,
            password: this.state.password
        };

                      
        axios({
            url: 'https://localhost:5001/v1/ValidarDatabase',
            method: 'POST',
            data: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json; charset=utf-8'
            }
        })
        .then(response => {
            console.log(response);
            
            if (response.statusText === 'OK') {     
                
                this.setState({
                    lblsubmit: CONEXION_EXITOSA,
                    colorsubmit: CAMBIA_COLOR_SECONDARY,
                    isEnabledBtnInstall: true,
                    disabledBtnConnection: true
                });      
                this.props.disableAction()   
                this.props.throwMessage('La conexión ha sido realizada correctamente', 'success')       
            }

            })
        .catch(error => {
            this.setState({
                colorsubmit: CAMBIA_COLOR_PRIMARY,
                lblsubmit: CONEXION_ERROR,
                disabledBtnConnection: true
            })
            if (error.response.status === 401){
                this.props.throwMessage('Las credenciales no son válidas', 'error')
            }else
            {
                this.props.throwMessage(error.message, 'error')
            }
            
        });
      };

     
    render() {
        const { classes } = this.props;
        return <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Configuración de base de datos
            </Typography>
            <Typography variant="subtitle2" gutterBottom>
                Asegúrese que la base de datos exista
            </Typography>

            <form className={classes.container} autoComplete="off" onSubmit={this.submit.bind(this)}>
                <Grid container spacing={24}>
                    <Grid item xs={12} md={6}>
                        <TextField  required id="hostname" label="Hostname" fullWidth onChange={this.handleChange('hostname')}
                            helperText="Si estás trabajando localmente, probablemente su hostname es localhost"
                            
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField required id="database" label="Database" fullWidth onChange={this.handleChange('database')}
                            helperText="Asegúrese que la base de datos este en blanco"                                                        
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField required  id="username" label="Username" fullWidth onChange={this.handleChange('username')} />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField required type="Password" id="password" label="Password" fullWidth onChange={this.handleChange('password')} />
                    </Grid>
                    <Grid item xs={12}>
                        <Button type="submit" disabled={this.state.disabledBtnConnection} variant="contained" size="small" color={this.state.colorsubmit} className={classes.Button}>    
                            {this.state.lblsubmit} 
                            {/* <Done className={classNames(classes.leftIcon, classes.iconSmall)} /> */}
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </React.Fragment>
    }
})