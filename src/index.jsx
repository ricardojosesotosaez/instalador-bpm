import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import MainForm from './containers/mainForm';
import { createMuiTheme} from '@material-ui/core/styles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { red, green, blue } from '@material-ui/core/colors';
import { SnackbarProvider, withSnackbar } from 'notistack';

const theme = createMuiTheme({
    palette: {
        primary: blue,
        secondary: green,
        error: red,
        contrastThreshold: 3,
        tonalOffset: 0.2,
    }
});


class App extends Component {
  render() {
    return (
      <div>
        <MuiThemeProvider theme={theme}>
          <SnackbarProvider maxSnack={3}>
              <MainForm/>
          </SnackbarProvider>
        </MuiThemeProvider>
      </div>
    )
  }
}


ReactDOM.render(<App/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
