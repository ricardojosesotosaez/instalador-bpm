import React, {Fragment } from 'react'
import EntryForm from '../components/entryForm'
import Installer from "../components/installer";
import { AppBar, CssBaseline, Typography, Paper, Stepper, Step, StepLabel, Button, withStyles} from '@material-ui/core';
import { PropTypes } from 'prop-types';
import classNames from 'classnames';
import { withSnackbar } from 'notistack';


const styles = theme => ({
    appBar: {
      position: 'relative',
    },  
    layout: {
      width: 'auto',
      marginLeft: theme.spacing.unit * 2,
      marginRight: theme.spacing.unit * 2,
      [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
        width: 600,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 3,
      marginBottom: theme.spacing.unit * 3,
      padding: theme.spacing.unit * 2,
      [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
        marginTop: theme.spacing.unit * 6,
        marginBottom: theme.spacing.unit * 6,
        padding: theme.spacing.unit * 3,
      },
    },
    stepper: {
      padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 5}px`,             
    },
  
    buttons: {
      display: 'flex',
      justifyContent: 'flex-end',      
    },
    cssButton: { 
      padding: '6x 12px',
      marginLeft:5,
    },
  });
  

const steps = ['Base de datos', 'Validar información', 'Instalar BPM']

function getStepContent(step, handler, tmessage) {
    switch (step) {
        case 0:
          return <EntryForm disableAction={handler} throwMessage={tmessage}  /> 
        case 1:
          return <Installer/>;
        case 2:
          return <div>Instalar</div>
        default:
          throw new Error('Unknown step');
      }
}

class mainForm extends React.Component {
  constructor(props){
    super(props)

    this.state = {
        activeStep: 0,
        disabled: true
    };

}

    handleNext = () => {
        this.setState(state => ({
            activeStep: state.activeStep + 1
        }))        
    }

    handleback = () => {
        this.setState(state => ({
            activeStep: state.activeStep - 1
        }))        
    }

    handleReset = () => {
        this.setState({
          activeStep: 0,
        });
      };

    handleDisabled() {
      this.setState({
         disabled: false
      });
    };

    handleThrowMessage(message, variant){
      // variant could be success, error, warning or info
      this.props.enqueueSnackbar(message,{ variant });
    };

    

  render() {

    const { classes } = this.props;
    const { activeStep } = this.state;

    return (
      <React.Fragment>
          <CssBaseline/>
            <AppBar position="absolute" color="default" className={classes.appBar}>
                <toolbar>
                    <Typography variant="h6" color="inherit" noWrap>
                        Instalador Sonda BPM
                    </Typography>
                </toolbar>
            </AppBar>
            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography component="h1" variant="h4" align="center">
                        Sonda BPM
                    </Typography>
                    <Stepper activeStep={activeStep} className={classes.stepper}>
                        {steps.map(label => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    <React.Fragment>
                        {activeStep === steps.length ? (
                            <Fragment>
                                <Typography variant="h5" gutterBottom>
                                    La instalación a finalizado
                                </Typography>
                            </Fragment>
                        ) : (
                            <Fragment>
                                {getStepContent(activeStep, this.handleDisabled.bind(this), this.handleThrowMessage.bind(this))}
                                 
                                <div className={classes.buttons}>
                                    {/* {activeStep !== 0 && (
                                        <Button size="small" onClick={this.handleback} className={classes.Button}>
                                            Volver
                                        </Button>            
                                    )} */}
                                    <Button disabled={this.state.disabled} variant="contained" size="small" color="primary" onClick={this.handleNext} className={classNames(classes.cssButton)}>
                                        Instalar {/* {activeStep === steps.length - 1 ? '' : 'Instalar' } */}
                                    </Button>
                                </div>
                            </Fragment>
                        )}          
                    </React.Fragment>
                </Paper>
            </main>
      </React.Fragment>
    )
  }
}


mainForm.prototypes = {
    classes: PropTypes.object.isRequired,
    enqueueSnackbar: PropTypes.func.isRequired,
}

export default withStyles(styles) (
  withSnackbar(mainForm),
);


