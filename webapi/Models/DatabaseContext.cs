﻿using Microsoft.EntityFrameworkCore;
using webapi.Services;
using System.Collections.Generic;
using System.Linq;
using System;

namespace webapi.Models
{
    public class DatabaseContext
    {

        public static Dictionary<string, string> _connectionstring;

        public static void SetConnectionString(Dictionary<string, string> connStr)
        {
            _connectionstring = connStr;
        }

        public DbSet<Model.UsuUsuarios> usuarios { get; set; }
        public DbSet<Model.UsuRoles> roles { get; set; }
        public DbSet<Model.UsuUsuariosRoles> usuroles { get; set; }
        public DbSet<Model.UsuFuncionalidades> funcionalidades { get; set; }    
        public DbSet<Model.UsuFuncionalidadesRoles> funcionalidadesroles { get; set; }

        public static DbContext Create(string connid)
        {

            if (!string.IsNullOrEmpty(connid))
            {
                var connStr = _connectionstring[connid];
                var optionsBuilder = new DbContextOptionsBuilder<DbContext>();
                optionsBuilder.UseSqlServer(connStr);
                return new DbContext(optionsBuilder.Options);
            }
            else
            {
                throw new ArgumentNullException("ConnectionId");
            }

        }


    }
}
