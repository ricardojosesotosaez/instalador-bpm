﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflUnidad
    {
        public WflUnidad()
        {
            WflUnidadAtrEntidad = new HashSet<WflUnidadAtrEntidad>();
            WflUnidadUsuario = new HashSet<WflUnidadUsuario>();
        }

        public Guid IdUnidad { get; set; }
        public Guid? IdPadre { get; set; }
        public Guid? IdJerarquia { get; set; }
        public string Nombre { get; set; }
        public int Orden { get; set; }
        public string CodUnidad { get; set; }

        public virtual WflJerarquia IdJerarquiaNavigation { get; set; }
        public virtual ICollection<WflUnidadAtrEntidad> WflUnidadAtrEntidad { get; set; }
        public virtual ICollection<WflUnidadUsuario> WflUnidadUsuario { get; set; }
    }
}
