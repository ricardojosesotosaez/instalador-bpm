﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflFlecha
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Flechaid { get; set; }
        public string Nombre { get; set; }
        public int? TareaidOrigen { get; set; }
        public string DiagidDestino { get; set; }
        public int? TareaidDestino { get; set; }
        public int Tipo { get; set; }
        public string IdExterno { get; set; }

        public virtual WflDiag WflDiag { get; set; }
        public virtual WflTarea WflTarea { get; set; }
        public virtual WflTarea WflTareaNavigation { get; set; }
        public virtual WflFlechaalarma WflFlechaalarma { get; set; }
        public virtual WflFlechaflujo WflFlechaflujo { get; set; }
    }
}
