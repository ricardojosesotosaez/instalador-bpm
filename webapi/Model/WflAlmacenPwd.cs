﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflAlmacenPwd
    {
        public string Aplicacion { get; set; }
        public string Usuarioid { get; set; }
        public string Usuarioappid { get; set; }
        public string Password { get; set; }
        public string Servidor { get; set; }
        public string Estado { get; set; }
        public DateTime? FecCambEstado { get; set; }
    }
}
