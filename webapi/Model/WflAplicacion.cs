﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflAplicacion
    {
        public string Aplicacion { get; set; }
        public string Region { get; set; }
        public string Claseauth { get; set; }
        public int SincUsu { get; set; }
        public int CrearUsu { get; set; }
        public string PeriodSincro { get; set; }
        public DateTime? UltimaSincro { get; set; }
    }
}
