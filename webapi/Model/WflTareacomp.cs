﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflTareacomp
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Tareaid { get; set; }
        public string IdRol { get; set; }
        public int? Modo { get; set; }
        public int Todos { get; set; }

        public virtual WflTarea WflTarea { get; set; }
    }
}
