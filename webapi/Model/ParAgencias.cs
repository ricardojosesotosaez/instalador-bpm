﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class ParAgencias
    {
        public short IdAdm { get; set; }
        public short CodAgencia { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public int CodComuna { get; set; }
        public int Folioreclamo650 { get; set; }
        public short CodSubgerencia { get; set; }
        public string EstadoReg { get; set; }
        public DateTime FecEstadoReg { get; set; }
        public DateTime FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public string HorarioAtencion { get; set; }
        public int? CodZona { get; set; }
    }
}
