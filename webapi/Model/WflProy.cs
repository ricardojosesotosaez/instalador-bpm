﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflProy
    {
        public WflProy()
        {
            WflAlarma = new HashSet<WflAlarma>();
            WflCopiasporpersona = new HashSet<WflCopiasporpersona>();
            WflDiag = new HashSet<WflDiag>();
        }

        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Nombre { get; set; }
        public DateTime Fecha { get; set; }
        public string Creador { get; set; }
        public string Observaciones { get; set; }

        public virtual ICollection<WflAlarma> WflAlarma { get; set; }
        public virtual ICollection<WflCopiasporpersona> WflCopiasporpersona { get; set; }
        public virtual ICollection<WflDiag> WflDiag { get; set; }
    }
}
