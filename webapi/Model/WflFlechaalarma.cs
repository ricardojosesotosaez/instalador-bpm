﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflFlechaalarma
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Flechaid { get; set; }
        public string Rolid { get; set; }
        public int Plazoalarma { get; set; }
        public string DiagidAlarmado { get; set; }

        public virtual WflFlecha WflFlecha { get; set; }
    }
}
