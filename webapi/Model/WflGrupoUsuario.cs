﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflGrupoUsuario
    {
        public Guid IdGrupoUsuario { get; set; }
        public Guid IdGrupo { get; set; }
        public short IdAdm { get; set; }
        public string IdUsuario { get; set; }
        public int Lider { get; set; }

        public virtual UsuUsuarios Id { get; set; }
        public virtual WflGrupo IdGrupoNavigation { get; set; }
    }
}
