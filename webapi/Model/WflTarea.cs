﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflTarea
    {
        public WflTarea()
        {
            WflCopiaexp = new HashSet<WflCopiaexp>();
            WflFlechaWflTarea = new HashSet<WflFlecha>();
            WflFlechaWflTareaNavigation = new HashSet<WflFlecha>();
            WflTareaRegion = new HashSet<WflTareaRegion>();
            WflTareacomp = new HashSet<WflTareacomp>();
        }

        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Tareaid { get; set; }
        public string Proposito { get; set; }
        public string Nombre { get; set; }
        public string Rolid { get; set; }
        public int Tipo { get; set; }
        public string Coordenadas { get; set; }
        public string Procesonegocio { get; set; }
        public string Modulo { get; set; }
        public string IdExterno { get; set; }
        public int? Subtipo { get; set; }
        public int? Prioridad { get; set; }
        public string CentroCosto { get; set; }
        public string FechaVenc { get; set; }
        public string Nota { get; set; }
        public string Referencia1 { get; set; }
        public string Referencia2 { get; set; }
        public string Referencia3 { get; set; }
        public int? Esreasignable { get; set; }
        public string Asunto { get; set; }
        public string CambioPrioridad { get; set; }
        public int TareaidPadre { get; set; }
        public int Adjunto { get; set; }

        public virtual WflDiag WflDiag { get; set; }
        public virtual WflTareaconector WflTareaconector { get; set; }
        public virtual WflTareaescucha WflTareaescucha { get; set; }
        public virtual WflTareageneral WflTareageneral { get; set; }
        public virtual ICollection<WflCopiaexp> WflCopiaexp { get; set; }
        public virtual ICollection<WflFlecha> WflFlechaWflTarea { get; set; }
        public virtual ICollection<WflFlecha> WflFlechaWflTareaNavigation { get; set; }
        public virtual ICollection<WflTareaRegion> WflTareaRegion { get; set; }
        public virtual ICollection<WflTareacomp> WflTareacomp { get; set; }
    }
}
