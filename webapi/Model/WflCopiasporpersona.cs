﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflCopiasporpersona
    {
        public string Personaid { get; set; }
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public int Numcopias { get; set; }
        public string EstadoReg { get; set; }
        public DateTime FecEstadoReg { get; set; }
        public DateTime FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual WflProy WflProy { get; set; }
    }
}
