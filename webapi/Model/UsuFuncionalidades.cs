﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuFuncionalidades
    {
        public UsuFuncionalidades()
        {
            UsuFuncionalidadesRoles = new HashSet<UsuFuncionalidadesRoles>();
            UsuMenusFuncionalidades = new HashSet<UsuMenusFuncionalidades>();
            UsuRecursoFuncionalidades = new HashSet<UsuRecursoFuncionalidades>();
            UsuUserControlFuncionalidad = new HashSet<UsuUserControlFuncionalidad>();
        }

        public string IdFuncionalidad { get; set; }
        public string IdProcesoNegocio { get; set; }
        public string Descripcion { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual UsuProcesosnegocio IdProcesoNegocioNavigation { get; set; }
        public virtual ICollection<UsuFuncionalidadesRoles> UsuFuncionalidadesRoles { get; set; }
        public virtual ICollection<UsuMenusFuncionalidades> UsuMenusFuncionalidades { get; set; }
        public virtual ICollection<UsuRecursoFuncionalidades> UsuRecursoFuncionalidades { get; set; }
        public virtual ICollection<UsuUserControlFuncionalidad> UsuUserControlFuncionalidad { get; set; }
    }
}
