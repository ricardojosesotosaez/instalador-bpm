﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflAlarma
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public int Alarmaid { get; set; }
        public DateTime Fechainicial { get; set; }
        public DateTime Fechafinal { get; set; }
        public DateTime Fechafinalposible { get; set; }
        public string Diagid { get; set; }
        public int Tareaid { get; set; }
        public string Llaveexpid { get; set; }
        public int? Copiaexpid { get; set; }
        public int? Flechaid { get; set; }
        public int? Activada { get; set; }
        public string EstadoReg { get; set; }
        public DateTime FecEstadoReg { get; set; }
        public DateTime FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual WflProy WflProy { get; set; }
    }
}
