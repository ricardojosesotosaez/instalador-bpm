﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflSubprocesoAPublicar
    {
        public int Idsubpub { get; set; }
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public string IdExterno { get; set; }
        public string Nombre { get; set; }
        public string Estado { get; set; }
        public string Origen { get; set; }
        public string DescArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public DateTime FecCambioEstado { get; set; }
        public DateTime FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual WflSubprocesoAPublicarExt WflSubprocesoAPublicarExt { get; set; }
    }
}
