﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflAtributos
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Llaveexpid { get; set; }
        public int Copiaexpid { get; set; }
        public string TipoDoc { get; set; }
        public int? NumDoc { get; set; }
        public string Entidad { get; set; }
        public decimal? Monto { get; set; }
        public string CentroCosto { get; set; }
        public string Referencia1 { get; set; }
        public string Referencia2 { get; set; }
        public string Referencia3 { get; set; }
        public int? Prioridad { get; set; }
        public string Region { get; set; }
        public string AtributosXml { get; set; }
        public DateTime? Fecha1 { get; set; }
        public DateTime? Fecha2 { get; set; }
        public string Asunto { get; set; }
        public DateTime? FecPrioridad { get; set; }
        public int? PrioridadNueva { get; set; }
        public DateTime FecSeguimiento { get; set; }
        public string Indice { get; set; }
        public Guid Uid { get; set; }
        public string IndiceComp { get; set; }

        public virtual WflCopiaexp WflCopiaexp { get; set; }
    }
}
