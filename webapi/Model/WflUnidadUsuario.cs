﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflUnidadUsuario
    {
        public Guid IdUnidadUsuario { get; set; }
        public Guid IdUnidad { get; set; }
        public string IdUsuario { get; set; }
        public short? IdAdm { get; set; }

        public virtual UsuUsuarios Id { get; set; }
        public virtual WflUnidad IdUnidadNavigation { get; set; }
    }
}
