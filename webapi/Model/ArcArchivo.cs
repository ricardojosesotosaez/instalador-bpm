﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class ArcArchivo
    {
        public Guid IdArchivo { get; set; }
        public int IdDirectorioPadre { get; set; }
        public string Notas { get; set; }
        public string Nombre { get; set; }
        public DateTime FecUltModificacion { get; set; }
        public DateTime FecCreacion { get; set; }
        public string UsuUltModificacion { get; set; }
        public string UsuCreacion { get; set; }
        public string UsuPropietario { get; set; }
        public long Tamano { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public int? Idrepositorio { get; set; }

        public virtual ArcDirectorio IdDirectorioPadreNavigation { get; set; }
    }
}
