﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflOpcion
    {
        public string IdOpcion { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public int Eseditable { get; set; }
    }
}
