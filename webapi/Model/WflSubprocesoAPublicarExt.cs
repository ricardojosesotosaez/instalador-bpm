﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflSubprocesoAPublicarExt
    {
        public int Idsubpub { get; set; }
        public string Diagrama { get; set; }
        public string ArchivoXpdl { get; set; }

        public virtual WflSubprocesoAPublicar IdsubpubNavigation { get; set; }
    }
}
