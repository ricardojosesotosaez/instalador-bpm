﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuProcesosnegocio
    {
        public UsuProcesosnegocio()
        {
            UsuFuncionalidades = new HashSet<UsuFuncionalidades>();
            UsuRoles = new HashSet<UsuRoles>();
        }

        public string IdProcesoNegocio { get; set; }
        public string Descripcion { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual ICollection<UsuFuncionalidades> UsuFuncionalidades { get; set; }
        public virtual ICollection<UsuRoles> UsuRoles { get; set; }
    }
}
