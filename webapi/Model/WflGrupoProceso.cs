﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflGrupoProceso
    {
        public Guid IdGrupoProceso { get; set; }
        public Guid IdGrupo { get; set; }
        public int Procesoid { get; set; }

        public virtual WflGrupo IdGrupoNavigation { get; set; }
        public virtual WflProc Proceso { get; set; }
    }
}
