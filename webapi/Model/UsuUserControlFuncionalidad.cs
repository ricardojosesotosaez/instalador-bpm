﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuUserControlFuncionalidad
    {
        public string NomUserControl { get; set; }
        public string IdFuncionalidad { get; set; }
        public short Inicial { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public string Modo { get; set; }

        public virtual UsuFuncionalidades IdFuncionalidadNavigation { get; set; }
        public virtual UsuUserControl NomUserControlNavigation { get; set; }
    }
}
