﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuSesiones
    {
        public short IdAdm { get; set; }
        public string IdUsuario { get; set; }
        public decimal IdSesion { get; set; }
        public DateTime FecInicio { get; set; }
        public DateTime? FecFin { get; set; }
        public string DireccionIp { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public string Token { get; set; }
        public string AspnetSessionid { get; set; }
    }
}
