﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuFuncionalidadesRoles
    {
        public short IdAdm { get; set; }
        public string IdFuncionalidad { get; set; }
        public string IdRol { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual UsuRoles Id { get; set; }
        public virtual UsuFuncionalidades IdFuncionalidadNavigation { get; set; }
    }
}
