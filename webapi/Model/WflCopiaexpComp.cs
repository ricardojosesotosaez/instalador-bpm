﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflCopiaexpComp
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Llaveexpid { get; set; }
        public int Copiaexpid { get; set; }
        public string Personaid { get; set; }
        public int? Modo { get; set; }

        public virtual WflCopiaexp WflCopiaexp { get; set; }
    }
}
