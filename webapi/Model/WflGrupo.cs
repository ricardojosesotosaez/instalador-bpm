﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflGrupo
    {
        public WflGrupo()
        {
            WflGrupoProceso = new HashSet<WflGrupoProceso>();
            WflGrupoRegion = new HashSet<WflGrupoRegion>();
            WflGrupoUsuario = new HashSet<WflGrupoUsuario>();
        }

        public Guid IdGrupo { get; set; }
        public string CodGrupo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string UrlSharepoint { get; set; }
        public string IdGrupoSharepoint { get; set; }
        public string NombreGrupoSharepoint { get; set; }

        public virtual ICollection<WflGrupoProceso> WflGrupoProceso { get; set; }
        public virtual ICollection<WflGrupoRegion> WflGrupoRegion { get; set; }
        public virtual ICollection<WflGrupoUsuario> WflGrupoUsuario { get; set; }
    }
}
