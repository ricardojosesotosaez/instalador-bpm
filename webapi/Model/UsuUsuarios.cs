﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuUsuarios
    {
        public UsuUsuarios()
        {
            UsuHistClave = new HashSet<UsuHistClave>();
            UsuUsuariosPerfiles = new HashSet<UsuUsuariosPerfiles>();
            UsuUsuariosRoles = new HashSet<UsuUsuariosRoles>();
            WflGrupoUsuario = new HashSet<WflGrupoUsuario>();
            WflUnidadUsuario = new HashSet<WflUnidadUsuario>();
        }

        public short IdAdm { get; set; }
        public string IdUsuario { get; set; }
        public string IdPersona { get; set; }
        public string Nombre { get; set; }
        public string Estado { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public short CodCargo { get; set; }
        public short CodAgencia { get; set; }
        public int? CodComuna { get; set; }
        public short CodArea { get; set; }
        public string IndBloqueo { get; set; }
        public string IdUsuarioJefe { get; set; }
        public string Clave { get; set; }
        public DateTime FecExpClave { get; set; }
        public short? CantIntentos { get; set; }
        public DateTime? FecUltIngreso { get; set; }
        public string CodSafp { get; set; }
        public string Email { get; set; }
        public string IdActiveDirectory { get; set; }
        public string HashIntegridad { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public string Tipo { get; set; }
        public string Region { get; set; }

        public virtual ICollection<UsuHistClave> UsuHistClave { get; set; }
        public virtual ICollection<UsuUsuariosPerfiles> UsuUsuariosPerfiles { get; set; }
        public virtual ICollection<UsuUsuariosRoles> UsuUsuariosRoles { get; set; }
        public virtual ICollection<WflGrupoUsuario> WflGrupoUsuario { get; set; }
        public virtual ICollection<WflUnidadUsuario> WflUnidadUsuario { get; set; }
    }
}
