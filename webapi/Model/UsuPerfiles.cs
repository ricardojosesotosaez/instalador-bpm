﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuPerfiles
    {
        public UsuPerfiles()
        {
            UsuPerfilesRoles = new HashSet<UsuPerfilesRoles>();
            UsuUsuariosPerfiles = new HashSet<UsuUsuariosPerfiles>();
        }

        public short IdAdm { get; set; }
        public string IdPerfil { get; set; }
        public string Descripcion { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual ICollection<UsuPerfilesRoles> UsuPerfilesRoles { get; set; }
        public virtual ICollection<UsuUsuariosPerfiles> UsuUsuariosPerfiles { get; set; }
    }
}
