﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflDiag
    {
        public WflDiag()
        {
            WflFlecha = new HashSet<WflFlecha>();
            WflProc = new HashSet<WflProc>();
            WflTarea = new HashSet<WflTarea>();
        }

        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int? Lapso { get; set; }
        public string IdExterno { get; set; }

        public virtual WflProy WflProy { get; set; }
        public virtual ICollection<WflFlecha> WflFlecha { get; set; }
        public virtual ICollection<WflProc> WflProc { get; set; }
        public virtual ICollection<WflTarea> WflTarea { get; set; }
    }
}
