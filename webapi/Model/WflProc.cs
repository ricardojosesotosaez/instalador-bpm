﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflProc
    {
        public WflProc()
        {
            UsuRolesProc = new HashSet<UsuRolesProc>();
            WflGrupoProceso = new HashSet<WflGrupoProceso>();
        }

        public int Procesoid { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }

        public virtual WflDiag WflDiag { get; set; }
        public virtual ICollection<UsuRolesProc> UsuRolesProc { get; set; }
        public virtual ICollection<WflGrupoProceso> WflGrupoProceso { get; set; }
    }
}
