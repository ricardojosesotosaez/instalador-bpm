﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuMenusFuncionalidades
    {
        public int IdMenu { get; set; }
        public string IdFuncionalidad { get; set; }
        public int? Orden { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual UsuFuncionalidades IdFuncionalidadNavigation { get; set; }
        public virtual UsuMenus IdMenuNavigation { get; set; }
    }
}
