﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflMensaje
    {
        public Guid IdMensaje { get; set; }
        public string Tipo { get; set; }
        public string IdUsuario { get; set; }
        public string Texto { get; set; }
        public string Xml { get; set; }
        public DateTime FecExpiracion { get; set; }
    }
}
