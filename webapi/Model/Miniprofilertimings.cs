﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class Miniprofilertimings
    {
        public int Rowid { get; set; }
        public Guid Id { get; set; }
        public Guid Miniprofilerid { get; set; }
        public Guid? Parenttimingid { get; set; }
        public string Name { get; set; }
        public decimal Durationmilliseconds { get; set; }
        public decimal Startmilliseconds { get; set; }
        public bool Isroot { get; set; }
        public short Depth { get; set; }
        public string Customtimingsjson { get; set; }
    }
}
