﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class Miniprofilerclienttimings
    {
        public int Rowid { get; set; }
        public Guid Id { get; set; }
        public Guid Miniprofilerid { get; set; }
        public string Name { get; set; }
        public decimal Start { get; set; }
        public decimal Duration { get; set; }
    }
}
