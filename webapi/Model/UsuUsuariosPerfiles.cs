﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuUsuariosPerfiles
    {
        public short IdAdm { get; set; }
        public string IdUsuario { get; set; }
        public string IdPerfil { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual UsuPerfiles Id { get; set; }
        public virtual UsuUsuarios IdNavigation { get; set; }
    }
}
