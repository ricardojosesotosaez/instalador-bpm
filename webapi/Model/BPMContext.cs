﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace webapi.Model
{
    public partial class BPMContext : DbContext
    {
        public BPMContext()
        {
        }

        public BPMContext(DbContextOptions<BPMContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ArcArchivo> ArcArchivo { get; set; }
        public virtual DbSet<ArcDirectorio> ArcDirectorio { get; set; }
        public virtual DbSet<Miniprofilerclienttimings> Miniprofilerclienttimings { get; set; }
        public virtual DbSet<Miniprofilers> Miniprofilers { get; set; }
        public virtual DbSet<Miniprofilertimings> Miniprofilertimings { get; set; }
        public virtual DbSet<ParAgencias> ParAgencias { get; set; }
        public virtual DbSet<ParAreas> ParAreas { get; set; }
        public virtual DbSet<ParComunas> ParComunas { get; set; }
        public virtual DbSet<ParDatosClave> ParDatosClave { get; set; }
        public virtual DbSet<ParFeriados> ParFeriados { get; set; }
        public virtual DbSet<ParGeneral> ParGeneral { get; set; }
        public virtual DbSet<ParTiposCargos> ParTiposCargos { get; set; }
        public virtual DbSet<UsuAccesos> UsuAccesos { get; set; }
        public virtual DbSet<UsuFuncionalidades> UsuFuncionalidades { get; set; }
        public virtual DbSet<UsuFuncionalidadesRoles> UsuFuncionalidadesRoles { get; set; }
        public virtual DbSet<UsuHistClave> UsuHistClave { get; set; }
        public virtual DbSet<UsuMenus> UsuMenus { get; set; }
        public virtual DbSet<UsuMenusFuncionalidades> UsuMenusFuncionalidades { get; set; }
        public virtual DbSet<UsuPerfiles> UsuPerfiles { get; set; }
        public virtual DbSet<UsuPerfilesRoles> UsuPerfilesRoles { get; set; }
        public virtual DbSet<UsuProcesosnegocio> UsuProcesosnegocio { get; set; }
        public virtual DbSet<UsuRecurso> UsuRecurso { get; set; }
        public virtual DbSet<UsuRecursoFuncionalidades> UsuRecursoFuncionalidades { get; set; }
        public virtual DbSet<UsuRoles> UsuRoles { get; set; }
        public virtual DbSet<UsuRolesProc> UsuRolesProc { get; set; }
        public virtual DbSet<UsuSesiones> UsuSesiones { get; set; }
        public virtual DbSet<UsuUserControl> UsuUserControl { get; set; }
        public virtual DbSet<UsuUserControlFuncionalidad> UsuUserControlFuncionalidad { get; set; }
        public virtual DbSet<UsuUsuarios> UsuUsuarios { get; set; }
        public virtual DbSet<UsuUsuariosPerfiles> UsuUsuariosPerfiles { get; set; }
        public virtual DbSet<UsuUsuariosRoles> UsuUsuariosRoles { get; set; }
        public virtual DbSet<WflAlarma> WflAlarma { get; set; }
        public virtual DbSet<WflAlmacenPwd> WflAlmacenPwd { get; set; }
        public virtual DbSet<WflAplicacion> WflAplicacion { get; set; }
        public virtual DbSet<WflAtrEntidad> WflAtrEntidad { get; set; }
        public virtual DbSet<WflAtributos> WflAtributos { get; set; }
        public virtual DbSet<WflBusqueda> WflBusqueda { get; set; }
        public virtual DbSet<WflBusquedaFiltro> WflBusquedaFiltro { get; set; }
        public virtual DbSet<WflCopiaexp> WflCopiaexp { get; set; }
        public virtual DbSet<WflCopiaexpComp> WflCopiaexpComp { get; set; }
        public virtual DbSet<WflCopiaexpLock> WflCopiaexpLock { get; set; }
        public virtual DbSet<WflCopiaexpTemp> WflCopiaexpTemp { get; set; }
        public virtual DbSet<WflCopiasporpersona> WflCopiasporpersona { get; set; }
        public virtual DbSet<WflDiag> WflDiag { get; set; }
        public virtual DbSet<WflEntidad> WflEntidad { get; set; }
        public virtual DbSet<WflEscucha> WflEscucha { get; set; }
        public virtual DbSet<WflExp> WflExp { get; set; }
        public virtual DbSet<WflFlecha> WflFlecha { get; set; }
        public virtual DbSet<WflFlechaalarma> WflFlechaalarma { get; set; }
        public virtual DbSet<WflFlechaflujo> WflFlechaflujo { get; set; }
        public virtual DbSet<WflGrupo> WflGrupo { get; set; }
        public virtual DbSet<WflGrupoProceso> WflGrupoProceso { get; set; }
        public virtual DbSet<WflGrupoRegion> WflGrupoRegion { get; set; }
        public virtual DbSet<WflGrupoUsuario> WflGrupoUsuario { get; set; }
        public virtual DbSet<WflJerarquia> WflJerarquia { get; set; }
        public virtual DbSet<WflMensaje> WflMensaje { get; set; }
        public virtual DbSet<WflOpcion> WflOpcion { get; set; }
        public virtual DbSet<WflOpcionUsuario> WflOpcionUsuario { get; set; }
        public virtual DbSet<WflParametrogeneral> WflParametrogeneral { get; set; }
        public virtual DbSet<WflPlantilla> WflPlantilla { get; set; }
        public virtual DbSet<WflProc> WflProc { get; set; }
        public virtual DbSet<WflProy> WflProy { get; set; }
        public virtual DbSet<WflRegion> WflRegion { get; set; }
        public virtual DbSet<WflRegistro> WflRegistro { get; set; }
        public virtual DbSet<WflSubprocesoAPublicar> WflSubprocesoAPublicar { get; set; }
        public virtual DbSet<WflSubprocesoAPublicarExt> WflSubprocesoAPublicarExt { get; set; }
        public virtual DbSet<WflTarea> WflTarea { get; set; }
        public virtual DbSet<WflTareaRegion> WflTareaRegion { get; set; }
        public virtual DbSet<WflTareacomp> WflTareacomp { get; set; }
        public virtual DbSet<WflTareaconector> WflTareaconector { get; set; }
        public virtual DbSet<WflTareaescucha> WflTareaescucha { get; set; }
        public virtual DbSet<WflTareageneral> WflTareageneral { get; set; }
        public virtual DbSet<WflTestescucha> WflTestescucha { get; set; }
        public virtual DbSet<WflTipoexp> WflTipoexp { get; set; }
        public virtual DbSet<WflUnidad> WflUnidad { get; set; }
        public virtual DbSet<WflUnidadAtrEntidad> WflUnidadAtrEntidad { get; set; }
        public virtual DbSet<WflUnidadRol> WflUnidadRol { get; set; }
        public virtual DbSet<WflUnidadUsuario> WflUnidadUsuario { get; set; }

        // Unable to generate entity type for table 'dbo.WFL_SUBPROCESO_A_PUBLICAR_EXT_RESPALDO_HOMO'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SYS_SEQUENCES'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.MyTable'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TEMPCLARITY'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.WFL_REGISTRO_BCK_170921'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.WFL_SUBPROCESO_A_PUBLICAR_EXT_RESPALDO'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.WFL_TOGGLE'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\;Database=BPM;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<ArcArchivo>(entity =>
            {
                entity.HasKey(e => e.IdArchivo);

                entity.ToTable("ARC_ARCHIVO");

                entity.Property(e => e.IdArchivo).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FecCreacion).HasColumnType("datetime");

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModificacion).HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Idrepositorio).HasColumnName("IDREPOSITORIO");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Notas)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UsuCreacion)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UsuPropietario)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UsuUltModificacion)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdDirectorioPadreNavigation)
                    .WithMany(p => p.ArcArchivo)
                    .HasForeignKey(d => d.IdDirectorioPadre)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ARC_ARCHIVO_ARC_DIRECTORIO");
            });

            modelBuilder.Entity<ArcDirectorio>(entity =>
            {
                entity.HasKey(e => e.IdDirectorio);

                entity.ToTable("ARC_DIRECTORIO");

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('SISTEMA')");

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('SISTEMA')");

                entity.Property(e => e.Idrepositorio).HasColumnName("IDREPOSITORIO");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ruta)
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.RutaVirtual)
                    .HasColumnName("RUTA_VIRTUAL")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdDirectorioPadreNavigation)
                    .WithMany(p => p.InverseIdDirectorioPadreNavigation)
                    .HasForeignKey(d => d.IdDirectorioPadre)
                    .HasConstraintName("FK_DIRECTORIO_PADRE");
            });

            modelBuilder.Entity<Miniprofilerclienttimings>(entity =>
            {
                entity.HasKey(e => e.Rowid);

                entity.ToTable("MINIPROFILERCLIENTTIMINGS");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.HasIndex(e => e.Miniprofilerid);

                entity.Property(e => e.Rowid).HasColumnName("ROWID");

                entity.Property(e => e.Duration)
                    .HasColumnName("DURATION")
                    .HasColumnType("decimal(9, 3)");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Miniprofilerid).HasColumnName("MINIPROFILERID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.Start)
                    .HasColumnName("START")
                    .HasColumnType("decimal(9, 3)");
            });

            modelBuilder.Entity<Miniprofilers>(entity =>
            {
                entity.HasKey(e => e.Rowid);

                entity.ToTable("MINIPROFILERS");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.HasIndex(e => new { e.Id, e.Started, e.User, e.Hasuserviewed })
                    .HasName("IX_MINIPROFILERS_USER_HASUSERVIEWED_INCLUDES");

                entity.Property(e => e.Rowid).HasColumnName("ROWID");

                entity.Property(e => e.Clienttimingsredirectcount).HasColumnName("CLIENTTIMINGSREDIRECTCOUNT");

                entity.Property(e => e.Customlinksjson).HasColumnName("CUSTOMLINKSJSON");

                entity.Property(e => e.Durationmilliseconds)
                    .HasColumnName("DURATIONMILLISECONDS")
                    .HasColumnType("decimal(7, 1)");

                entity.Property(e => e.Hasuserviewed).HasColumnName("HASUSERVIEWED");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Machinename)
                    .HasColumnName("MACHINENAME")
                    .HasMaxLength(100);

                entity.Property(e => e.Roottimingid).HasColumnName("ROOTTIMINGID");

                entity.Property(e => e.Started)
                    .HasColumnName("STARTED")
                    .HasColumnType("datetime");

                entity.Property(e => e.User)
                    .HasColumnName("USER")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Miniprofilertimings>(entity =>
            {
                entity.HasKey(e => e.Rowid);

                entity.ToTable("MINIPROFILERTIMINGS");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.HasIndex(e => e.Miniprofilerid);

                entity.Property(e => e.Rowid).HasColumnName("ROWID");

                entity.Property(e => e.Customtimingsjson).HasColumnName("CUSTOMTIMINGSJSON");

                entity.Property(e => e.Depth).HasColumnName("DEPTH");

                entity.Property(e => e.Durationmilliseconds)
                    .HasColumnName("DURATIONMILLISECONDS")
                    .HasColumnType("decimal(9, 3)");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Isroot).HasColumnName("ISROOT");

                entity.Property(e => e.Miniprofilerid).HasColumnName("MINIPROFILERID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.Parenttimingid).HasColumnName("PARENTTIMINGID");

                entity.Property(e => e.Startmilliseconds)
                    .HasColumnName("STARTMILLISECONDS")
                    .HasColumnType("decimal(9, 3)");
            });

            modelBuilder.Entity<ParAgencias>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.CodAgencia });

                entity.ToTable("PAR_AGENCIAS");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.CodAgencia).HasColumnName("COD_AGENCIA");

                entity.Property(e => e.CodComuna).HasColumnName("COD_COMUNA");

                entity.Property(e => e.CodSubgerencia).HasColumnName("COD_SUBGERENCIA");

                entity.Property(e => e.CodZona).HasColumnName("COD_ZONA");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .IsRequired()
                    .HasColumnName("DIRECCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.Folioreclamo650).HasColumnName("FOLIORECLAMO650");

                entity.Property(e => e.HorarioAtencion)
                    .HasColumnName("HORARIO_ATENCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ParAreas>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.CodArea });

                entity.ToTable("PAR_AREAS");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.CodArea).HasColumnName("COD_AREA");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ParComunas>(entity =>
            {
                entity.HasKey(e => e.CodComuna);

                entity.ToTable("PAR_COMUNAS");

                entity.Property(e => e.CodComuna)
                    .HasColumnName("COD_COMUNA")
                    .ValueGeneratedNever();

                entity.Property(e => e.Ciudad)
                    .IsRequired()
                    .HasColumnName("CIUDAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodRegion).HasColumnName("COD_REGION");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ParDatosClave>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.Id });

                entity.ToTable("PAR_DATOS_CLAVE");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DiasAviso).HasColumnName("DIAS_AVISO");

                entity.Property(e => e.DiasExpira).HasColumnName("DIAS_EXPIRA");

                entity.Property(e => e.DiasInactivo).HasColumnName("DIAS_INACTIVO");

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LargoMaximo).HasColumnName("LARGO_MAXIMO");

                entity.Property(e => e.LargoMinimo).HasColumnName("LARGO_MINIMO");

                entity.Property(e => e.NumIntentos).HasColumnName("NUM_INTENTOS");
            });

            modelBuilder.Entity<ParFeriados>(entity =>
            {
                entity.HasKey(e => e.FecFeriado)
                    .HasName("PK_PAR_FERIADOS_1");

                entity.ToTable("PAR_FERIADOS");

                entity.HasIndex(e => e.FecFeriado)
                    .HasName("PK_PAR_FERIADOS");

                entity.Property(e => e.FecFeriado)
                    .HasColumnName("FEC_FERIADO")
                    .HasColumnType("datetime");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ParGeneral>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.CodParametro });

                entity.ToTable("PAR_GENERAL");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.CodParametro)
                    .HasColumnName("COD_PARAMETRO")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Columnas)
                    .HasColumnName("COLUMNAS")
                    .HasColumnType("image");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdProcesoNegocio)
                    .IsRequired()
                    .HasColumnName("ID_PROCESO_NEGOCIO")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NombreTabla)
                    .IsRequired()
                    .HasColumnName("NOMBRE_TABLA")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TipoParametro)
                    .HasColumnName("TIPO_PARAMETRO")
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ParTiposCargos>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.CodCargo });

                entity.ToTable("PAR_TIPOS_CARGOS");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.CodCargo).HasColumnName("COD_CARGO");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IndRecibeComision)
                    .IsRequired()
                    .HasColumnName("IND_RECIBE_COMISION")
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuAccesos>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.IdAcceso });

                entity.ToTable("USU_ACCESOS");

                entity.HasIndex(e => e.FecAcceso)
                    .HasName("IDX_ACCESOS_FECHA");

                entity.HasIndex(e => e.IdFuncionalidad)
                    .HasName("IDX_ACCESOS_FUNCIONALIDAD");

                entity.HasIndex(e => e.IdUsuario)
                    .HasName("IDX_ACCESOS_USUARIO");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdAcceso)
                    .HasColumnName("ID_ACCESO")
                    .HasColumnType("numeric(20, 0)");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FecAcceso)
                    .HasColumnName("FEC_ACCESO")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionalidad)
                    .IsRequired()
                    .HasColumnName("ID_FUNCIONALIDAD")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuario)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ip)
                    .HasColumnName("IP")
                    .HasMaxLength(32)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuFuncionalidades>(entity =>
            {
                entity.HasKey(e => e.IdFuncionalidad)
                    .HasName("PK_FUNCIONALIDAD");

                entity.ToTable("USU_FUNCIONALIDADES");

                entity.Property(e => e.IdFuncionalidad)
                    .HasColumnName("ID_FUNCIONALIDAD")
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdProcesoNegocio)
                    .IsRequired()
                    .HasColumnName("ID_PROCESO_NEGOCIO")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdProcesoNegocioNavigation)
                    .WithMany(p => p.UsuFuncionalidades)
                    .HasForeignKey(d => d.IdProcesoNegocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_FUNCIONALIDADES_USU_PROCESOSNEGOCIO");
            });

            modelBuilder.Entity<UsuFuncionalidadesRoles>(entity =>
            {
                entity.HasKey(e => new { e.IdFuncionalidad, e.IdAdm, e.IdRol })
                    .HasName("PK_FUNCIONALIDAD_ROL");

                entity.ToTable("USU_FUNCIONALIDADES_ROLES");

                entity.Property(e => e.IdFuncionalidad)
                    .HasColumnName("ID_FUNCIONALIDAD")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdFuncionalidadNavigation)
                    .WithMany(p => p.UsuFuncionalidadesRoles)
                    .HasForeignKey(d => d.IdFuncionalidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_FUNCIONALIDADES_ROLES_USU_FUNCIONALIDADES");

                entity.HasOne(d => d.Id)
                    .WithMany(p => p.UsuFuncionalidadesRoles)
                    .HasForeignKey(d => new { d.IdAdm, d.IdRol })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_FUNCIONALIDADES_ROLES_USU_ROLES");
            });

            modelBuilder.Entity<UsuHistClave>(entity =>
            {
                entity.HasKey(e => new { e.Fec, e.IdAdm, e.IdUsuario })
                    .HasName("USU_HIST_CLAVE_PK");

                entity.ToTable("USU_HIST_CLAVE");

                entity.Property(e => e.Fec)
                    .HasColumnName("FEC")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Clave)
                    .HasColumnName("CLAVE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo).HasColumnName("TIPO");

                entity.HasOne(d => d.Id)
                    .WithMany(p => p.UsuHistClave)
                    .HasForeignKey(d => new { d.IdUsuario, d.IdAdm })
                    .HasConstraintName("FK_USU_HIST_CLAVE_USU_USUARIOS");
            });

            modelBuilder.Entity<UsuMenus>(entity =>
            {
                entity.HasKey(e => e.IdMenu);

                entity.ToTable("USU_MENUS");

                entity.Property(e => e.IdMenu)
                    .HasColumnName("ID_MENU")
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdPadre).HasColumnName("ID_PADRE");

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Orden).HasColumnName("ORDEN");

                entity.Property(e => e.Url)
                    .HasColumnName("URL")
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuMenusFuncionalidades>(entity =>
            {
                entity.HasKey(e => new { e.IdMenu, e.IdFuncionalidad })
                    .HasName("PK_USU_MENUS_FUN");

                entity.ToTable("USU_MENUS_FUNCIONALIDADES");

                entity.Property(e => e.IdMenu).HasColumnName("ID_MENU");

                entity.Property(e => e.IdFuncionalidad)
                    .HasColumnName("ID_FUNCIONALIDAD")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Orden).HasColumnName("ORDEN");

                entity.HasOne(d => d.IdFuncionalidadNavigation)
                    .WithMany(p => p.UsuMenusFuncionalidades)
                    .HasForeignKey(d => d.IdFuncionalidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_MENUS_FUNCIONALIDADES_USU_FUNCIONALIDADES");

                entity.HasOne(d => d.IdMenuNavigation)
                    .WithMany(p => p.UsuMenusFuncionalidades)
                    .HasForeignKey(d => d.IdMenu)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_MENUS_FUNCIONALIDADES_USU_MENUS");
            });

            modelBuilder.Entity<UsuPerfiles>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.IdPerfil });

                entity.ToTable("USU_PERFILES");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdPerfil)
                    .HasColumnName("ID_PERFIL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuPerfilesRoles>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.IdPerfil, e.IdRol });

                entity.ToTable("USU_PERFILES_ROLES");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdPerfil)
                    .HasColumnName("ID_PERFIL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Id)
                    .WithMany(p => p.UsuPerfilesRoles)
                    .HasForeignKey(d => new { d.IdAdm, d.IdPerfil })
                    .HasConstraintName("FK_USU_PERFILES_ROLES_USU_PERFILES");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.UsuPerfilesRoles)
                    .HasForeignKey(d => new { d.IdAdm, d.IdRol })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_PERFILES_ROLES_USU_ROLES");
            });

            modelBuilder.Entity<UsuProcesosnegocio>(entity =>
            {
                entity.HasKey(e => e.IdProcesoNegocio)
                    .HasName("PK_PROCESONEGOCIO");

                entity.ToTable("USU_PROCESOSNEGOCIO");

                entity.Property(e => e.IdProcesoNegocio)
                    .HasColumnName("ID_PROCESO_NEGOCIO")
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuRecurso>(entity =>
            {
                entity.HasKey(e => e.IdRecurso);

                entity.ToTable("USU_RECURSO");

                entity.HasIndex(e => e.Recurso);

                entity.HasIndex(e => e.Tipo);

                entity.Property(e => e.IdRecurso)
                    .HasColumnName("ID_RECURSO")
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Recurso)
                    .HasColumnName("RECURSO")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasColumnName("TIPO")
                    .HasMaxLength(16)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuRecursoFuncionalidades>(entity =>
            {
                entity.HasKey(e => new { e.IdRecurso, e.IdFuncionalidad });

                entity.ToTable("USU_RECURSO_FUNCIONALIDADES");

                entity.Property(e => e.IdRecurso)
                    .HasColumnName("ID_RECURSO")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IdFuncionalidad)
                    .HasColumnName("ID_FUNCIONALIDAD")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdFuncionalidadNavigation)
                    .WithMany(p => p.UsuRecursoFuncionalidades)
                    .HasForeignKey(d => d.IdFuncionalidad)
                    .HasConstraintName("FK_RECFUN_RECURSO");

                entity.HasOne(d => d.IdRecursoNavigation)
                    .WithMany(p => p.UsuRecursoFuncionalidades)
                    .HasForeignKey(d => d.IdRecurso)
                    .HasConstraintName("FK_RECFUN_FUNCIONALIDADES");
            });

            modelBuilder.Entity<UsuRoles>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.IdRol })
                    .HasName("PK_ROLES");

                entity.ToTable("USU_ROLES");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdProcesoNegocio)
                    .IsRequired()
                    .HasColumnName("ID_PROCESO_NEGOCIO")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IndAdminPn)
                    .IsRequired()
                    .HasColumnName("IND_ADMIN_PN")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TipoRolWorkflow)
                    .HasColumnName("TIPO_ROL_WORKFLOW")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdProcesoNegocioNavigation)
                    .WithMany(p => p.UsuRoles)
                    .HasForeignKey(d => d.IdProcesoNegocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_ROLES_USU_PROCESOSNEGOCIO");
            });

            modelBuilder.Entity<UsuRolesProc>(entity =>
            {
                entity.HasKey(e => new { e.Procesoid, e.IdRol, e.IdAdm });

                entity.ToTable("USU_ROLES_PROC");

                entity.Property(e => e.Procesoid).HasColumnName("PROCESOID");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.Proceso)
                    .WithMany(p => p.UsuRolesProc)
                    .HasForeignKey(d => d.Procesoid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_ROLES_PROC_WFL_PROC");

                entity.HasOne(d => d.Id)
                    .WithMany(p => p.UsuRolesProc)
                    .HasForeignKey(d => new { d.IdAdm, d.IdRol })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_ROLES_PROC_USU_ROLES");
            });

            modelBuilder.Entity<UsuSesiones>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.IdUsuario, e.IdSesion });

                entity.ToTable("USU_SESIONES");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdSesion)
                    .HasColumnName("ID_SESION")
                    .HasColumnType("numeric(20, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AspnetSessionid)
                    .HasColumnName("ASPNET_SESSIONID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionIp)
                    .HasColumnName("DIRECCION_IP")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecFin)
                    .HasColumnName("FEC_FIN")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecInicio)
                    .HasColumnName("FEC_INICIO")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Token)
                    .HasColumnName("TOKEN")
                    .HasMaxLength(64)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuUserControl>(entity =>
            {
                entity.HasKey(e => e.NomUserControl)
                    .HasName("USU_USER_CONTROL_PK");

                entity.ToTable("USU_USER_CONTROL");

                entity.Property(e => e.NomUserControl)
                    .HasColumnName("NOM_USER_CONTROL")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuUserControlFuncionalidad>(entity =>
            {
                entity.HasKey(e => new { e.NomUserControl, e.IdFuncionalidad })
                    .HasName("PK_USER_CONTROL_FUNCIONALIDAD");

                entity.ToTable("USU_USER_CONTROL_FUNCIONALIDAD");

                entity.Property(e => e.NomUserControl)
                    .HasColumnName("NOM_USER_CONTROL")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IdFuncionalidad)
                    .HasColumnName("ID_FUNCIONALIDAD")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Inicial).HasColumnName("INICIAL");

                entity.Property(e => e.Modo)
                    .HasColumnName("MODO")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdFuncionalidadNavigation)
                    .WithMany(p => p.UsuUserControlFuncionalidad)
                    .HasForeignKey(d => d.IdFuncionalidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_USER_CONTROL_FUNCIONALIDAD_USU_FUNCIONALIDADES");

                entity.HasOne(d => d.NomUserControlNavigation)
                    .WithMany(p => p.UsuUserControlFuncionalidad)
                    .HasForeignKey(d => d.NomUserControl)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_USER_CONTROL_FUNCIONALIDAD_USU_USER_CONTROL");
            });

            modelBuilder.Entity<UsuUsuarios>(entity =>
            {
                entity.HasKey(e => new { e.IdUsuario, e.IdAdm })
                    .HasName("PK_PERSONA");

                entity.ToTable("USU_USUARIOS");

                entity.HasIndex(e => e.IdUsuario)
                    .HasName("IDX2_USU_USUARIOS");

                entity.HasIndex(e => new { e.IdUsuario, e.Nombre })
                    .HasName("IDX1_USU_USUARIOS");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.CantIntentos).HasColumnName("CANT_INTENTOS");

                entity.Property(e => e.Clave)
                    .IsRequired()
                    .HasColumnName("CLAVE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodAgencia).HasColumnName("COD_AGENCIA");

                entity.Property(e => e.CodArea).HasColumnName("COD_AREA");

                entity.Property(e => e.CodCargo).HasColumnName("COD_CARGO");

                entity.Property(e => e.CodComuna).HasColumnName("COD_COMUNA");

                entity.Property(e => e.CodSafp)
                    .HasColumnName("COD_SAFP")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasColumnName("DIRECCION")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("ESTADO")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecExpClave)
                    .HasColumnName("FEC_EXP_CLAVE")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltIngreso)
                    .HasColumnName("FEC_ULT_INGRESO")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.HashIntegridad)
                    .HasColumnName("HASH_INTEGRIDAD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdActiveDirectory)
                    .HasColumnName("ID_ACTIVE_DIRECTORY")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdPersona)
                    .IsRequired()
                    .HasColumnName("ID_PERSONA")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioJefe)
                    .HasColumnName("ID_USUARIO_JEFE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IndBloqueo)
                    .IsRequired()
                    .HasColumnName("IND_BLOQUEO")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasColumnName("REGION")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasColumnName("TELEFONO")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("TIPO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('U')");
            });

            modelBuilder.Entity<UsuUsuariosPerfiles>(entity =>
            {
                entity.HasKey(e => new { e.IdAdm, e.IdUsuario, e.IdPerfil });

                entity.ToTable("USU_USUARIOS_PERFILES");

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdPerfil)
                    .HasColumnName("ID_PERFIL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Id)
                    .WithMany(p => p.UsuUsuariosPerfiles)
                    .HasForeignKey(d => new { d.IdAdm, d.IdPerfil })
                    .HasConstraintName("FK_USU_USUARIOS_PERFILES_USU_PERFILES");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.UsuUsuariosPerfiles)
                    .HasForeignKey(d => new { d.IdUsuario, d.IdAdm })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_USUARIOS_PERFILES_USU_USUARIOS");
            });

            modelBuilder.Entity<UsuUsuariosRoles>(entity =>
            {
                entity.HasKey(e => new { e.IdUsuario, e.IdAdm, e.IdRol })
                    .HasName("PK_PERSONA_ROL");

                entity.ToTable("USU_USUARIOS_ROLES");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Id)
                    .WithMany(p => p.UsuUsuariosRoles)
                    .HasForeignKey(d => new { d.IdAdm, d.IdRol })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_USUARIOS_ROLES_USU_ROLES");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.UsuUsuariosRoles)
                    .HasForeignKey(d => new { d.IdUsuario, d.IdAdm })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USU_USUARIOS_ROLES_USU_USUARIOS");
            });

            modelBuilder.Entity<WflAlarma>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Alarmaid })
                    .HasName("PK_ALARMA");

                entity.ToTable("WFL_ALARMA");

                entity.HasIndex(e => e.Activada)
                    .HasName("IDX1_ALARMA");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid })
                    .HasName("IDX3_ALARMA");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Flechaid })
                    .HasName("IDX2_ALARMA");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Alarmaid).HasColumnName("ALARMAID");

                entity.Property(e => e.Activada).HasColumnName("ACTIVADA");

                entity.Property(e => e.Copiaexpid).HasColumnName("COPIAEXPID");

                entity.Property(e => e.Diagid)
                    .IsRequired()
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fechafinal)
                    .HasColumnName("FECHAFINAL")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fechafinalposible)
                    .HasColumnName("FECHAFINALPOSIBLE")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fechainicial)
                    .HasColumnName("FECHAINICIAL")
                    .HasColumnType("datetime");

                entity.Property(e => e.Flechaid).HasColumnName("FLECHAID");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Llaveexpid)
                    .IsRequired()
                    .HasColumnName("LLAVEEXPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Tareaid).HasColumnName("TAREAID");

                entity.HasOne(d => d.WflProy)
                    .WithMany(p => p.WflAlarma)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ALARMA_PROY");
            });

            modelBuilder.Entity<WflAlmacenPwd>(entity =>
            {
                entity.HasKey(e => new { e.Aplicacion, e.Usuarioid });

                entity.ToTable("WFL_ALMACEN_PWD");

                entity.Property(e => e.Aplicacion)
                    .HasColumnName("APLICACION")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Usuarioid)
                    .HasColumnName("USUARIOID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .HasColumnName("ESTADO")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.FecCambEstado)
                    .HasColumnName("FEC_CAMB_ESTADO")
                    .HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("PASSWORD")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Servidor)
                    .HasColumnName("SERVIDOR")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Usuarioappid)
                    .IsRequired()
                    .HasColumnName("USUARIOAPPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WflAplicacion>(entity =>
            {
                entity.HasKey(e => new { e.Aplicacion, e.Region });

                entity.ToTable("WFL_APLICACION");

                entity.Property(e => e.Aplicacion)
                    .HasColumnName("APLICACION")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasColumnName("REGION")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Claseauth)
                    .HasColumnName("CLASEAUTH")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CrearUsu).HasColumnName("CREAR_USU");

                entity.Property(e => e.PeriodSincro)
                    .HasColumnName("PERIOD_SINCRO")
                    .HasColumnType("text");

                entity.Property(e => e.SincUsu).HasColumnName("SINC_USU");

                entity.Property(e => e.UltimaSincro)
                    .HasColumnName("ULTIMA_SINCRO")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<WflAtrEntidad>(entity =>
            {
                entity.HasKey(e => e.IdAtrEntidad);

                entity.ToTable("WFL_ATR_ENTIDAD");

                entity.Property(e => e.IdAtrEntidad)
                    .HasColumnName("ID_ATR_ENTIDAD")
                    .ValueGeneratedNever();

                entity.Property(e => e.Clase)
                    .HasColumnName("CLASE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CodAtrEntidad)
                    .IsRequired()
                    .HasColumnName("COD_ATR_ENTIDAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdEntidad).HasColumnName("ID_ENTIDAD");

                entity.Property(e => e.Nombre)
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasColumnName("TIPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Usercontrol)
                    .HasColumnName("USERCONTROL")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdEntidadNavigation)
                    .WithMany(p => p.WflAtrEntidad)
                    .HasForeignKey(d => d.IdEntidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WFL_ATR_ENTIDAD_WFL_ENTIDAD");
            });

            modelBuilder.Entity<WflAtributos>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid })
                    .HasName("PK_WFL_ATIRBUTO");

                entity.ToTable("WFL_ATRIBUTOS");

                entity.HasIndex(e => e.Uid)
                    .HasName("IDX_UID_WFL_ATRIBUTOS")
                    .IsUnique();

                entity.HasIndex(e => new { e.Proyid, e.Referencia1 })
                    .HasName("IDX_ATRIBUTO_REFERENCIA_2");

                entity.HasIndex(e => new { e.Proyid, e.Referencia3 })
                    .HasName("IDX_ATRIBUTO_REFERENCIA_3");

                entity.HasIndex(e => new { e.Versionid, e.Llaveexpid })
                    .HasName("IDX3_WFL_ATRIBUTOS");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid })
                    .HasName("WFL_ATRIBUTOS_idx020");

                entity.HasIndex(e => new { e.Asunto, e.Llaveexpid, e.Proyid, e.Versionid, e.Copiaexpid })
                    .HasName("IDX4_WFL_ATRIBUTOS");

                entity.HasIndex(e => new { e.Asunto, e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid })
                    .HasName("WFL_ATRIBUTOS_idx021");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Llaveexpid)
                    .HasColumnName("LLAVEEXPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Copiaexpid).HasColumnName("COPIAEXPID");

                entity.Property(e => e.Asunto)
                    .HasColumnName("ASUNTO")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.AtributosXml)
                    .HasColumnName("ATRIBUTOS_XML")
                    .HasColumnType("text");

                entity.Property(e => e.CentroCosto)
                    .HasColumnName("CENTRO_COSTO")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.Entidad)
                    .HasColumnName("ENTIDAD")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.FecPrioridad)
                    .HasColumnName("FEC_PRIORIDAD")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecSeguimiento)
                    .HasColumnName("FEC_SEGUIMIENTO")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Fecha1)
                    .HasColumnName("FECHA_1")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecha2)
                    .HasColumnName("FECHA_2")
                    .HasColumnType("datetime");

                entity.Property(e => e.Indice)
                    .HasColumnName("INDICE")
                    .IsUnicode(false);

                entity.Property(e => e.IndiceComp)
                    .HasColumnName("INDICE_COMP")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Monto)
                    .HasColumnName("MONTO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NumDoc).HasColumnName("NUM_DOC");

                entity.Property(e => e.Prioridad).HasColumnName("PRIORIDAD");

                entity.Property(e => e.PrioridadNueva).HasColumnName("PRIORIDAD_NUEVA");

                entity.Property(e => e.Referencia1)
                    .HasColumnName("REFERENCIA_1")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Referencia2)
                    .HasColumnName("REFERENCIA_2")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Referencia3)
                    .HasColumnName("REFERENCIA_3")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasColumnName("REGION")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDoc)
                    .HasColumnName("TIPO_DOC")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.Uid)
                    .HasColumnName("UID")
                    .HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.WflCopiaexp)
                    .WithOne(p => p.WflAtributos)
                    .HasForeignKey<WflAtributos>(d => new { d.Proyid, d.Versionid, d.Llaveexpid, d.Copiaexpid })
                    .HasConstraintName("FK_WFL_ATRIB_WFL_COPIA");
            });

            modelBuilder.Entity<WflBusqueda>(entity =>
            {
                entity.HasKey(e => e.IdBusqueda);

                entity.ToTable("WFL_BUSQUEDA");

                entity.Property(e => e.IdBusqueda)
                    .HasColumnName("ID_BUSQUEDA")
                    .ValueGeneratedNever();

                entity.Property(e => e.Entidad)
                    .IsRequired()
                    .HasColumnName("ENTIDAD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuario)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Titulo)
                    .IsRequired()
                    .HasColumnName("TITULO")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WflBusquedaFiltro>(entity =>
            {
                entity.HasKey(e => e.IdBusquedaFiltro);

                entity.ToTable("WFL_BUSQUEDA_FILTRO");

                entity.Property(e => e.IdBusquedaFiltro)
                    .HasColumnName("ID_BUSQUEDA_FILTRO")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdBusqueda).HasColumnName("ID_BUSQUEDA");

                entity.Property(e => e.IdCampo)
                    .IsRequired()
                    .HasColumnName("ID_CAMPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdFiltro)
                    .HasColumnName("ID_FILTRO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Orden).HasColumnName("ORDEN");

                entity.Property(e => e.Valor1)
                    .HasColumnName("VALOR1")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Valor2)
                    .HasColumnName("VALOR2")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdBusquedaNavigation)
                    .WithMany(p => p.WflBusquedaFiltro)
                    .HasForeignKey(d => d.IdBusqueda)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_BUSQUEDA_FILTRO");
            });

            modelBuilder.Entity<WflCopiaexp>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid })
                    .HasName("PK_COPIAEXP");

                entity.ToTable("WFL_COPIAEXP");

                entity.HasIndex(e => e.Asignada)
                    .HasName("IDX1_COPIAEXP");

                entity.HasIndex(e => e.Fecha)
                    .HasName("IDX5_COPIAEXP");

                entity.HasIndex(e => e.Pendpordesp)
                    .HasName("IDX3_COPIAEXP");

                entity.HasIndex(e => e.Personaid)
                    .HasName("IDX6_COPIAEXP");

                entity.HasIndex(e => new { e.Esdespachable, e.Pendpordesp })
                    .HasName("IDX2_COPIAEXP");

                entity.HasIndex(e => new { e.Personaid, e.Diagid, e.Proyid })
                    .HasName("IDX8_WFL_COPIAEXP");

                entity.HasIndex(e => new { e.Versionid, e.Llaveexpid, e.Proyid })
                    .HasName("IDX10_WFL_COPIAEXP");

                entity.HasIndex(e => new { e.Personaid, e.Esdespachable, e.Asignada, e.Proyid })
                    .HasName("IDX7_COPIAEXP");

                entity.HasIndex(e => new { e.Personaid, e.Versionid, e.Diagid, e.Proyid })
                    .HasName("IDX2_WFL_COPIAEXP");

                entity.HasIndex(e => new { e.Personaid, e.Versionid, e.Diagid, e.Tareaid })
                    .HasName("IDX6_WFL_COPIAEXP");

                entity.HasIndex(e => new { e.Versionid, e.Llaveexpid, e.Personaid, e.Diagid, e.Proyid })
                    .HasName("IDX1_WFL_COPIAEXP");

                entity.HasIndex(e => new { e.Personaid, e.Tareaid, e.Proyid, e.Versionid, e.Diagid, e.Llaveexpid })
                    .HasName("IDX5_WFL_COPIAEXP");

                entity.HasIndex(e => new { e.Diagid, e.Tareaid, e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid, e.Personaid })
                    .HasName("WFL_COPIAEXP_idx020");

                entity.HasIndex(e => new { e.Copiaexpid, e.Diagid, e.Tareaid, e.Asignada, e.Esdespachable, e.Fecha, e.Descripcion, e.Fechavenc, e.EsfuerzoEsperado, e.TiempoEsperado, e.Proyid, e.Versionid, e.Llaveexpid, e.Personaid })
                    .HasName("WFL_COPIAEXP_idx021");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Llaveexpid)
                    .HasColumnName("LLAVEEXPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Copiaexpid).HasColumnName("COPIAEXPID");

                entity.Property(e => e.Asignada).HasColumnName("ASIGNADA");

                entity.Property(e => e.CodGrupo)
                    .HasColumnName("COD_GRUPO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Diagid)
                    .IsRequired()
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Esdespachable).HasColumnName("ESDESPACHABLE");

                entity.Property(e => e.EsfuerzoEsperado).HasColumnName("ESFUERZO_ESPERADO");

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecha)
                    .HasColumnName("FECHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fechavenc)
                    .HasColumnName("FECHAVENC")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NroErrServicio).HasColumnName("NRO_ERR_SERVICIO");

                entity.Property(e => e.Obs)
                    .HasColumnName("OBS")
                    .HasColumnType("text");

                entity.Property(e => e.Pendpordesp).HasColumnName("PENDPORDESP");

                entity.Property(e => e.Personaid)
                    .HasColumnName("PERSONAID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasColumnName("REGION")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Tareaid).HasColumnName("TAREAID");

                entity.Property(e => e.TiempoEsperado).HasColumnName("TIEMPO_ESPERADO");

                entity.Property(e => e.Tipoexpid)
                    .IsRequired()
                    .HasColumnName("TIPOEXPID")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("VERSION")
                    .IsRowVersion();

                entity.HasOne(d => d.WflExp)
                    .WithMany(p => p.WflCopiaexp)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.Llaveexpid })
                    .HasConstraintName("FK_COPIAEXP_EXP");

                entity.HasOne(d => d.WflTarea)
                    .WithMany(p => p.WflCopiaexp)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.Diagid, d.Tareaid })
                    .HasConstraintName("FK_COPIAEXP_TAREA");
            });

            modelBuilder.Entity<WflCopiaexpComp>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid, e.Personaid })
                    .HasName("PK_COPIAEXP_COMPARTIDA");

                entity.ToTable("WFL_COPIAEXP_COMP");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Llaveexpid)
                    .HasColumnName("LLAVEEXPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Copiaexpid).HasColumnName("COPIAEXPID");

                entity.Property(e => e.Personaid)
                    .HasColumnName("PERSONAID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Modo).HasColumnName("MODO");

                entity.HasOne(d => d.WflCopiaexp)
                    .WithMany(p => p.WflCopiaexpComp)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.Llaveexpid, d.Copiaexpid })
                    .HasConstraintName("FK_WFL_COPIAEXP_COMP_WFL_COPIAEXP1");
            });

            modelBuilder.Entity<WflCopiaexpLock>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid })
                    .HasName("PK_COPIAEXP_LOCK");

                entity.ToTable("WFL_COPIAEXP_LOCK");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Llaveexpid)
                    .HasColumnName("LLAVEEXPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Copiaexpid).HasColumnName("COPIAEXPID");

                entity.Property(e => e.FechaLock)
                    .HasColumnName("FECHA_LOCK")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lock).HasColumnName("LOCK");

                entity.Property(e => e.Version)
                    .HasColumnName("VERSION")
                    .IsRowVersion();
            });

            modelBuilder.Entity<WflCopiaexpTemp>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid })
                    .HasName("PK_COPIAEXP_TEMP");

                entity.ToTable("WFL_COPIAEXP_TEMP");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Llaveexpid)
                    .HasColumnName("LLAVEEXPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Copiaexpid).HasColumnName("COPIAEXPID");

                entity.Property(e => e.Esfuerzoreal).HasColumnName("ESFUERZOREAL");

                entity.Property(e => e.IdExterno)
                    .HasColumnName("ID_EXTERNO")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Observacion)
                    .HasColumnName("OBSERVACION")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Prioridad).HasColumnName("PRIORIDAD");

                entity.Property(e => e.Tiemporeal).HasColumnName("TIEMPOREAL");

                entity.HasOne(d => d.WflCopiaexp)
                    .WithOne(p => p.WflCopiaexpTemp)
                    .HasForeignKey<WflCopiaexpTemp>(d => new { d.Proyid, d.Versionid, d.Llaveexpid, d.Copiaexpid })
                    .HasConstraintName("FK_COPIAEXP_TEMP_COPIAEXP");
            });

            modelBuilder.Entity<WflCopiasporpersona>(entity =>
            {
                entity.HasKey(e => new { e.Personaid, e.Proyid, e.Versionid })
                    .HasName("PK_COPIASPORPERSONA");

                entity.ToTable("WFL_COPIASPORPERSONA");

                entity.Property(e => e.Personaid)
                    .HasColumnName("PERSONAID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Numcopias).HasColumnName("NUMCOPIAS");

                entity.HasOne(d => d.WflProy)
                    .WithMany(p => p.WflCopiasporpersona)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_COPIASPORPERSONA_PROY");
            });

            modelBuilder.Entity<WflDiag>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid })
                    .HasName("PK_DIAG");

                entity.ToTable("WFL_DIAG");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IdExterno)
                    .HasColumnName("ID_EXTERNO")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Lapso).HasColumnName("LAPSO");

                entity.Property(e => e.Nombre)
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.HasOne(d => d.WflProy)
                    .WithMany(p => p.WflDiag)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DIAG_PROY");
            });

            modelBuilder.Entity<WflEntidad>(entity =>
            {
                entity.HasKey(e => e.IdEntidad);

                entity.ToTable("WFL_ENTIDAD");

                entity.Property(e => e.IdEntidad)
                    .HasColumnName("ID_ENTIDAD")
                    .ValueGeneratedNever();

                entity.Property(e => e.Aplicacion)
                    .HasColumnName("APLICACION")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Asignar).HasColumnName("ASIGNAR");

                entity.Property(e => e.Clase)
                    .HasColumnName("CLASE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Compartir).HasColumnName("COMPARTIR");

                entity.Property(e => e.Editar).HasColumnName("EDITAR");

                entity.Property(e => e.Eliminar).HasColumnName("ELIMINAR");

                entity.Property(e => e.IdJerarquia)
                    .HasColumnName("ID_JERARQUIA")
                    .HasDefaultValueSql("('24CDD37B-4CCF-4647-8A38-DD2C39DBD6F9')");

                entity.Property(e => e.Leer).HasColumnName("LEER");

                entity.Property(e => e.Nombre)
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .HasColumnName("URL")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdJerarquiaNavigation)
                    .WithMany(p => p.WflEntidad)
                    .HasForeignKey(d => d.IdJerarquia)
                    .HasConstraintName("FK_WFL_ENTIDAD_WFL_JERARQUIA");
            });

            modelBuilder.Entity<WflEscucha>(entity =>
            {
                entity.HasKey(e => e.Idescucha);

                entity.ToTable("WFL_ESCUCHA");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Diagid, e.Tareaid, e.Proximaescucha })
                    .HasName("IDX_ESCUCHA_PROXIMAESCUCHA");

                entity.Property(e => e.Idescucha).HasColumnName("IDESCUCHA");

                entity.Property(e => e.Copiaexpid).HasColumnName("COPIAEXPID");

                entity.Property(e => e.Diagid)
                    .IsRequired()
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.DiagidDestino)
                    .HasColumnName("DIAGID_DESTINO")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Estadoescucha).HasColumnName("ESTADOESCUCHA");

                entity.Property(e => e.Estadoservicio)
                    .HasColumnName("ESTADOSERVICIO")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FlechaidDestino).HasColumnName("FLECHAID_DESTINO");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Llaveexpid)
                    .HasColumnName("LLAVEEXPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.NroErrDespacho).HasColumnName("NRO_ERR_DESPACHO");

                entity.Property(e => e.NroErrEvento).HasColumnName("NRO_ERR_EVENTO");

                entity.Property(e => e.Obs)
                    .HasColumnName("OBS")
                    .HasColumnType("text");

                entity.Property(e => e.Personaid)
                    .HasColumnName("PERSONAID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Proximaescucha)
                    .HasColumnName("PROXIMAESCUCHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Repeticiones)
                    .HasColumnName("REPETICIONES")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Tareaid).HasColumnName("TAREAID");

                entity.Property(e => e.Ultimaescucha)
                    .HasColumnName("ULTIMAESCUCHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");
            });

            modelBuilder.Entity<WflExp>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Llaveexpid })
                    .HasName("PK_EXP");

                entity.ToTable("WFL_EXP");

                entity.HasIndex(e => e.Fechainicio)
                    .HasName("IDX1_EXP");

                entity.HasIndex(e => new { e.Proyid, e.Llaveexpid })
                    .HasName("IDX2_EXP")
                    .IsUnique();

                entity.HasIndex(e => new { e.Llaveexpid, e.Personaid, e.Proyid })
                    .HasName("IDX2_WFL_EXP");

                entity.HasIndex(e => new { e.Fechainicio, e.Fechafin, e.Personaid, e.Proyid })
                    .HasName("IDX1_WFL_EXP");

                entity.HasIndex(e => new { e.Llaveexpid, e.Proyid, e.Versionid, e.Personaid })
                    .HasName("WFL_EXP_idx020");

                entity.HasIndex(e => new { e.Fechainicio, e.Fechafin, e.Proyid, e.Versionid, e.Llaveexpid, e.Personaid })
                    .HasName("WFL_EXP_idx021");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Llaveexpid)
                    .HasColumnName("LLAVEEXPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoReg)
                    .IsRequired()
                    .HasColumnName("ESTADO_REG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecEstadoReg)
                    .HasColumnName("FEC_ESTADO_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fechafin)
                    .HasColumnName("FECHAFIN")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fechainicio)
                    .HasColumnName("FECHAINICIO")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Obs)
                    .HasColumnName("OBS")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Personaid)
                    .IsRequired()
                    .HasColumnName("PERSONAID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Tipoexpid)
                    .IsRequired()
                    .HasColumnName("TIPOEXPID")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.HasOne(d => d.Tipoexp)
                    .WithMany(p => p.WflExp)
                    .HasForeignKey(d => d.Tipoexpid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EXP_TIPOEXP");
            });

            modelBuilder.Entity<WflFlecha>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Flechaid })
                    .HasName("PK_FLECHA");

                entity.ToTable("WFL_FLECHA");

                entity.HasIndex(e => e.Nombre)
                    .HasName("IDX1_FLECHA");

                entity.HasIndex(e => e.Tipo)
                    .HasName("IDX2_FLECHA");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Flechaid).HasColumnName("FLECHAID");

                entity.Property(e => e.DiagidDestino)
                    .HasColumnName("DIAGID_DESTINO")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.IdExterno)
                    .HasColumnName("ID_EXTERNO")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TareaidDestino).HasColumnName("TAREAID_DESTINO");

                entity.Property(e => e.TareaidOrigen).HasColumnName("TAREAID_ORIGEN");

                entity.Property(e => e.Tipo).HasColumnName("TIPO");

                entity.HasOne(d => d.WflDiag)
                    .WithMany(p => p.WflFlecha)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.Diagid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FLECHA_DIAG");

                entity.HasOne(d => d.WflTarea)
                    .WithMany(p => p.WflFlechaWflTarea)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.Diagid, d.TareaidOrigen })
                    .HasConstraintName("FK_FLECHA_TAREA1");

                entity.HasOne(d => d.WflTareaNavigation)
                    .WithMany(p => p.WflFlechaWflTareaNavigation)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.DiagidDestino, d.TareaidDestino })
                    .HasConstraintName("FK_FLECHA_TAREA2");
            });

            modelBuilder.Entity<WflFlechaalarma>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Flechaid })
                    .HasName("PK_FLECHAALARMA");

                entity.ToTable("WFL_FLECHAALARMA");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Flechaid).HasColumnName("FLECHAID");

                entity.Property(e => e.DiagidAlarmado)
                    .HasColumnName("DIAGID_ALARMADO")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Plazoalarma).HasColumnName("PLAZOALARMA");

                entity.Property(e => e.Rolid)
                    .IsRequired()
                    .HasColumnName("ROLID")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.HasOne(d => d.WflFlecha)
                    .WithOne(p => p.WflFlechaalarma)
                    .HasForeignKey<WflFlechaalarma>(d => new { d.Proyid, d.Versionid, d.Diagid, d.Flechaid })
                    .HasConstraintName("FK_FLECHAALARMA_FLECHA");
            });

            modelBuilder.Entity<WflFlechaflujo>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Flechaid })
                    .HasName("PK_FLECHAFLUJO");

                entity.ToTable("WFL_FLECHAFLUJO");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Diagid, e.Flechaid, e.Tipoexpinic })
                    .HasName("IDX1_FLECHAFLUJO");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Flechaid).HasColumnName("FLECHAID");

                entity.Property(e => e.AtributosXml)
                    .HasColumnName("ATRIBUTOS_XML")
                    .HasColumnType("text");

                entity.Property(e => e.Flujoportpovdo).HasColumnName("FLUJOPORTPOVDO");

                entity.Property(e => e.Terminatarea).HasColumnName("TERMINATAREA");

                entity.Property(e => e.Tipoexpid)
                    .IsRequired()
                    .HasColumnName("TIPOEXPID")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Tipoexpinic).HasColumnName("TIPOEXPINIC");

                entity.HasOne(d => d.WflFlecha)
                    .WithOne(p => p.WflFlechaflujo)
                    .HasForeignKey<WflFlechaflujo>(d => new { d.Proyid, d.Versionid, d.Diagid, d.Flechaid })
                    .HasConstraintName("FK_FLECHAFLUJO_FLECHA");
            });

            modelBuilder.Entity<WflGrupo>(entity =>
            {
                entity.HasKey(e => e.IdGrupo);

                entity.ToTable("WFL_GRUPO");

                entity.HasIndex(e => e.CodGrupo)
                    .IsUnique();

                entity.HasIndex(e => e.IdGrupo)
                    .HasName("IX_WFL_GRUPO")
                    .IsUnique();

                entity.Property(e => e.IdGrupo)
                    .HasColumnName("ID_GRUPO")
                    .ValueGeneratedNever();

                entity.Property(e => e.CodGrupo)
                    .IsRequired()
                    .HasColumnName("COD_GRUPO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdGrupoSharepoint)
                    .HasColumnName("ID_GRUPO_SHAREPOINT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NombreGrupoSharepoint)
                    .HasColumnName("NOMBRE_GRUPO_SHAREPOINT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UrlSharepoint)
                    .HasColumnName("URL_SHAREPOINT")
                    .HasMaxLength(300)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WflGrupoProceso>(entity =>
            {
                entity.HasKey(e => e.IdGrupoProceso);

                entity.ToTable("WFL_GRUPO_PROCESO");

                entity.HasIndex(e => new { e.IdGrupo, e.Procesoid })
                    .HasName("IX_WFL_GRUPO_PROCESO")
                    .IsUnique();

                entity.Property(e => e.IdGrupoProceso)
                    .HasColumnName("ID_GRUPO_PROCESO")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdGrupo).HasColumnName("ID_GRUPO");

                entity.Property(e => e.Procesoid).HasColumnName("PROCESOID");

                entity.HasOne(d => d.IdGrupoNavigation)
                    .WithMany(p => p.WflGrupoProceso)
                    .HasForeignKey(d => d.IdGrupo)
                    .HasConstraintName("FK_WFL_GRUPO_PROCESO_WFL_GRUPO");

                entity.HasOne(d => d.Proceso)
                    .WithMany(p => p.WflGrupoProceso)
                    .HasForeignKey(d => d.Procesoid)
                    .HasConstraintName("FK_WFL_GRUPO_PROCESO_WFL_PROC");
            });

            modelBuilder.Entity<WflGrupoRegion>(entity =>
            {
                entity.HasKey(e => e.IdGrupoRegion);

                entity.ToTable("WFL_GRUPO_REGION");

                entity.HasIndex(e => new { e.IdGrupo, e.IdRegion })
                    .HasName("IX_WFL_GRUPO_REGION")
                    .IsUnique();

                entity.Property(e => e.IdGrupoRegion)
                    .HasColumnName("ID_GRUPO_REGION")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdGrupo).HasColumnName("ID_GRUPO");

                entity.Property(e => e.IdRegion)
                    .IsRequired()
                    .HasColumnName("ID_REGION")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdGrupoNavigation)
                    .WithMany(p => p.WflGrupoRegion)
                    .HasForeignKey(d => d.IdGrupo)
                    .HasConstraintName("FK_WFL_GRUPO_REGION_WFL_GRUPO");

                entity.HasOne(d => d.IdRegionNavigation)
                    .WithMany(p => p.WflGrupoRegion)
                    .HasForeignKey(d => d.IdRegion)
                    .HasConstraintName("FK_WFL_GRUPO_REGION_WFL_REGION");
            });

            modelBuilder.Entity<WflGrupoUsuario>(entity =>
            {
                entity.HasKey(e => e.IdGrupoUsuario);

                entity.ToTable("WFL_GRUPO_USUARIO");

                entity.HasIndex(e => new { e.IdGrupo, e.IdAdm, e.IdUsuario })
                    .HasName("IX_WFL_GRUPO_USUARIO")
                    .IsUnique();

                entity.Property(e => e.IdGrupoUsuario)
                    .HasColumnName("ID_GRUPO_USUARIO")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdGrupo).HasColumnName("ID_GRUPO");

                entity.Property(e => e.IdUsuario)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Lider).HasColumnName("LIDER");

                entity.HasOne(d => d.IdGrupoNavigation)
                    .WithMany(p => p.WflGrupoUsuario)
                    .HasForeignKey(d => d.IdGrupo)
                    .HasConstraintName("FK_WFL_GRUPO_USUARIO_WFL_GRUPO");

                entity.HasOne(d => d.Id)
                    .WithMany(p => p.WflGrupoUsuario)
                    .HasForeignKey(d => new { d.IdUsuario, d.IdAdm })
                    .HasConstraintName("FK_WFL_GRUPO_USUARIO_USU_USUARIOS");
            });

            modelBuilder.Entity<WflJerarquia>(entity =>
            {
                entity.HasKey(e => e.IdJerarquia);

                entity.ToTable("WFL_JERARQUIA");

                entity.HasIndex(e => e.Nombre)
                    .IsUnique();

                entity.Property(e => e.IdJerarquia)
                    .HasColumnName("ID_JERARQUIA")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdEntidad).HasColumnName("ID_ENTIDAD");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Usumultnodo)
                    .HasColumnName("USUMULTNODO")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.IdEntidadNavigation)
                    .WithMany(p => p.WflJerarquia)
                    .HasForeignKey(d => d.IdEntidad)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_WFL_JERARQUIA_ENTIDAD");
            });

            modelBuilder.Entity<WflMensaje>(entity =>
            {
                entity.HasKey(e => e.IdMensaje);

                entity.ToTable("WFL_MENSAJE");

                entity.Property(e => e.IdMensaje)
                    .HasColumnName("ID_MENSAJE")
                    .ValueGeneratedNever();

                entity.Property(e => e.FecExpiracion)
                    .HasColumnName("FEC_EXPIRACION")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Texto)
                    .HasColumnName("TEXTO")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasColumnName("TIPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Xml)
                    .HasColumnName("XML")
                    .HasColumnType("ntext");
            });

            modelBuilder.Entity<WflOpcion>(entity =>
            {
                entity.HasKey(e => e.IdOpcion);

                entity.ToTable("WFL_OPCION");

                entity.Property(e => e.IdOpcion)
                    .HasColumnName("ID_OPCION")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Eseditable).HasColumnName("ESEDITABLE");

                entity.Property(e => e.Tipo)
                    .HasColumnName("TIPO")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WflOpcionUsuario>(entity =>
            {
                entity.HasKey(e => new { e.IdUsuario, e.IdOpcion, e.Llave })
                    .HasName("PK_OPCION");

                entity.ToTable("WFL_OPCION_USUARIO");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdOpcion)
                    .HasColumnName("ID_OPCION")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Llave)
                    .HasColumnName("LLAVE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Valor)
                    .HasColumnName("VALOR")
                    .HasMaxLength(2000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WflParametrogeneral>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Llave });

                entity.ToTable("WFL_PARAMETROGENERAL");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Llave)
                    .HasColumnName("LLAVE")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Valor)
                    .IsRequired()
                    .HasColumnName("VALOR")
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<WflPlantilla>(entity =>
            {
                entity.HasKey(e => e.IdPlantilla);

                entity.ToTable("WFL_PLANTILLA");

                entity.Property(e => e.IdPlantilla)
                    .HasColumnName("ID_PLANTILLA")
                    .ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Plantilla)
                    .HasColumnName("PLANTILLA")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<WflProc>(entity =>
            {
                entity.HasKey(e => e.Procesoid);

                entity.ToTable("WFL_PROC");

                entity.HasIndex(e => new { e.Proyid, e.Procesoid })
                    .HasName("WFL_PROC_idx02");

                entity.Property(e => e.Procesoid).HasColumnName("PROCESOID");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Diagid)
                    .IsRequired()
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.HasOne(d => d.WflDiag)
                    .WithMany(p => p.WflProc)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.Diagid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WFL_PROC_WFL_DIAG");
            });

            modelBuilder.Entity<WflProy>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid })
                    .HasName("PK_PROY");

                entity.ToTable("WFL_PROY");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Creador)
                    .HasColumnName("CREADOR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("FECHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasColumnName("OBSERVACIONES")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WflRegion>(entity =>
            {
                entity.HasKey(e => e.IdRegion);

                entity.ToTable("WFL_REGION");

                entity.Property(e => e.IdRegion)
                    .HasColumnName("ID_REGION")
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WflRegistro>(entity =>
            {
                entity.HasKey(e => e.Registroid)
                    .HasName("PK_REGISTRO");

                entity.ToTable("WFL_REGISTRO");

                entity.HasIndex(e => e.IdExterno)
                    .HasName("IX_WFL_REGISTRO_05");

                entity.HasIndex(e => e.Tipo)
                    .HasName("IX_WFL_REGISTRO_07");

                entity.HasIndex(e => new { e.Tipo, e.Fecha })
                    .HasName("IX_WFL_REGISTRO_04");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Diagid })
                    .HasName("IDX4_WFL_REGISTRO");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Fecha })
                    .HasName("IDX5_REGISTRO");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid })
                    .HasName("IDX2_REGISTRO");

                entity.HasIndex(e => new { e.Llaveexpid, e.Proyid, e.Registroid, e.Versionid })
                    .HasName("IDX1_WFL_REGISTRO");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid })
                    .HasName("IX_WFL_REGISTRO_02");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.CopiaexpidPadre })
                    .HasName("IDX1_REGISTRO");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid, e.Fecha })
                    .HasName("IX_WFL_REGISTRO_03");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.PersonaidDestino, e.RolidDestino })
                    .HasName("IDX4_REGISTRO");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.PersonaidOrigen, e.Registroid })
                    .HasName("IDX5_WFL_REGISTRO");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Llaveexpid, e.PersonaidOrigen, e.RolidOrigen })
                    .HasName("IDX3_REGISTRO");

                entity.HasIndex(e => new { e.Copiaexpid, e.Diagid, e.Fecha, e.Llaveexpid, e.Proyid, e.Versionid, e.TareaidOrigen })
                    .HasName("IDX3_WFL_REGISTRO");

                entity.HasIndex(e => new { e.Copiaexpid, e.Diagid, e.Fecha, e.Llaveexpid, e.Proyid, e.Registroid, e.Versionid, e.TareaidOrigen })
                    .HasName("IDX2_WFL_REGISTRO");

                entity.HasIndex(e => new { e.Diagid, e.Tipo, e.Proyid, e.Versionid, e.Copiaexpid, e.Fecha, e.Llaveexpid, e.TareaidOrigen })
                    .HasName("IDX6_WFL_REGISTRO");

                entity.HasIndex(e => new { e.Diagid, e.Llaveexpid, e.Copiaexpid, e.PersonaidOrigen, e.TareaidOrigen, e.Asunto, e.Proyid, e.Versionid, e.Registroid })
                    .HasName("IX_WFL_REGISTRO_06");

                entity.HasIndex(e => new { e.Registroid, e.Tipo, e.PersonaidOrigen, e.TipoDoc, e.NumDoc, e.Entidad, e.Monto, e.CentroCosto, e.Referencia1, e.Referencia2, e.Referencia3, e.Prioridad, e.Fecha1, e.Fecha2, e.EsfuerzoEsperado, e.TiempoEsperado, e.Asunto, e.Proyid, e.Versionid, e.Llaveexpid, e.Copiaexpid, e.Diagid, e.TareaidOrigen, e.Fecha })
                    .HasName("WFL_REGISTRO_idx020");

                entity.Property(e => e.Registroid).HasColumnName("REGISTROID");

                entity.Property(e => e.Asunto)
                    .HasColumnName("ASUNTO")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.AtributosXml)
                    .HasColumnName("ATRIBUTOS_XML")
                    .HasColumnType("text");

                entity.Property(e => e.CentroCosto)
                    .HasColumnName("CENTRO_COSTO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Copiaexpid).HasColumnName("COPIAEXPID");

                entity.Property(e => e.CopiaexpidPadre).HasColumnName("COPIAEXPID_PADRE");

                entity.Property(e => e.Diagid)
                    .IsRequired()
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Entidad)
                    .HasColumnName("ENTIDAD")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.EsfuerzoEsperado).HasColumnName("ESFUERZO_ESPERADO");

                entity.Property(e => e.Esfuerzoreal).HasColumnName("ESFUERZOREAL");

                entity.Property(e => e.Fecha)
                    .HasColumnName("FECHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecha1)
                    .HasColumnName("FECHA_1")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecha2)
                    .HasColumnName("FECHA_2")
                    .HasColumnType("datetime");

                entity.Property(e => e.Flechaid).HasColumnName("FLECHAID");

                entity.Property(e => e.IdExterno)
                    .HasColumnName("ID_EXTERNO")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Llaveexpid)
                    .IsRequired()
                    .HasColumnName("LLAVEEXPID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Monto)
                    .HasColumnName("MONTO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NumDoc).HasColumnName("NUM_DOC");

                entity.Property(e => e.Observacion)
                    .HasColumnName("OBSERVACION")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PersonaidDestino)
                    .HasColumnName("PERSONAID_DESTINO")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.PersonaidOrigen)
                    .HasColumnName("PERSONAID_ORIGEN")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Prioridad).HasColumnName("PRIORIDAD");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Referencia1)
                    .HasColumnName("REFERENCIA_1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Referencia2)
                    .HasColumnName("REFERENCIA_2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Referencia3)
                    .HasColumnName("REFERENCIA_3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasColumnName("REGION")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.RolidDestino)
                    .HasColumnName("ROLID_DESTINO")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.RolidOrigen)
                    .HasColumnName("ROLID_ORIGEN")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.TareaidDestino).HasColumnName("TAREAID_DESTINO");

                entity.Property(e => e.TareaidOrigen).HasColumnName("TAREAID_ORIGEN");

                entity.Property(e => e.TiempoEsperado).HasColumnName("TIEMPO_ESPERADO");

                entity.Property(e => e.Tiemporeal).HasColumnName("TIEMPOREAL");

                entity.Property(e => e.Tipo).HasColumnName("TIPO");

                entity.Property(e => e.TipoDoc)
                    .HasColumnName("TIPO_DOC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");
            });

            modelBuilder.Entity<WflSubprocesoAPublicar>(entity =>
            {
                entity.HasKey(e => e.Idsubpub)
                    .HasName("PK_WFL_SUBPUB");

                entity.ToTable("WFL_SUBPROCESO_A_PUBLICAR");

                entity.Property(e => e.Idsubpub).HasColumnName("IDSUBPUB");

                entity.Property(e => e.DescArchivo)
                    .HasColumnName("DESC_ARCHIVO")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Diagid)
                    .IsRequired()
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("ESTADO")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FecCambioEstado)
                    .HasColumnName("FEC_CAMBIO_ESTADO")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecIngReg)
                    .HasColumnName("FEC_ING_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecUltModifReg)
                    .HasColumnName("FEC_ULT_MODIF_REG")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdExterno)
                    .IsRequired()
                    .HasColumnName("ID_EXTERNO")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IdFuncionUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_FUNCION_ULT_MODIF_REG")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioIngReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ING_REG")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuarioUltModifReg)
                    .IsRequired()
                    .HasColumnName("ID_USUARIO_ULT_MODIF_REG")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.NombreArchivo)
                    .HasColumnName("NOMBRE_ARCHIVO")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Origen)
                    .IsRequired()
                    .HasColumnName("ORIGEN")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");
            });

            modelBuilder.Entity<WflSubprocesoAPublicarExt>(entity =>
            {
                entity.HasKey(e => e.Idsubpub)
                    .HasName("PK_WFL_SUBPUB_EXT");

                entity.ToTable("WFL_SUBPROCESO_A_PUBLICAR_EXT");

                entity.Property(e => e.Idsubpub)
                    .HasColumnName("IDSUBPUB")
                    .ValueGeneratedNever();

                entity.Property(e => e.ArchivoXpdl)
                    .HasColumnName("ARCHIVO_XPDL")
                    .HasColumnType("text");

                entity.Property(e => e.Diagrama)
                    .IsRequired()
                    .HasColumnName("DIAGRAMA")
                    .HasColumnType("text");

                entity.HasOne(d => d.IdsubpubNavigation)
                    .WithOne(p => p.WflSubprocesoAPublicarExt)
                    .HasForeignKey<WflSubprocesoAPublicarExt>(d => d.Idsubpub)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WFL_SUBPROCESO_A_PUBLICAR_EXT_WFL_SUBPROCESO_A_PUBLICAR");
            });

            modelBuilder.Entity<WflTarea>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Tareaid })
                    .HasName("PK_TAREA");

                entity.ToTable("WFL_TAREA");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Tipo })
                    .HasName("IDX2_TAREA");

                entity.HasIndex(e => new { e.Tareaid, e.Proyid, e.Versionid })
                    .HasName("IDX1_WFL_TAREA");

                entity.HasIndex(e => new { e.Versionid, e.Diagid, e.Tareaid })
                    .HasName("IDX2_WFL_TAREA");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Diagid, e.Tareaid, e.Tipo })
                    .HasName("WFL_TAREA_idx010");

                entity.HasIndex(e => new { e.Nombre, e.Proyid, e.Versionid, e.Diagid, e.Tareaid, e.Procesonegocio, e.Modulo })
                    .HasName("IDX3_TAREA");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Tareaid).HasColumnName("TAREAID");

                entity.Property(e => e.Adjunto).HasColumnName("ADJUNTO");

                entity.Property(e => e.Asunto)
                    .HasColumnName("ASUNTO")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.CambioPrioridad)
                    .HasColumnName("CAMBIO_PRIORIDAD")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CentroCosto)
                    .HasColumnName("CENTRO_COSTO")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.Coordenadas)
                    .HasColumnName("COORDENADAS")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Esreasignable).HasColumnName("ESREASIGNABLE");

                entity.Property(e => e.FechaVenc)
                    .HasColumnName("FECHA_VENC")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.IdExterno)
                    .HasColumnName("ID_EXTERNO")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Modulo)
                    .HasColumnName("MODULO")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Nota)
                    .HasColumnName("NOTA")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Prioridad).HasColumnName("PRIORIDAD");

                entity.Property(e => e.Procesonegocio)
                    .HasColumnName("PROCESONEGOCIO")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Proposito)
                    .HasColumnName("PROPOSITO")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Referencia1)
                    .HasColumnName("REFERENCIA_1")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Referencia2)
                    .HasColumnName("REFERENCIA_2")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Referencia3)
                    .HasColumnName("REFERENCIA_3")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Rolid)
                    .HasColumnName("ROLID")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Subtipo).HasColumnName("SUBTIPO");

                entity.Property(e => e.TareaidPadre).HasColumnName("TAREAID_PADRE");

                entity.Property(e => e.Tipo).HasColumnName("TIPO");

                entity.HasOne(d => d.WflDiag)
                    .WithMany(p => p.WflTarea)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.Diagid })
                    .HasConstraintName("FK_TAREA_DIAG");
            });

            modelBuilder.Entity<WflTareaRegion>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Tareaid, e.Region });

                entity.ToTable("WFL_TAREA_REGION");

                entity.HasIndex(e => e.Claseescucha)
                    .HasName("IDX_TAREA_REGION_CLASEESCUCHA");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Diagid, e.Tareaid, e.Region, e.Postcondicionmultiple })
                    .HasName("IDX_TAREA_REGION_POSTCONDICIONMULTIPLE");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Tareaid).HasColumnName("TAREAID");

                entity.Property(e => e.Region)
                    .HasColumnName("REGION")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.AtributosXml)
                    .HasColumnName("ATRIBUTOS_XML")
                    .HasColumnType("text");

                entity.Property(e => e.Clase)
                    .HasColumnName("CLASE")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Clasecomp)
                    .HasColumnName("CLASECOMP")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Claseescucha)
                    .HasColumnName("CLASEESCUCHA")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Claseformaasig)
                    .HasColumnName("CLASEFORMAASIG")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Clasegenid)
                    .HasColumnName("CLASEGENID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Clasemanreg)
                    .HasColumnName("CLASEMANREG")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Clasepostcondicion)
                    .HasColumnName("CLASEPOSTCONDICION")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Claseprecondicion)
                    .HasColumnName("CLASEPRECONDICION")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.CorreoAutomatico).HasColumnName("CORREO_AUTOMATICO");

                entity.Property(e => e.CorreoRequerido).HasColumnName("CORREO_REQUERIDO");

                entity.Property(e => e.EnviarCorreo).HasColumnName("ENVIAR_CORREO");

                entity.Property(e => e.Metodo)
                    .HasColumnName("METODO")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Metodocomp)
                    .HasColumnName("METODOCOMP")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Metodoescucha)
                    .HasColumnName("METODOESCUCHA")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Metodoformaasig)
                    .HasColumnName("METODOFORMAASIG")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.Metodogenid)
                    .HasColumnName("METODOGENID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Metodomanreg)
                    .HasColumnName("METODOMANREG")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Metodopostcondicion)
                    .HasColumnName("METODOPOSTCONDICION")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Metodoprecondicion)
                    .HasColumnName("METODOPRECONDICION")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Postcondicionmultiple).HasColumnName("POSTCONDICIONMULTIPLE");

                entity.HasOne(d => d.WflTarea)
                    .WithMany(p => p.WflTareaRegion)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.Diagid, d.Tareaid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TAREA_REGION_TAREA");
            });

            modelBuilder.Entity<WflTareacomp>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Tareaid, e.IdRol })
                    .HasName("PK_TAREACOMP");

                entity.ToTable("WFL_TAREACOMP");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Tareaid).HasColumnName("TAREAID");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Modo).HasColumnName("MODO");

                entity.Property(e => e.Todos).HasColumnName("TODOS");

                entity.HasOne(d => d.WflTarea)
                    .WithMany(p => p.WflTareacomp)
                    .HasForeignKey(d => new { d.Proyid, d.Versionid, d.Diagid, d.Tareaid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WFL_TAREACOMP_WFL_TAREA");
            });

            modelBuilder.Entity<WflTareaconector>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Tareaid })
                    .HasName("PK_TAREACONECTOR");

                entity.ToTable("WFL_TAREACONECTOR");

                entity.HasIndex(e => new { e.Proyid, e.Versionid, e.Diagid, e.FlechaidDestino })
                    .HasName("IDX1_TAREACONECTOR");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Tareaid).HasColumnName("TAREAID");

                entity.Property(e => e.DiagidDestino)
                    .HasColumnName("DIAGID_DESTINO")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.FlechaidDestino)
                    .HasColumnName("FLECHAID_DESTINO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdExterno)
                    .HasColumnName("ID_EXTERNO")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.HasOne(d => d.WflTarea)
                    .WithOne(p => p.WflTareaconector)
                    .HasForeignKey<WflTareaconector>(d => new { d.Proyid, d.Versionid, d.Diagid, d.Tareaid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TAREACONECTOR_TAREA");
            });

            modelBuilder.Entity<WflTareaescucha>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Tareaid })
                    .HasName("PK_TAREAESCUCHA");

                entity.ToTable("WFL_TAREAESCUCHA");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Tareaid).HasColumnName("TAREAID");

                entity.Property(e => e.Asincrono)
                    .HasColumnName("ASINCRONO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Frecuenciarevision)
                    .HasColumnName("FRECUENCIAREVISION")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.HasOne(d => d.WflTarea)
                    .WithOne(p => p.WflTareaescucha)
                    .HasForeignKey<WflTareaescucha>(d => new { d.Proyid, d.Versionid, d.Diagid, d.Tareaid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TAREAESCUCHA_TAREA");
            });

            modelBuilder.Entity<WflTareageneral>(entity =>
            {
                entity.HasKey(e => new { e.Proyid, e.Versionid, e.Diagid, e.Tareaid })
                    .HasName("PK_TAREAGENERAL");

                entity.ToTable("WFL_TAREAGENERAL");

                entity.Property(e => e.Proyid).HasColumnName("PROYID");

                entity.Property(e => e.Versionid).HasColumnName("VERSIONID");

                entity.Property(e => e.Diagid)
                    .HasColumnName("DIAGID")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Tareaid).HasColumnName("TAREAID");

                entity.Property(e => e.CodGrupo)
                    .HasColumnName("COD_GRUPO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CorreoConfig).HasColumnName("CORREO_CONFIG");

                entity.Property(e => e.CorreoConfigXml)
                    .HasColumnName("CORREO_CONFIG_XML")
                    .HasColumnType("text");

                entity.Property(e => e.Editararchivos).HasColumnName("EDITARARCHIVOS");

                entity.Property(e => e.Editardocumentos).HasColumnName("EDITARDOCUMENTOS");

                entity.Property(e => e.Editaresfreal)
                    .HasColumnName("EDITARESFREAL")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Editarobs)
                    .HasColumnName("EDITAROBS")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Esautoasignable).HasColumnName("ESAUTOASIGNABLE");

                entity.Property(e => e.Esfuerzoesperado).HasColumnName("ESFUERZOESPERADO");

                entity.Property(e => e.Formaasig).HasColumnName("FORMAASIG");

                entity.Property(e => e.Formacomp).HasColumnName("FORMACOMP");

                entity.Property(e => e.Permiteanular).HasColumnName("PERMITEANULAR");

                entity.Property(e => e.Permiteanularinicia).HasColumnName("PERMITEANULARINICIA");

                entity.Property(e => e.Reqautoasignar).HasColumnName("REQAUTOASIGNAR");

                entity.Property(e => e.Tiempoesperado).HasColumnName("TIEMPOESPERADO");

                entity.Property(e => e.Verarchivos).HasColumnName("VERARCHIVOS");

                entity.Property(e => e.Verdocumentos).HasColumnName("VERDOCUMENTOS");

                entity.HasOne(d => d.WflTarea)
                    .WithOne(p => p.WflTareageneral)
                    .HasForeignKey<WflTareageneral>(d => new { d.Proyid, d.Versionid, d.Diagid, d.Tareaid })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TAREAGENERAL_TAREA");
            });

            modelBuilder.Entity<WflTestescucha>(entity =>
            {
                entity.HasKey(e => e.TestId);

                entity.ToTable("WFL_TESTESCUCHA");

                entity.Property(e => e.TestId).HasColumnName("TEST_ID");

                entity.Property(e => e.Paso)
                    .IsRequired()
                    .HasColumnName("PASO")
                    .HasMaxLength(64)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WflTipoexp>(entity =>
            {
                entity.HasKey(e => e.Tipoexpid)
                    .HasName("PK_TIPOEXP");

                entity.ToTable("WFL_TIPOEXP");

                entity.Property(e => e.Tipoexpid)
                    .HasColumnName("TIPOEXPID")
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AtributosXml)
                    .HasColumnName("ATRIBUTOS_XML")
                    .HasColumnType("text");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WflUnidad>(entity =>
            {
                entity.HasKey(e => e.IdUnidad)
                    .HasName("PK_WFL_UNIDAD_1");

                entity.ToTable("WFL_UNIDAD");

                entity.Property(e => e.IdUnidad)
                    .HasColumnName("ID_UNIDAD")
                    .ValueGeneratedNever();

                entity.Property(e => e.CodUnidad)
                    .HasColumnName("COD_UNIDAD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdJerarquia).HasColumnName("ID_JERARQUIA");

                entity.Property(e => e.IdPadre).HasColumnName("ID_PADRE");

                entity.Property(e => e.Nombre)
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Orden)
                    .HasColumnName("ORDEN")
                    .ValueGeneratedOnAdd();

                entity.HasOne(d => d.IdJerarquiaNavigation)
                    .WithMany(p => p.WflUnidad)
                    .HasForeignKey(d => d.IdJerarquia)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_WFL_UNIDAD_WFL_JERARQUIA");
            });

            modelBuilder.Entity<WflUnidadAtrEntidad>(entity =>
            {
                entity.HasKey(e => e.IdUnidadAtrEntidad)
                    .HasName("PK_WFL_UNIDAD_ATR_ENTIDAD_1");

                entity.ToTable("WFL_UNIDAD_ATR_ENTIDAD");

                entity.Property(e => e.IdUnidadAtrEntidad)
                    .HasColumnName("ID_UNIDAD_ATR_ENTIDAD")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdAtrEntidad).HasColumnName("ID_ATR_ENTIDAD");

                entity.Property(e => e.IdUnidad).HasColumnName("ID_UNIDAD");

                entity.Property(e => e.Valor)
                    .HasColumnName("VALOR")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAtrEntidadNavigation)
                    .WithMany(p => p.WflUnidadAtrEntidad)
                    .HasForeignKey(d => d.IdAtrEntidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WFL_UNIDAD_ATR_ENTIDAD_WFL_ATR_ENTIDAD");

                entity.HasOne(d => d.IdUnidadNavigation)
                    .WithMany(p => p.WflUnidadAtrEntidad)
                    .HasForeignKey(d => d.IdUnidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WFL_UNIDAD_ATR_ENTIDAD_WFL_UNIDAD");
            });

            modelBuilder.Entity<WflUnidadRol>(entity =>
            {
                entity.HasKey(e => e.IdUnidadRol);

                entity.ToTable("WFL_UNIDAD_ROL");

                entity.Property(e => e.IdUnidadRol)
                    .HasColumnName("ID_UNIDAD_ROL")
                    .ValueGeneratedNever();

                entity.Property(e => e.Asignar).HasColumnName("ASIGNAR");

                entity.Property(e => e.Compartir).HasColumnName("COMPARTIR");

                entity.Property(e => e.Editar).HasColumnName("EDITAR");

                entity.Property(e => e.Eliminar).HasColumnName("ELIMINAR");

                entity.Property(e => e.IdEntidad).HasColumnName("ID_ENTIDAD");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Leer).HasColumnName("LEER");
            });

            modelBuilder.Entity<WflUnidadUsuario>(entity =>
            {
                entity.HasKey(e => e.IdUnidadUsuario);

                entity.ToTable("WFL_UNIDAD_USUARIO");

                entity.Property(e => e.IdUnidadUsuario)
                    .HasColumnName("ID_UNIDAD_USUARIO")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdAdm).HasColumnName("ID_ADM");

                entity.Property(e => e.IdUnidad).HasColumnName("ID_UNIDAD");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdUnidadNavigation)
                    .WithMany(p => p.WflUnidadUsuario)
                    .HasForeignKey(d => d.IdUnidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WFL_UNIDAD_USUARIO_WFL_UNIDAD");

                entity.HasOne(d => d.Id)
                    .WithMany(p => p.WflUnidadUsuario)
                    .HasForeignKey(d => new { d.IdUsuario, d.IdAdm })
                    .HasConstraintName("FK_WFL_UNIDAD_USUARIO_USU_USUARIOS");
            });
        }
    }
}
