﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflCopiaexpTemp
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Llaveexpid { get; set; }
        public int Copiaexpid { get; set; }
        public string IdExterno { get; set; }
        public int? Esfuerzoreal { get; set; }
        public int? Tiemporeal { get; set; }
        public string Observacion { get; set; }
        public int? Prioridad { get; set; }

        public virtual WflCopiaexp WflCopiaexp { get; set; }
    }
}
