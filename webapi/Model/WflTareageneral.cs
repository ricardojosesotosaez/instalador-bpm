﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflTareageneral
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Tareaid { get; set; }
        public int Formaasig { get; set; }
        public int? Tiempoesperado { get; set; }
        public int? Esfuerzoesperado { get; set; }
        public int Verarchivos { get; set; }
        public int Editararchivos { get; set; }
        public int Editarobs { get; set; }
        public int Permiteanular { get; set; }
        public int Editaresfreal { get; set; }
        public int Formacomp { get; set; }
        public int Permiteanularinicia { get; set; }
        public string CodGrupo { get; set; }
        public int Reqautoasignar { get; set; }
        public int Esautoasignable { get; set; }
        public int Verdocumentos { get; set; }
        public int Editardocumentos { get; set; }
        public int CorreoConfig { get; set; }
        public string CorreoConfigXml { get; set; }

        public virtual WflTarea WflTarea { get; set; }
    }
}
