﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflPlantilla
    {
        public string Nombre { get; set; }
        public string Plantilla { get; set; }
        public Guid IdPlantilla { get; set; }
    }
}
