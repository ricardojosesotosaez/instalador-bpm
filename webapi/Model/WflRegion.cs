﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflRegion
    {
        public WflRegion()
        {
            WflGrupoRegion = new HashSet<WflGrupoRegion>();
        }

        public string IdRegion { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<WflGrupoRegion> WflGrupoRegion { get; set; }
    }
}
