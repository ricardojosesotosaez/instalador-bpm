﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflTareaescucha
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Tareaid { get; set; }
        public string Frecuenciarevision { get; set; }
        public int Asincrono { get; set; }

        public virtual WflTarea WflTarea { get; set; }
    }
}
