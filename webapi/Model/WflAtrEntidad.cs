﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflAtrEntidad
    {
        public WflAtrEntidad()
        {
            WflUnidadAtrEntidad = new HashSet<WflUnidadAtrEntidad>();
        }

        public Guid IdAtrEntidad { get; set; }
        public string CodAtrEntidad { get; set; }
        public Guid IdEntidad { get; set; }
        public string Nombre { get; set; }
        public string Clase { get; set; }
        public string Usercontrol { get; set; }
        public string Tipo { get; set; }

        public virtual WflEntidad IdEntidadNavigation { get; set; }
        public virtual ICollection<WflUnidadAtrEntidad> WflUnidadAtrEntidad { get; set; }
    }
}
