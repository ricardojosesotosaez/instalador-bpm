﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflCopiaexpLock
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Llaveexpid { get; set; }
        public int Copiaexpid { get; set; }
        public bool Lock { get; set; }
        public DateTime FechaLock { get; set; }
        public byte[] Version { get; set; }
    }
}
