﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflExp
    {
        public WflExp()
        {
            WflCopiaexp = new HashSet<WflCopiaexp>();
        }

        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Llaveexpid { get; set; }
        public string Tipoexpid { get; set; }
        public string Descripcion { get; set; }
        public string Obs { get; set; }
        public DateTime? Fechainicio { get; set; }
        public DateTime? Fechafin { get; set; }
        public string EstadoReg { get; set; }
        public DateTime FecEstadoReg { get; set; }
        public DateTime FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public string Personaid { get; set; }

        public virtual WflTipoexp Tipoexp { get; set; }
        public virtual ICollection<WflCopiaexp> WflCopiaexp { get; set; }
    }
}
