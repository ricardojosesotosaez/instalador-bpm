﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflTipoexp
    {
        public WflTipoexp()
        {
            WflExp = new HashSet<WflExp>();
        }

        public string Tipoexpid { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string AtributosXml { get; set; }

        public virtual ICollection<WflExp> WflExp { get; set; }
    }
}
