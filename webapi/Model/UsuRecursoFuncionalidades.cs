﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuRecursoFuncionalidades
    {
        public string IdRecurso { get; set; }
        public string IdFuncionalidad { get; set; }

        public virtual UsuFuncionalidades IdFuncionalidadNavigation { get; set; }
        public virtual UsuRecurso IdRecursoNavigation { get; set; }
    }
}
