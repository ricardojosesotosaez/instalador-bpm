﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuRecurso
    {
        public UsuRecurso()
        {
            UsuRecursoFuncionalidades = new HashSet<UsuRecursoFuncionalidades>();
        }

        public string IdRecurso { get; set; }
        public string Tipo { get; set; }
        public string Recurso { get; set; }

        public virtual ICollection<UsuRecursoFuncionalidades> UsuRecursoFuncionalidades { get; set; }
    }
}
