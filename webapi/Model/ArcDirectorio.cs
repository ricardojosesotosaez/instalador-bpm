﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class ArcDirectorio
    {
        public ArcDirectorio()
        {
            ArcArchivo = new HashSet<ArcArchivo>();
            InverseIdDirectorioPadreNavigation = new HashSet<ArcDirectorio>();
        }

        public int IdDirectorio { get; set; }
        public string Nombre { get; set; }
        public string Ruta { get; set; }
        public int? IdDirectorioPadre { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public string RutaVirtual { get; set; }
        public int? Idrepositorio { get; set; }

        public virtual ArcDirectorio IdDirectorioPadreNavigation { get; set; }
        public virtual ICollection<ArcArchivo> ArcArchivo { get; set; }
        public virtual ICollection<ArcDirectorio> InverseIdDirectorioPadreNavigation { get; set; }
    }
}
