﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuRoles
    {
        public UsuRoles()
        {
            UsuFuncionalidadesRoles = new HashSet<UsuFuncionalidadesRoles>();
            UsuPerfilesRoles = new HashSet<UsuPerfilesRoles>();
            UsuRolesProc = new HashSet<UsuRolesProc>();
            UsuUsuariosRoles = new HashSet<UsuUsuariosRoles>();
        }

        public short IdAdm { get; set; }
        public string IdRol { get; set; }
        public string IdProcesoNegocio { get; set; }
        public string Descripcion { get; set; }
        public string IndAdminPn { get; set; }
        public string TipoRolWorkflow { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }

        public virtual UsuProcesosnegocio IdProcesoNegocioNavigation { get; set; }
        public virtual ICollection<UsuFuncionalidadesRoles> UsuFuncionalidadesRoles { get; set; }
        public virtual ICollection<UsuPerfilesRoles> UsuPerfilesRoles { get; set; }
        public virtual ICollection<UsuRolesProc> UsuRolesProc { get; set; }
        public virtual ICollection<UsuUsuariosRoles> UsuUsuariosRoles { get; set; }
    }
}
