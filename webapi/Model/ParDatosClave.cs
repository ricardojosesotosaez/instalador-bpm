﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class ParDatosClave
    {
        public int IdAdm { get; set; }
        public int Id { get; set; }
        public int LargoMinimo { get; set; }
        public int LargoMaximo { get; set; }
        public int DiasExpira { get; set; }
        public int DiasAviso { get; set; }
        public int NumIntentos { get; set; }
        public int DiasInactivo { get; set; }
        public string EstadoReg { get; set; }
        public DateTime FecEstadoReg { get; set; }
        public DateTime FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
    }
}
