﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflFlechaflujo
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Flechaid { get; set; }
        public int Terminatarea { get; set; }
        public int Flujoportpovdo { get; set; }
        public string Tipoexpid { get; set; }
        public int? Tipoexpinic { get; set; }
        public string AtributosXml { get; set; }

        public virtual WflFlecha WflFlecha { get; set; }
    }
}
