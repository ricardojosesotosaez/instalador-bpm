﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflCopiaexp
    {
        public WflCopiaexp()
        {
            WflCopiaexpComp = new HashSet<WflCopiaexpComp>();
        }

        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Llaveexpid { get; set; }
        public int Copiaexpid { get; set; }
        public string Diagid { get; set; }
        public int? Tareaid { get; set; }
        public int Asignada { get; set; }
        public int Esdespachable { get; set; }
        public DateTime Fecha { get; set; }
        public int Pendpordesp { get; set; }
        public string Personaid { get; set; }
        public string Obs { get; set; }
        public string Descripcion { get; set; }
        public string Tipoexpid { get; set; }
        public string Region { get; set; }
        public DateTime? Fechavenc { get; set; }
        public string EstadoReg { get; set; }
        public byte[] Version { get; set; }
        public int NroErrServicio { get; set; }
        public DateTime FecEstadoReg { get; set; }
        public DateTime FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public int? EsfuerzoEsperado { get; set; }
        public int? TiempoEsperado { get; set; }
        public string CodGrupo { get; set; }

        public virtual WflExp WflExp { get; set; }
        public virtual WflTarea WflTarea { get; set; }
        public virtual WflAtributos WflAtributos { get; set; }
        public virtual WflCopiaexpTemp WflCopiaexpTemp { get; set; }
        public virtual ICollection<WflCopiaexpComp> WflCopiaexpComp { get; set; }
    }
}
