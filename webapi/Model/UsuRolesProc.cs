﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuRolesProc
    {
        public short IdAdm { get; set; }
        public int Procesoid { get; set; }
        public string IdRol { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public virtual UsuRoles Id { get; set; }
        public virtual WflProc Proceso { get; set; }
    }
}
