﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflBusquedaFiltro
    {
        public Guid IdBusquedaFiltro { get; set; }
        public Guid? IdBusqueda { get; set; }
        public int Orden { get; set; }
        public string IdCampo { get; set; }
        public string IdFiltro { get; set; }
        public string Valor1 { get; set; }
        public string Valor2 { get; set; }

        public virtual WflBusqueda IdBusquedaNavigation { get; set; }
    }
}
