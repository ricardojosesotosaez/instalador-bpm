﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflOpcionUsuario
    {
        public string IdUsuario { get; set; }
        public string IdOpcion { get; set; }
        public string Llave { get; set; }
        public string Valor { get; set; }
    }
}
