﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflGrupoRegion
    {
        public Guid IdGrupoRegion { get; set; }
        public Guid IdGrupo { get; set; }
        public string IdRegion { get; set; }

        public virtual WflGrupo IdGrupoNavigation { get; set; }
        public virtual WflRegion IdRegionNavigation { get; set; }
    }
}
