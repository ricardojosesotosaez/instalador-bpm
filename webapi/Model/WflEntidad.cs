﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflEntidad
    {
        public WflEntidad()
        {
            WflAtrEntidad = new HashSet<WflAtrEntidad>();
            WflJerarquia = new HashSet<WflJerarquia>();
        }

        public Guid IdEntidad { get; set; }
        public string Nombre { get; set; }
        public int? Leer { get; set; }
        public int? Editar { get; set; }
        public int? Eliminar { get; set; }
        public int? Asignar { get; set; }
        public int? Compartir { get; set; }
        public string Clase { get; set; }
        public string Aplicacion { get; set; }
        public string Url { get; set; }
        public Guid? IdJerarquia { get; set; }

        public virtual WflJerarquia IdJerarquiaNavigation { get; set; }
        public virtual ICollection<WflAtrEntidad> WflAtrEntidad { get; set; }
        public virtual ICollection<WflJerarquia> WflJerarquia { get; set; }
    }
}
