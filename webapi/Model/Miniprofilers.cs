﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class Miniprofilers
    {
        public int Rowid { get; set; }
        public Guid Id { get; set; }
        public Guid? Roottimingid { get; set; }
        public DateTime Started { get; set; }
        public decimal Durationmilliseconds { get; set; }
        public string User { get; set; }
        public bool Hasuserviewed { get; set; }
        public string Machinename { get; set; }
        public string Customlinksjson { get; set; }
        public int? Clienttimingsredirectcount { get; set; }
    }
}
