﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class ParGeneral
    {
        public short IdAdm { get; set; }
        public string CodParametro { get; set; }
        public string Descripcion { get; set; }
        public string IdProcesoNegocio { get; set; }
        public string NombreTabla { get; set; }
        public string TipoParametro { get; set; }
        public string EstadoReg { get; set; }
        public DateTime FecEstadoReg { get; set; }
        public DateTime FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public byte[] Columnas { get; set; }
    }
}
