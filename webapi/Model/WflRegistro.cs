﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflRegistro
    {
        public int Registroid { get; set; }
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public string Llaveexpid { get; set; }
        public int Copiaexpid { get; set; }
        public int? CopiaexpidPadre { get; set; }
        public int Flechaid { get; set; }
        public int Tipo { get; set; }
        public DateTime Fecha { get; set; }
        public string Observacion { get; set; }
        public string PersonaidOrigen { get; set; }
        public string RolidOrigen { get; set; }
        public string PersonaidDestino { get; set; }
        public string RolidDestino { get; set; }
        public int? TareaidOrigen { get; set; }
        public int? TareaidDestino { get; set; }
        public string TipoDoc { get; set; }
        public int? NumDoc { get; set; }
        public string Entidad { get; set; }
        public decimal? Monto { get; set; }
        public string CentroCosto { get; set; }
        public string Referencia1 { get; set; }
        public string Referencia2 { get; set; }
        public string Referencia3 { get; set; }
        public int? Prioridad { get; set; }
        public string Region { get; set; }
        public string AtributosXml { get; set; }
        public int? Tiemporeal { get; set; }
        public int? Esfuerzoreal { get; set; }
        public string IdExterno { get; set; }
        public DateTime? Fecha1 { get; set; }
        public DateTime? Fecha2 { get; set; }
        public int? EsfuerzoEsperado { get; set; }
        public int? TiempoEsperado { get; set; }
        public string Asunto { get; set; }
    }
}
