﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflTareaRegion
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Tareaid { get; set; }
        public string Region { get; set; }
        public string Clase { get; set; }
        public string Metodo { get; set; }
        public string Clasemanreg { get; set; }
        public string Metodomanreg { get; set; }
        public string Clasegenid { get; set; }
        public string Metodogenid { get; set; }
        public string Claseformaasig { get; set; }
        public string Metodoformaasig { get; set; }
        public string Claseescucha { get; set; }
        public string Metodoescucha { get; set; }
        public string AtributosXml { get; set; }
        public string Claseprecondicion { get; set; }
        public string Metodoprecondicion { get; set; }
        public string Clasepostcondicion { get; set; }
        public string Metodopostcondicion { get; set; }
        public string Clasecomp { get; set; }
        public string Metodocomp { get; set; }
        public int Postcondicionmultiple { get; set; }
        public int EnviarCorreo { get; set; }
        public int CorreoAutomatico { get; set; }
        public int CorreoRequerido { get; set; }

        public virtual WflTarea WflTarea { get; set; }
    }
}
