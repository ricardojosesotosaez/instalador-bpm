﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflJerarquia
    {
        public WflJerarquia()
        {
            WflEntidad = new HashSet<WflEntidad>();
            WflUnidad = new HashSet<WflUnidad>();
        }

        public Guid IdJerarquia { get; set; }
        public string Nombre { get; set; }
        public short Usumultnodo { get; set; }
        public Guid? IdEntidad { get; set; }

        public virtual WflEntidad IdEntidadNavigation { get; set; }
        public virtual ICollection<WflEntidad> WflEntidad { get; set; }
        public virtual ICollection<WflUnidad> WflUnidad { get; set; }
    }
}
