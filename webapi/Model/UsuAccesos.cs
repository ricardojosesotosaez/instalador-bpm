﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class UsuAccesos
    {
        public int IdAdm { get; set; }
        public decimal IdAcceso { get; set; }
        public string IdFuncionalidad { get; set; }
        public DateTime FecAcceso { get; set; }
        public string Ip { get; set; }
        public string IdUsuario { get; set; }
        public string Descripcion { get; set; }
    }
}
