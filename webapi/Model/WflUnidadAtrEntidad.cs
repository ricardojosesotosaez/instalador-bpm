﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflUnidadAtrEntidad
    {
        public Guid IdUnidadAtrEntidad { get; set; }
        public Guid IdUnidad { get; set; }
        public Guid IdAtrEntidad { get; set; }
        public string Valor { get; set; }

        public virtual WflAtrEntidad IdAtrEntidadNavigation { get; set; }
        public virtual WflUnidad IdUnidadNavigation { get; set; }
    }
}
