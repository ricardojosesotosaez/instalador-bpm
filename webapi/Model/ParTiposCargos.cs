﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class ParTiposCargos
    {
        public short IdAdm { get; set; }
        public short CodCargo { get; set; }
        public string Descripcion { get; set; }
        public string IndRecibeComision { get; set; }
        public string EstadoReg { get; set; }
        public DateTime FecEstadoReg { get; set; }
        public DateTime FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
    }
}
