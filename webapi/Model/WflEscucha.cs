﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflEscucha
    {
        public int Idescucha { get; set; }
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Tareaid { get; set; }
        public string Llaveexpid { get; set; }
        public int? Copiaexpid { get; set; }
        public string Personaid { get; set; }
        public DateTime? Ultimaescucha { get; set; }
        public DateTime? Proximaescucha { get; set; }
        public string DiagidDestino { get; set; }
        public int? FlechaidDestino { get; set; }
        public int? Estadoescucha { get; set; }
        public string Obs { get; set; }
        public string EstadoReg { get; set; }
        public DateTime? FecEstadoReg { get; set; }
        public DateTime? FecIngReg { get; set; }
        public string IdUsuarioIngReg { get; set; }
        public DateTime? FecUltModifReg { get; set; }
        public string IdUsuarioUltModifReg { get; set; }
        public string IdFuncionUltModifReg { get; set; }
        public int? Repeticiones { get; set; }
        public int? NroErrDespacho { get; set; }
        public int? NroErrEvento { get; set; }
        public int? Estadoservicio { get; set; }
    }
}
