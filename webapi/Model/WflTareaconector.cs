﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflTareaconector
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public int Tareaid { get; set; }
        public string DiagidDestino { get; set; }
        public string FlechaidDestino { get; set; }
        public string IdExterno { get; set; }

        public virtual WflTarea WflTarea { get; set; }
    }
}
