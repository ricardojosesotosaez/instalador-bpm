﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflBusqueda
    {
        public WflBusqueda()
        {
            WflBusquedaFiltro = new HashSet<WflBusquedaFiltro>();
        }

        public Guid IdBusqueda { get; set; }
        public string Entidad { get; set; }
        public string IdUsuario { get; set; }
        public string Titulo { get; set; }

        public virtual ICollection<WflBusquedaFiltro> WflBusquedaFiltro { get; set; }
    }
}
