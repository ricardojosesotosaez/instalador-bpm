﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflParametrogeneral
    {
        public int Proyid { get; set; }
        public int Versionid { get; set; }
        public string Diagid { get; set; }
        public string Llave { get; set; }
        public string Valor { get; set; }
        public string Descripcion { get; set; }
    }
}
