﻿using System;
using System.Collections.Generic;

namespace webapi.Model
{
    public partial class WflUnidadRol
    {
        public Guid IdUnidadRol { get; set; }
        public string IdRol { get; set; }
        public Guid? IdEntidad { get; set; }
        public int? Leer { get; set; }
        public int? Editar { get; set; }
        public int? Eliminar { get; set; }
        public int? Asignar { get; set; }
        public int? Compartir { get; set; }
    }
}
