﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace webapi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ARC_DIRECTORIO",
                columns: table => new
                {
                    IdDirectorio = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    Ruta = table.Column<string>(unicode: false, maxLength: 8000, nullable: true),
                    IdDirectorioPadre = table.Column<int>(nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false, defaultValueSql: "('SISTEMA')"),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false, defaultValueSql: "('SISTEMA')"),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    RUTA_VIRTUAL = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    IDREPOSITORIO = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ARC_DIRECTORIO", x => x.IdDirectorio);
                    table.ForeignKey(
                        name: "FK_DIRECTORIO_PADRE",
                        column: x => x.IdDirectorioPadre,
                        principalTable: "ARC_DIRECTORIO",
                        principalColumn: "IdDirectorio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MINIPROFILERCLIENTTIMINGS",
                columns: table => new
                {
                    ROWID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID = table.Column<Guid>(nullable: false),
                    MINIPROFILERID = table.Column<Guid>(nullable: false),
                    NAME = table.Column<string>(maxLength: 200, nullable: false),
                    START = table.Column<decimal>(type: "decimal(9, 3)", nullable: false),
                    DURATION = table.Column<decimal>(type: "decimal(9, 3)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MINIPROFILERCLIENTTIMINGS", x => x.ROWID);
                });

            migrationBuilder.CreateTable(
                name: "MINIPROFILERS",
                columns: table => new
                {
                    ROWID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID = table.Column<Guid>(nullable: false),
                    ROOTTIMINGID = table.Column<Guid>(nullable: true),
                    STARTED = table.Column<DateTime>(type: "datetime", nullable: false),
                    DURATIONMILLISECONDS = table.Column<decimal>(type: "decimal(7, 1)", nullable: false),
                    USER = table.Column<string>(maxLength: 100, nullable: true),
                    HASUSERVIEWED = table.Column<bool>(nullable: false),
                    MACHINENAME = table.Column<string>(maxLength: 100, nullable: true),
                    CUSTOMLINKSJSON = table.Column<string>(nullable: true),
                    CLIENTTIMINGSREDIRECTCOUNT = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MINIPROFILERS", x => x.ROWID);
                });

            migrationBuilder.CreateTable(
                name: "MINIPROFILERTIMINGS",
                columns: table => new
                {
                    ROWID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID = table.Column<Guid>(nullable: false),
                    MINIPROFILERID = table.Column<Guid>(nullable: false),
                    PARENTTIMINGID = table.Column<Guid>(nullable: true),
                    NAME = table.Column<string>(maxLength: 200, nullable: false),
                    DURATIONMILLISECONDS = table.Column<decimal>(type: "decimal(9, 3)", nullable: false),
                    STARTMILLISECONDS = table.Column<decimal>(type: "decimal(9, 3)", nullable: false),
                    ISROOT = table.Column<bool>(nullable: false),
                    DEPTH = table.Column<short>(nullable: false),
                    CUSTOMTIMINGSJSON = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MINIPROFILERTIMINGS", x => x.ROWID);
                });

            migrationBuilder.CreateTable(
                name: "PAR_AGENCIAS",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    COD_AGENCIA = table.Column<short>(nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    DIRECCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    COD_COMUNA = table.Column<int>(nullable: false),
                    FOLIORECLAMO650 = table.Column<int>(nullable: false),
                    COD_SUBGERENCIA = table.Column<short>(nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    HORARIO_ATENCION = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    COD_ZONA = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAR_AGENCIAS", x => new { x.ID_ADM, x.COD_AGENCIA });
                });

            migrationBuilder.CreateTable(
                name: "PAR_AREAS",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    COD_AREA = table.Column<short>(nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAR_AREAS", x => new { x.ID_ADM, x.COD_AREA });
                });

            migrationBuilder.CreateTable(
                name: "PAR_COMUNAS",
                columns: table => new
                {
                    COD_COMUNA = table.Column<int>(nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CIUDAD = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    COD_REGION = table.Column<int>(nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAR_COMUNAS", x => x.COD_COMUNA);
                });

            migrationBuilder.CreateTable(
                name: "PAR_DATOS_CLAVE",
                columns: table => new
                {
                    ID_ADM = table.Column<int>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    LARGO_MINIMO = table.Column<int>(nullable: false),
                    LARGO_MAXIMO = table.Column<int>(nullable: false),
                    DIAS_EXPIRA = table.Column<int>(nullable: false),
                    DIAS_AVISO = table.Column<int>(nullable: false),
                    NUM_INTENTOS = table.Column<int>(nullable: false),
                    DIAS_INACTIVO = table.Column<int>(nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAR_DATOS_CLAVE", x => new { x.ID_ADM, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "PAR_FERIADOS",
                columns: table => new
                {
                    FEC_FERIADO = table.Column<DateTime>(type: "datetime", nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAR_FERIADOS_1", x => x.FEC_FERIADO);
                });

            migrationBuilder.CreateTable(
                name: "PAR_GENERAL",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    COD_PARAMETRO = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ID_PROCESO_NEGOCIO = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    NOMBRE_TABLA = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    TIPO_PARAMETRO = table.Column<string>(unicode: false, maxLength: 4, nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    COLUMNAS = table.Column<byte[]>(type: "image", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAR_GENERAL", x => new { x.ID_ADM, x.COD_PARAMETRO });
                });

            migrationBuilder.CreateTable(
                name: "PAR_TIPOS_CARGOS",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    COD_CARGO = table.Column<short>(nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    IND_RECIBE_COMISION = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAR_TIPOS_CARGOS", x => new { x.ID_ADM, x.COD_CARGO });
                });

            migrationBuilder.CreateTable(
                name: "USU_ACCESOS",
                columns: table => new
                {
                    ID_ADM = table.Column<int>(nullable: false),
                    ID_ACCESO = table.Column<decimal>(type: "numeric(20, 0)", nullable: false),
                    ID_FUNCIONALIDAD = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    FEC_ACCESO = table.Column<DateTime>(type: "datetime", nullable: false),
                    IP = table.Column<string>(unicode: false, maxLength: 32, nullable: true),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_ACCESOS", x => new { x.ID_ADM, x.ID_ACCESO });
                });

            migrationBuilder.CreateTable(
                name: "USU_MENUS",
                columns: table => new
                {
                    ID_MENU = table.Column<int>(nullable: false),
                    ID_PADRE = table.Column<int>(nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    URL = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    ORDEN = table.Column<int>(nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_MENUS", x => x.ID_MENU);
                });

            migrationBuilder.CreateTable(
                name: "USU_PERFILES",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_PERFIL = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_PERFILES", x => new { x.ID_ADM, x.ID_PERFIL });
                });

            migrationBuilder.CreateTable(
                name: "USU_PROCESOSNEGOCIO",
                columns: table => new
                {
                    ID_PROCESO_NEGOCIO = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PROCESONEGOCIO", x => x.ID_PROCESO_NEGOCIO);
                });

            migrationBuilder.CreateTable(
                name: "USU_RECURSO",
                columns: table => new
                {
                    ID_RECURSO = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    TIPO = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    RECURSO = table.Column<string>(unicode: false, maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_RECURSO", x => x.ID_RECURSO);
                });

            migrationBuilder.CreateTable(
                name: "USU_SESIONES",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_SESION = table.Column<decimal>(type: "numeric(20, 0)", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FEC_INICIO = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_FIN = table.Column<DateTime>(type: "datetime", nullable: true),
                    DIRECCION_IP = table.Column<string>(unicode: false, maxLength: 32, nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    TOKEN = table.Column<string>(unicode: false, maxLength: 64, nullable: true),
                    ASPNET_SESSIONID = table.Column<string>(unicode: false, maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_SESIONES", x => new { x.ID_ADM, x.ID_USUARIO, x.ID_SESION });
                });

            migrationBuilder.CreateTable(
                name: "USU_USER_CONTROL",
                columns: table => new
                {
                    NOM_USER_CONTROL = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("USU_USER_CONTROL_PK", x => x.NOM_USER_CONTROL);
                });

            migrationBuilder.CreateTable(
                name: "USU_USUARIOS",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_PERSONA = table.Column<string>(unicode: false, maxLength: 15, nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    ESTADO = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    TELEFONO = table.Column<string>(unicode: false, maxLength: 15, nullable: true),
                    DIRECCION = table.Column<string>(unicode: false, maxLength: 45, nullable: true),
                    COD_CARGO = table.Column<short>(nullable: false),
                    COD_AGENCIA = table.Column<short>(nullable: false),
                    COD_COMUNA = table.Column<int>(nullable: true),
                    COD_AREA = table.Column<short>(nullable: false),
                    IND_BLOQUEO = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    ID_USUARIO_JEFE = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    CLAVE = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    FEC_EXP_CLAVE = table.Column<DateTime>(type: "datetime", nullable: false),
                    CANT_INTENTOS = table.Column<short>(nullable: true),
                    FEC_ULT_INGRESO = table.Column<DateTime>(type: "datetime", nullable: true),
                    COD_SAFP = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    EMAIL = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ID_ACTIVE_DIRECTORY = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    HASH_INTEGRIDAD = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    TIPO = table.Column<string>(unicode: false, maxLength: 1, nullable: false, defaultValueSql: "('U')"),
                    REGION = table.Column<string>(unicode: false, maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PERSONA", x => new { x.ID_USUARIO, x.ID_ADM });
                });

            migrationBuilder.CreateTable(
                name: "WFL_ALMACEN_PWD",
                columns: table => new
                {
                    APLICACION = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    USUARIOID = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    USUARIOAPPID = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    PASSWORD = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    SERVIDOR = table.Column<string>(unicode: false, maxLength: 64, nullable: true),
                    ESTADO = table.Column<string>(unicode: false, maxLength: 64, nullable: true),
                    FEC_CAMB_ESTADO = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_ALMACEN_PWD", x => new { x.APLICACION, x.USUARIOID });
                });

            migrationBuilder.CreateTable(
                name: "WFL_APLICACION",
                columns: table => new
                {
                    APLICACION = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    REGION = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    CLASEAUTH = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    SINC_USU = table.Column<int>(nullable: false),
                    CREAR_USU = table.Column<int>(nullable: false),
                    PERIOD_SINCRO = table.Column<string>(type: "text", nullable: true),
                    ULTIMA_SINCRO = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_APLICACION", x => new { x.APLICACION, x.REGION });
                });

            migrationBuilder.CreateTable(
                name: "WFL_BUSQUEDA",
                columns: table => new
                {
                    ID_BUSQUEDA = table.Column<Guid>(nullable: false),
                    ENTIDAD = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    TITULO = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_BUSQUEDA", x => x.ID_BUSQUEDA);
                });

            migrationBuilder.CreateTable(
                name: "WFL_COPIAEXP_LOCK",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    LLAVEEXPID = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    COPIAEXPID = table.Column<int>(nullable: false),
                    LOCK = table.Column<bool>(nullable: false),
                    FECHA_LOCK = table.Column<DateTime>(type: "datetime", nullable: false),
                    VERSION = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_COPIAEXP_LOCK", x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID, x.COPIAEXPID });
                });

            migrationBuilder.CreateTable(
                name: "WFL_ESCUCHA",
                columns: table => new
                {
                    IDESCUCHA = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    TAREAID = table.Column<int>(nullable: false),
                    LLAVEEXPID = table.Column<string>(unicode: false, maxLength: 64, nullable: true),
                    COPIAEXPID = table.Column<int>(nullable: true),
                    PERSONAID = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    ULTIMAESCUCHA = table.Column<DateTime>(type: "datetime", nullable: true),
                    PROXIMAESCUCHA = table.Column<DateTime>(type: "datetime", nullable: true),
                    DIAGID_DESTINO = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    FLECHAID_DESTINO = table.Column<int>(nullable: true),
                    ESTADOESCUCHA = table.Column<int>(nullable: true),
                    OBS = table.Column<string>(type: "text", nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    REPETICIONES = table.Column<int>(nullable: true, defaultValueSql: "((0))"),
                    NRO_ERR_DESPACHO = table.Column<int>(nullable: true),
                    NRO_ERR_EVENTO = table.Column<int>(nullable: true),
                    ESTADOSERVICIO = table.Column<int>(nullable: true, defaultValueSql: "((0))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_ESCUCHA", x => x.IDESCUCHA);
                });

            migrationBuilder.CreateTable(
                name: "WFL_GRUPO",
                columns: table => new
                {
                    ID_GRUPO = table.Column<Guid>(nullable: false),
                    COD_GRUPO = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    URL_SHAREPOINT = table.Column<string>(unicode: false, maxLength: 300, nullable: true),
                    ID_GRUPO_SHAREPOINT = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    NOMBRE_GRUPO_SHAREPOINT = table.Column<string>(unicode: false, maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_GRUPO", x => x.ID_GRUPO);
                });

            migrationBuilder.CreateTable(
                name: "WFL_MENSAJE",
                columns: table => new
                {
                    ID_MENSAJE = table.Column<Guid>(nullable: false),
                    TIPO = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    TEXTO = table.Column<string>(unicode: false, maxLength: 2000, nullable: true),
                    XML = table.Column<string>(type: "ntext", nullable: true),
                    FEC_EXPIRACION = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_MENSAJE", x => x.ID_MENSAJE);
                });

            migrationBuilder.CreateTable(
                name: "WFL_OPCION",
                columns: table => new
                {
                    ID_OPCION = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TIPO = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ESEDITABLE = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_OPCION", x => x.ID_OPCION);
                });

            migrationBuilder.CreateTable(
                name: "WFL_OPCION_USUARIO",
                columns: table => new
                {
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_OPCION = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    LLAVE = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    VALOR = table.Column<string>(unicode: false, maxLength: 2000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OPCION", x => new { x.ID_USUARIO, x.ID_OPCION, x.LLAVE });
                });

            migrationBuilder.CreateTable(
                name: "WFL_PARAMETROGENERAL",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 200, nullable: false, defaultValueSql: "('')"),
                    LLAVE = table.Column<string>(unicode: false, maxLength: 64, nullable: false, defaultValueSql: "('')"),
                    VALOR = table.Column<string>(unicode: false, maxLength: 500, nullable: false, defaultValueSql: "('')"),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 500, nullable: false, defaultValueSql: "('')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_PARAMETROGENERAL", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.LLAVE });
                });

            migrationBuilder.CreateTable(
                name: "WFL_PLANTILLA",
                columns: table => new
                {
                    ID_PLANTILLA = table.Column<Guid>(nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    PLANTILLA = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_PLANTILLA", x => x.ID_PLANTILLA);
                });

            migrationBuilder.CreateTable(
                name: "WFL_PROY",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FECHA = table.Column<DateTime>(type: "datetime", nullable: false),
                    CREADOR = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    OBSERVACIONES = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PROY", x => new { x.PROYID, x.VERSIONID });
                });

            migrationBuilder.CreateTable(
                name: "WFL_REGION",
                columns: table => new
                {
                    ID_REGION = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_REGION", x => x.ID_REGION);
                });

            migrationBuilder.CreateTable(
                name: "WFL_REGISTRO",
                columns: table => new
                {
                    REGISTROID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    LLAVEEXPID = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    COPIAEXPID = table.Column<int>(nullable: false),
                    COPIAEXPID_PADRE = table.Column<int>(nullable: true),
                    FLECHAID = table.Column<int>(nullable: false),
                    TIPO = table.Column<int>(nullable: false),
                    FECHA = table.Column<DateTime>(type: "datetime", nullable: false),
                    OBSERVACION = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    PERSONAID_ORIGEN = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    ROLID_ORIGEN = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    PERSONAID_DESTINO = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    ROLID_DESTINO = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    TAREAID_ORIGEN = table.Column<int>(nullable: true),
                    TAREAID_DESTINO = table.Column<int>(nullable: true),
                    TIPO_DOC = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    NUM_DOC = table.Column<int>(nullable: true),
                    ENTIDAD = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    MONTO = table.Column<decimal>(type: "decimal(18, 0)", nullable: true),
                    CENTRO_COSTO = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    REFERENCIA_1 = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    REFERENCIA_2 = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    REFERENCIA_3 = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    PRIORIDAD = table.Column<int>(nullable: true),
                    REGION = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    ATRIBUTOS_XML = table.Column<string>(type: "text", nullable: true),
                    TIEMPOREAL = table.Column<int>(nullable: true),
                    ESFUERZOREAL = table.Column<int>(nullable: true),
                    ID_EXTERNO = table.Column<string>(unicode: false, maxLength: 64, nullable: true),
                    FECHA_1 = table.Column<DateTime>(type: "datetime", nullable: true),
                    FECHA_2 = table.Column<DateTime>(type: "datetime", nullable: true),
                    ESFUERZO_ESPERADO = table.Column<int>(nullable: true),
                    TIEMPO_ESPERADO = table.Column<int>(nullable: true),
                    ASUNTO = table.Column<string>(unicode: false, maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_REGISTRO", x => x.REGISTROID);
                });

            migrationBuilder.CreateTable(
                name: "WFL_SUBPROCESO_A_PUBLICAR",
                columns: table => new
                {
                    IDSUBPUB = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    ID_EXTERNO = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    ESTADO = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    ORIGEN = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    DESC_ARCHIVO = table.Column<string>(unicode: false, maxLength: 120, nullable: true),
                    NOMBRE_ARCHIVO = table.Column<string>(unicode: false, maxLength: 120, nullable: true),
                    FEC_CAMBIO_ESTADO = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_SUBPUB", x => x.IDSUBPUB);
                });

            migrationBuilder.CreateTable(
                name: "WFL_TESTESCUCHA",
                columns: table => new
                {
                    TEST_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PASO = table.Column<string>(unicode: false, maxLength: 64, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_TESTESCUCHA", x => x.TEST_ID);
                });

            migrationBuilder.CreateTable(
                name: "WFL_TIPOEXP",
                columns: table => new
                {
                    TIPOEXPID = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ATRIBUTOS_XML = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TIPOEXP", x => x.TIPOEXPID);
                });

            migrationBuilder.CreateTable(
                name: "WFL_UNIDAD_ROL",
                columns: table => new
                {
                    ID_UNIDAD_ROL = table.Column<Guid>(nullable: false),
                    ID_ROL = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    ID_ENTIDAD = table.Column<Guid>(nullable: true),
                    LEER = table.Column<int>(nullable: true),
                    EDITAR = table.Column<int>(nullable: true),
                    ELIMINAR = table.Column<int>(nullable: true),
                    ASIGNAR = table.Column<int>(nullable: true),
                    COMPARTIR = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_UNIDAD_ROL", x => x.ID_UNIDAD_ROL);
                });

            migrationBuilder.CreateTable(
                name: "ARC_ARCHIVO",
                columns: table => new
                {
                    IdArchivo = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    IdDirectorioPadre = table.Column<int>(nullable: false),
                    Notas = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Nombre = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    FecUltModificacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FecCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    UsuUltModificacion = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    UsuCreacion = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    UsuPropietario = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    Tamano = table.Column<long>(nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false, defaultValueSql: "((1))"),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    IDREPOSITORIO = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ARC_ARCHIVO", x => x.IdArchivo);
                    table.ForeignKey(
                        name: "FK_ARC_ARCHIVO_ARC_DIRECTORIO",
                        column: x => x.IdDirectorioPadre,
                        principalTable: "ARC_DIRECTORIO",
                        principalColumn: "IdDirectorio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USU_FUNCIONALIDADES",
                columns: table => new
                {
                    ID_FUNCIONALIDAD = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ID_PROCESO_NEGOCIO = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FUNCIONALIDAD", x => x.ID_FUNCIONALIDAD);
                    table.ForeignKey(
                        name: "FK_USU_FUNCIONALIDADES_USU_PROCESOSNEGOCIO",
                        column: x => x.ID_PROCESO_NEGOCIO,
                        principalTable: "USU_PROCESOSNEGOCIO",
                        principalColumn: "ID_PROCESO_NEGOCIO",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USU_ROLES",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_ROL = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ID_PROCESO_NEGOCIO = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    IND_ADMIN_PN = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    TIPO_ROL_WORKFLOW = table.Column<string>(unicode: false, maxLength: 4, nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ROLES", x => new { x.ID_ADM, x.ID_ROL });
                    table.ForeignKey(
                        name: "FK_USU_ROLES_USU_PROCESOSNEGOCIO",
                        column: x => x.ID_PROCESO_NEGOCIO,
                        principalTable: "USU_PROCESOSNEGOCIO",
                        principalColumn: "ID_PROCESO_NEGOCIO",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USU_HIST_CLAVE",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC = table.Column<DateTime>(type: "datetime", nullable: false),
                    TIPO = table.Column<int>(nullable: false),
                    CLAVE = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("USU_HIST_CLAVE_PK", x => new { x.FEC, x.ID_ADM, x.ID_USUARIO });
                    table.ForeignKey(
                        name: "FK_USU_HIST_CLAVE_USU_USUARIOS",
                        columns: x => new { x.ID_USUARIO, x.ID_ADM },
                        principalTable: "USU_USUARIOS",
                        principalColumns: new[] { "ID_USUARIO", "ID_ADM" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "USU_USUARIOS_PERFILES",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_PERFIL = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_USUARIOS_PERFILES", x => new { x.ID_ADM, x.ID_USUARIO, x.ID_PERFIL });
                    table.ForeignKey(
                        name: "FK_USU_USUARIOS_PERFILES_USU_PERFILES",
                        columns: x => new { x.ID_ADM, x.ID_PERFIL },
                        principalTable: "USU_PERFILES",
                        principalColumns: new[] { "ID_ADM", "ID_PERFIL" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_USU_USUARIOS_PERFILES_USU_USUARIOS",
                        columns: x => new { x.ID_USUARIO, x.ID_ADM },
                        principalTable: "USU_USUARIOS",
                        principalColumns: new[] { "ID_USUARIO", "ID_ADM" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_BUSQUEDA_FILTRO",
                columns: table => new
                {
                    ID_BUSQUEDA_FILTRO = table.Column<Guid>(nullable: false),
                    ID_BUSQUEDA = table.Column<Guid>(nullable: true),
                    ORDEN = table.Column<int>(nullable: false),
                    ID_CAMPO = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ID_FILTRO = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    VALOR1 = table.Column<string>(unicode: false, maxLength: 2000, nullable: true),
                    VALOR2 = table.Column<string>(unicode: false, maxLength: 2000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_BUSQUEDA_FILTRO", x => x.ID_BUSQUEDA_FILTRO);
                    table.ForeignKey(
                        name: "FK_BUSQUEDA_FILTRO",
                        column: x => x.ID_BUSQUEDA,
                        principalTable: "WFL_BUSQUEDA",
                        principalColumn: "ID_BUSQUEDA",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WFL_GRUPO_USUARIO",
                columns: table => new
                {
                    ID_GRUPO_USUARIO = table.Column<Guid>(nullable: false),
                    ID_GRUPO = table.Column<Guid>(nullable: false),
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    LIDER = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_GRUPO_USUARIO", x => x.ID_GRUPO_USUARIO);
                    table.ForeignKey(
                        name: "FK_WFL_GRUPO_USUARIO_WFL_GRUPO",
                        column: x => x.ID_GRUPO,
                        principalTable: "WFL_GRUPO",
                        principalColumn: "ID_GRUPO",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WFL_GRUPO_USUARIO_USU_USUARIOS",
                        columns: x => new { x.ID_USUARIO, x.ID_ADM },
                        principalTable: "USU_USUARIOS",
                        principalColumns: new[] { "ID_USUARIO", "ID_ADM" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WFL_ALARMA",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    ALARMAID = table.Column<int>(nullable: false),
                    FECHAINICIAL = table.Column<DateTime>(type: "datetime", nullable: false),
                    FECHAFINAL = table.Column<DateTime>(type: "datetime", nullable: false),
                    FECHAFINALPOSIBLE = table.Column<DateTime>(type: "datetime", nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    TAREAID = table.Column<int>(nullable: false),
                    LLAVEEXPID = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    COPIAEXPID = table.Column<int>(nullable: true),
                    FLECHAID = table.Column<int>(nullable: true),
                    ACTIVADA = table.Column<int>(nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ALARMA", x => new { x.PROYID, x.VERSIONID, x.ALARMAID });
                    table.ForeignKey(
                        name: "FK_ALARMA_PROY",
                        columns: x => new { x.PROYID, x.VERSIONID },
                        principalTable: "WFL_PROY",
                        principalColumns: new[] { "PROYID", "VERSIONID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_COPIASPORPERSONA",
                columns: table => new
                {
                    PERSONAID = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    NUMCOPIAS = table.Column<int>(nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_COPIASPORPERSONA", x => new { x.PERSONAID, x.PROYID, x.VERSIONID });
                    table.ForeignKey(
                        name: "FK_COPIASPORPERSONA_PROY",
                        columns: x => new { x.PROYID, x.VERSIONID },
                        principalTable: "WFL_PROY",
                        principalColumns: new[] { "PROYID", "VERSIONID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_DIAG",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    LAPSO = table.Column<int>(nullable: true),
                    ID_EXTERNO = table.Column<string>(unicode: false, maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DIAG", x => new { x.PROYID, x.VERSIONID, x.DIAGID });
                    table.ForeignKey(
                        name: "FK_DIAG_PROY",
                        columns: x => new { x.PROYID, x.VERSIONID },
                        principalTable: "WFL_PROY",
                        principalColumns: new[] { "PROYID", "VERSIONID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_GRUPO_REGION",
                columns: table => new
                {
                    ID_GRUPO_REGION = table.Column<Guid>(nullable: false),
                    ID_GRUPO = table.Column<Guid>(nullable: false),
                    ID_REGION = table.Column<string>(unicode: false, maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_GRUPO_REGION", x => x.ID_GRUPO_REGION);
                    table.ForeignKey(
                        name: "FK_WFL_GRUPO_REGION_WFL_GRUPO",
                        column: x => x.ID_GRUPO,
                        principalTable: "WFL_GRUPO",
                        principalColumn: "ID_GRUPO",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WFL_GRUPO_REGION_WFL_REGION",
                        column: x => x.ID_REGION,
                        principalTable: "WFL_REGION",
                        principalColumn: "ID_REGION",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WFL_SUBPROCESO_A_PUBLICAR_EXT",
                columns: table => new
                {
                    IDSUBPUB = table.Column<int>(nullable: false),
                    DIAGRAMA = table.Column<string>(type: "text", nullable: false),
                    ARCHIVO_XPDL = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_SUBPUB_EXT", x => x.IDSUBPUB);
                    table.ForeignKey(
                        name: "FK_WFL_SUBPROCESO_A_PUBLICAR_EXT_WFL_SUBPROCESO_A_PUBLICAR",
                        column: x => x.IDSUBPUB,
                        principalTable: "WFL_SUBPROCESO_A_PUBLICAR",
                        principalColumn: "IDSUBPUB",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_EXP",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    LLAVEEXPID = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    TIPOEXPID = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    OBS = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    FECHAINICIO = table.Column<DateTime>(type: "datetime", nullable: true),
                    FECHAFIN = table.Column<DateTime>(type: "datetime", nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    PERSONAID = table.Column<string>(unicode: false, maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EXP", x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID });
                    table.ForeignKey(
                        name: "FK_EXP_TIPOEXP",
                        column: x => x.TIPOEXPID,
                        principalTable: "WFL_TIPOEXP",
                        principalColumn: "TIPOEXPID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USU_MENUS_FUNCIONALIDADES",
                columns: table => new
                {
                    ID_MENU = table.Column<int>(nullable: false),
                    ID_FUNCIONALIDAD = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ORDEN = table.Column<int>(nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_MENUS_FUN", x => new { x.ID_MENU, x.ID_FUNCIONALIDAD });
                    table.ForeignKey(
                        name: "FK_USU_MENUS_FUNCIONALIDADES_USU_FUNCIONALIDADES",
                        column: x => x.ID_FUNCIONALIDAD,
                        principalTable: "USU_FUNCIONALIDADES",
                        principalColumn: "ID_FUNCIONALIDAD",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_USU_MENUS_FUNCIONALIDADES_USU_MENUS",
                        column: x => x.ID_MENU,
                        principalTable: "USU_MENUS",
                        principalColumn: "ID_MENU",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USU_RECURSO_FUNCIONALIDADES",
                columns: table => new
                {
                    ID_RECURSO = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    ID_FUNCIONALIDAD = table.Column<string>(unicode: false, maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_RECURSO_FUNCIONALIDADES", x => new { x.ID_RECURSO, x.ID_FUNCIONALIDAD });
                    table.ForeignKey(
                        name: "FK_RECFUN_RECURSO",
                        column: x => x.ID_FUNCIONALIDAD,
                        principalTable: "USU_FUNCIONALIDADES",
                        principalColumn: "ID_FUNCIONALIDAD",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RECFUN_FUNCIONALIDADES",
                        column: x => x.ID_RECURSO,
                        principalTable: "USU_RECURSO",
                        principalColumn: "ID_RECURSO",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "USU_USER_CONTROL_FUNCIONALIDAD",
                columns: table => new
                {
                    NOM_USER_CONTROL = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    ID_FUNCIONALIDAD = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    INICIAL = table.Column<short>(nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    MODO = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USER_CONTROL_FUNCIONALIDAD", x => new { x.NOM_USER_CONTROL, x.ID_FUNCIONALIDAD });
                    table.ForeignKey(
                        name: "FK_USU_USER_CONTROL_FUNCIONALIDAD_USU_FUNCIONALIDADES",
                        column: x => x.ID_FUNCIONALIDAD,
                        principalTable: "USU_FUNCIONALIDADES",
                        principalColumn: "ID_FUNCIONALIDAD",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_USU_USER_CONTROL_FUNCIONALIDAD_USU_USER_CONTROL",
                        column: x => x.NOM_USER_CONTROL,
                        principalTable: "USU_USER_CONTROL",
                        principalColumn: "NOM_USER_CONTROL",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USU_FUNCIONALIDADES_ROLES",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_FUNCIONALIDAD = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ID_ROL = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FUNCIONALIDAD_ROL", x => new { x.ID_FUNCIONALIDAD, x.ID_ADM, x.ID_ROL });
                    table.ForeignKey(
                        name: "FK_USU_FUNCIONALIDADES_ROLES_USU_FUNCIONALIDADES",
                        column: x => x.ID_FUNCIONALIDAD,
                        principalTable: "USU_FUNCIONALIDADES",
                        principalColumn: "ID_FUNCIONALIDAD",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_USU_FUNCIONALIDADES_ROLES_USU_ROLES",
                        columns: x => new { x.ID_ADM, x.ID_ROL },
                        principalTable: "USU_ROLES",
                        principalColumns: new[] { "ID_ADM", "ID_ROL" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USU_PERFILES_ROLES",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_PERFIL = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ID_ROL = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_PERFILES_ROLES", x => new { x.ID_ADM, x.ID_PERFIL, x.ID_ROL });
                    table.ForeignKey(
                        name: "FK_USU_PERFILES_ROLES_USU_PERFILES",
                        columns: x => new { x.ID_ADM, x.ID_PERFIL },
                        principalTable: "USU_PERFILES",
                        principalColumns: new[] { "ID_ADM", "ID_PERFIL" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_USU_PERFILES_ROLES_USU_ROLES",
                        columns: x => new { x.ID_ADM, x.ID_ROL },
                        principalTable: "USU_ROLES",
                        principalColumns: new[] { "ID_ADM", "ID_ROL" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "USU_USUARIOS_ROLES",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_ROL = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: true),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PERSONA_ROL", x => new { x.ID_USUARIO, x.ID_ADM, x.ID_ROL });
                    table.ForeignKey(
                        name: "FK_USU_USUARIOS_ROLES_USU_ROLES",
                        columns: x => new { x.ID_ADM, x.ID_ROL },
                        principalTable: "USU_ROLES",
                        principalColumns: new[] { "ID_ADM", "ID_ROL" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_USU_USUARIOS_ROLES_USU_USUARIOS",
                        columns: x => new { x.ID_USUARIO, x.ID_ADM },
                        principalTable: "USU_USUARIOS",
                        principalColumns: new[] { "ID_USUARIO", "ID_ADM" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_PROC",
                columns: table => new
                {
                    PROCESOID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 2000, nullable: false),
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_PROC", x => x.PROCESOID);
                    table.ForeignKey(
                        name: "FK_WFL_PROC_WFL_DIAG",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID },
                        principalTable: "WFL_DIAG",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_TAREA",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    TAREAID = table.Column<int>(nullable: false),
                    PROPOSITO = table.Column<string>(unicode: false, maxLength: 2000, nullable: true),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    ROLID = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    TIPO = table.Column<int>(nullable: false),
                    COORDENADAS = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    PROCESONEGOCIO = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    MODULO = table.Column<string>(unicode: false, maxLength: 64, nullable: true),
                    ID_EXTERNO = table.Column<string>(unicode: false, maxLength: 64, nullable: true),
                    SUBTIPO = table.Column<int>(nullable: true),
                    PRIORIDAD = table.Column<int>(nullable: true),
                    CENTRO_COSTO = table.Column<string>(unicode: false, maxLength: 24, nullable: true),
                    FECHA_VENC = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    NOTA = table.Column<string>(unicode: false, maxLength: 2000, nullable: true),
                    REFERENCIA_1 = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    REFERENCIA_2 = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    REFERENCIA_3 = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    ESREASIGNABLE = table.Column<int>(nullable: true),
                    ASUNTO = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    CAMBIO_PRIORIDAD = table.Column<string>(unicode: false, maxLength: 2000, nullable: true),
                    TAREAID_PADRE = table.Column<int>(nullable: false),
                    ADJUNTO = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TAREA", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID });
                    table.ForeignKey(
                        name: "FK_TAREA_DIAG",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID },
                        principalTable: "WFL_DIAG",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "USU_ROLES_PROC",
                columns: table => new
                {
                    ID_ADM = table.Column<short>(nullable: false),
                    PROCESOID = table.Column<int>(nullable: false),
                    ID_ROL = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 2000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USU_ROLES_PROC", x => new { x.PROCESOID, x.ID_ROL, x.ID_ADM });
                    table.ForeignKey(
                        name: "FK_USU_ROLES_PROC_WFL_PROC",
                        column: x => x.PROCESOID,
                        principalTable: "WFL_PROC",
                        principalColumn: "PROCESOID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_USU_ROLES_PROC_USU_ROLES",
                        columns: x => new { x.ID_ADM, x.ID_ROL },
                        principalTable: "USU_ROLES",
                        principalColumns: new[] { "ID_ADM", "ID_ROL" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_GRUPO_PROCESO",
                columns: table => new
                {
                    ID_GRUPO_PROCESO = table.Column<Guid>(nullable: false),
                    ID_GRUPO = table.Column<Guid>(nullable: false),
                    PROCESOID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_GRUPO_PROCESO", x => x.ID_GRUPO_PROCESO);
                    table.ForeignKey(
                        name: "FK_WFL_GRUPO_PROCESO_WFL_GRUPO",
                        column: x => x.ID_GRUPO,
                        principalTable: "WFL_GRUPO",
                        principalColumn: "ID_GRUPO",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WFL_GRUPO_PROCESO_WFL_PROC",
                        column: x => x.PROCESOID,
                        principalTable: "WFL_PROC",
                        principalColumn: "PROCESOID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WFL_COPIAEXP",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    LLAVEEXPID = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    COPIAEXPID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    TAREAID = table.Column<int>(nullable: true),
                    ASIGNADA = table.Column<int>(nullable: false),
                    ESDESPACHABLE = table.Column<int>(nullable: false),
                    FECHA = table.Column<DateTime>(type: "datetime", nullable: false),
                    PENDPORDESP = table.Column<int>(nullable: false),
                    PERSONAID = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    OBS = table.Column<string>(type: "text", nullable: true),
                    DESCRIPCION = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    TIPOEXPID = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    REGION = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    FECHAVENC = table.Column<DateTime>(type: "datetime", nullable: true),
                    ESTADO_REG = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    VERSION = table.Column<byte[]>(rowVersion: true, nullable: true),
                    NRO_ERR_SERVICIO = table.Column<int>(nullable: false),
                    FEC_ESTADO_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    FEC_ING_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ING_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FEC_ULT_MODIF_REG = table.Column<DateTime>(type: "datetime", nullable: false),
                    ID_USUARIO_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ID_FUNCION_ULT_MODIF_REG = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    ESFUERZO_ESPERADO = table.Column<int>(nullable: true),
                    TIEMPO_ESPERADO = table.Column<int>(nullable: true),
                    COD_GRUPO = table.Column<string>(unicode: false, maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_COPIAEXP", x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID, x.COPIAEXPID });
                    table.ForeignKey(
                        name: "FK_COPIAEXP_EXP",
                        columns: x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID },
                        principalTable: "WFL_EXP",
                        principalColumns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_COPIAEXP_TAREA",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID },
                        principalTable: "WFL_TAREA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_FLECHA",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    FLECHAID = table.Column<int>(nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    TAREAID_ORIGEN = table.Column<int>(nullable: true),
                    DIAGID_DESTINO = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    TAREAID_DESTINO = table.Column<int>(nullable: true),
                    TIPO = table.Column<int>(nullable: false),
                    ID_EXTERNO = table.Column<string>(unicode: false, maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLECHA", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.FLECHAID });
                    table.ForeignKey(
                        name: "FK_FLECHA_DIAG",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID },
                        principalTable: "WFL_DIAG",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FLECHA_TAREA1",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID_ORIGEN },
                        principalTable: "WFL_TAREA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FLECHA_TAREA2",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID_DESTINO, x.TAREAID_DESTINO },
                        principalTable: "WFL_TAREA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_TAREA_REGION",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    TAREAID = table.Column<int>(nullable: false),
                    REGION = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    CLASE = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    METODO = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    CLASEMANREG = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    METODOMANREG = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    CLASEGENID = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    METODOGENID = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    CLASEFORMAASIG = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    METODOFORMAASIG = table.Column<string>(unicode: false, maxLength: 24, nullable: true),
                    CLASEESCUCHA = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    METODOESCUCHA = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    ATRIBUTOS_XML = table.Column<string>(type: "text", nullable: true),
                    CLASEPRECONDICION = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    METODOPRECONDICION = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    CLASEPOSTCONDICION = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    METODOPOSTCONDICION = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    CLASECOMP = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    METODOCOMP = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    POSTCONDICIONMULTIPLE = table.Column<int>(nullable: false),
                    ENVIAR_CORREO = table.Column<int>(nullable: false),
                    CORREO_AUTOMATICO = table.Column<int>(nullable: false),
                    CORREO_REQUERIDO = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_TAREA_REGION", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID, x.REGION });
                    table.ForeignKey(
                        name: "FK_TAREA_REGION_TAREA",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID },
                        principalTable: "WFL_TAREA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_TAREACOMP",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    TAREAID = table.Column<int>(nullable: false),
                    ID_ROL = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    MODO = table.Column<int>(nullable: true),
                    TODOS = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TAREACOMP", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID, x.ID_ROL });
                    table.ForeignKey(
                        name: "FK_WFL_TAREACOMP_WFL_TAREA",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID },
                        principalTable: "WFL_TAREA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_TAREACONECTOR",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    TAREAID = table.Column<int>(nullable: false),
                    DIAGID_DESTINO = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    FLECHAID_DESTINO = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ID_EXTERNO = table.Column<string>(unicode: false, maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TAREACONECTOR", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID });
                    table.ForeignKey(
                        name: "FK_TAREACONECTOR_TAREA",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID },
                        principalTable: "WFL_TAREA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_TAREAESCUCHA",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    TAREAID = table.Column<int>(nullable: false),
                    FRECUENCIAREVISION = table.Column<string>(unicode: false, maxLength: 2000, nullable: true),
                    ASINCRONO = table.Column<int>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TAREAESCUCHA", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID });
                    table.ForeignKey(
                        name: "FK_TAREAESCUCHA_TAREA",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID },
                        principalTable: "WFL_TAREA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_TAREAGENERAL",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    TAREAID = table.Column<int>(nullable: false),
                    FORMAASIG = table.Column<int>(nullable: false),
                    TIEMPOESPERADO = table.Column<int>(nullable: true),
                    ESFUERZOESPERADO = table.Column<int>(nullable: true),
                    VERARCHIVOS = table.Column<int>(nullable: false),
                    EDITARARCHIVOS = table.Column<int>(nullable: false),
                    EDITAROBS = table.Column<int>(nullable: false, defaultValueSql: "((1))"),
                    PERMITEANULAR = table.Column<int>(nullable: false),
                    EDITARESFREAL = table.Column<int>(nullable: false, defaultValueSql: "((1))"),
                    FORMACOMP = table.Column<int>(nullable: false),
                    PERMITEANULARINICIA = table.Column<int>(nullable: false),
                    COD_GRUPO = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    REQAUTOASIGNAR = table.Column<int>(nullable: false),
                    ESAUTOASIGNABLE = table.Column<int>(nullable: false),
                    VERDOCUMENTOS = table.Column<int>(nullable: false),
                    EDITARDOCUMENTOS = table.Column<int>(nullable: false),
                    CORREO_CONFIG = table.Column<int>(nullable: false),
                    CORREO_CONFIG_XML = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TAREAGENERAL", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID });
                    table.ForeignKey(
                        name: "FK_TAREAGENERAL_TAREA",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.TAREAID },
                        principalTable: "WFL_TAREA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_ATRIBUTOS",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    LLAVEEXPID = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    COPIAEXPID = table.Column<int>(nullable: false),
                    TIPO_DOC = table.Column<string>(unicode: false, maxLength: 24, nullable: true),
                    NUM_DOC = table.Column<int>(nullable: true),
                    ENTIDAD = table.Column<string>(unicode: false, maxLength: 24, nullable: true),
                    MONTO = table.Column<decimal>(type: "decimal(18, 0)", nullable: true),
                    CENTRO_COSTO = table.Column<string>(unicode: false, maxLength: 24, nullable: true),
                    REFERENCIA_1 = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    REFERENCIA_2 = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    REFERENCIA_3 = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    PRIORIDAD = table.Column<int>(nullable: true),
                    REGION = table.Column<string>(unicode: false, maxLength: 16, nullable: true),
                    ATRIBUTOS_XML = table.Column<string>(type: "text", nullable: true),
                    FECHA_1 = table.Column<DateTime>(type: "datetime", nullable: true),
                    FECHA_2 = table.Column<DateTime>(type: "datetime", nullable: true),
                    ASUNTO = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    FEC_PRIORIDAD = table.Column<DateTime>(type: "datetime", nullable: true),
                    PRIORIDAD_NUEVA = table.Column<int>(nullable: true),
                    FEC_SEGUIMIENTO = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    INDICE = table.Column<string>(unicode: false, nullable: true),
                    UID = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    INDICE_COMP = table.Column<string>(unicode: false, maxLength: 2000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_ATIRBUTO", x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID, x.COPIAEXPID });
                    table.ForeignKey(
                        name: "FK_WFL_ATRIB_WFL_COPIA",
                        columns: x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID, x.COPIAEXPID },
                        principalTable: "WFL_COPIAEXP",
                        principalColumns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WFL_COPIAEXP_COMP",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    LLAVEEXPID = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    COPIAEXPID = table.Column<int>(nullable: false),
                    PERSONAID = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    MODO = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_COPIAEXP_COMPARTIDA", x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID, x.COPIAEXPID, x.PERSONAID });
                    table.ForeignKey(
                        name: "FK_WFL_COPIAEXP_COMP_WFL_COPIAEXP1",
                        columns: x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID, x.COPIAEXPID },
                        principalTable: "WFL_COPIAEXP",
                        principalColumns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WFL_COPIAEXP_TEMP",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    LLAVEEXPID = table.Column<string>(unicode: false, maxLength: 64, nullable: false),
                    COPIAEXPID = table.Column<int>(nullable: false),
                    ID_EXTERNO = table.Column<string>(unicode: false, maxLength: 64, nullable: true),
                    ESFUERZOREAL = table.Column<int>(nullable: true),
                    TIEMPOREAL = table.Column<int>(nullable: true),
                    OBSERVACION = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    PRIORIDAD = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_COPIAEXP_TEMP", x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID, x.COPIAEXPID });
                    table.ForeignKey(
                        name: "FK_COPIAEXP_TEMP_COPIAEXP",
                        columns: x => new { x.PROYID, x.VERSIONID, x.LLAVEEXPID, x.COPIAEXPID },
                        principalTable: "WFL_COPIAEXP",
                        principalColumns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WFL_FLECHAALARMA",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    FLECHAID = table.Column<int>(nullable: false),
                    ROLID = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    PLAZOALARMA = table.Column<int>(nullable: false),
                    DIAGID_ALARMADO = table.Column<string>(unicode: false, maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLECHAALARMA", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.FLECHAID });
                    table.ForeignKey(
                        name: "FK_FLECHAALARMA_FLECHA",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.FLECHAID },
                        principalTable: "WFL_FLECHA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "FLECHAID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WFL_FLECHAFLUJO",
                columns: table => new
                {
                    PROYID = table.Column<int>(nullable: false),
                    VERSIONID = table.Column<int>(nullable: false),
                    DIAGID = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    FLECHAID = table.Column<int>(nullable: false),
                    TERMINATAREA = table.Column<int>(nullable: false),
                    FLUJOPORTPOVDO = table.Column<int>(nullable: false),
                    TIPOEXPID = table.Column<string>(unicode: false, maxLength: 16, nullable: false),
                    TIPOEXPINIC = table.Column<int>(nullable: true),
                    ATRIBUTOS_XML = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLECHAFLUJO", x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.FLECHAID });
                    table.ForeignKey(
                        name: "FK_FLECHAFLUJO_FLECHA",
                        columns: x => new { x.PROYID, x.VERSIONID, x.DIAGID, x.FLECHAID },
                        principalTable: "WFL_FLECHA",
                        principalColumns: new[] { "PROYID", "VERSIONID", "DIAGID", "FLECHAID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WFL_UNIDAD_USUARIO",
                columns: table => new
                {
                    ID_UNIDAD_USUARIO = table.Column<Guid>(nullable: false),
                    ID_UNIDAD = table.Column<Guid>(nullable: false),
                    ID_USUARIO = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ID_ADM = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_UNIDAD_USUARIO", x => x.ID_UNIDAD_USUARIO);
                    table.ForeignKey(
                        name: "FK_WFL_UNIDAD_USUARIO_USU_USUARIOS",
                        columns: x => new { x.ID_USUARIO, x.ID_ADM },
                        principalTable: "USU_USUARIOS",
                        principalColumns: new[] { "ID_USUARIO", "ID_ADM" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_UNIDAD_ATR_ENTIDAD",
                columns: table => new
                {
                    ID_UNIDAD_ATR_ENTIDAD = table.Column<Guid>(nullable: false),
                    ID_UNIDAD = table.Column<Guid>(nullable: false),
                    ID_ATR_ENTIDAD = table.Column<Guid>(nullable: false),
                    VALOR = table.Column<string>(unicode: false, maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_UNIDAD_ATR_ENTIDAD_1", x => x.ID_UNIDAD_ATR_ENTIDAD);
                });

            migrationBuilder.CreateTable(
                name: "WFL_ATR_ENTIDAD",
                columns: table => new
                {
                    ID_ATR_ENTIDAD = table.Column<Guid>(nullable: false),
                    COD_ATR_ENTIDAD = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ID_ENTIDAD = table.Column<Guid>(nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CLASE = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    USERCONTROL = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    TIPO = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_ATR_ENTIDAD", x => x.ID_ATR_ENTIDAD);
                });

            migrationBuilder.CreateTable(
                name: "WFL_JERARQUIA",
                columns: table => new
                {
                    ID_JERARQUIA = table.Column<Guid>(nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    USUMULTNODO = table.Column<short>(nullable: false, defaultValueSql: "((1))"),
                    ID_ENTIDAD = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_JERARQUIA", x => x.ID_JERARQUIA);
                });

            migrationBuilder.CreateTable(
                name: "WFL_ENTIDAD",
                columns: table => new
                {
                    ID_ENTIDAD = table.Column<Guid>(nullable: false),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    LEER = table.Column<int>(nullable: true),
                    EDITAR = table.Column<int>(nullable: true),
                    ELIMINAR = table.Column<int>(nullable: true),
                    ASIGNAR = table.Column<int>(nullable: true),
                    COMPARTIR = table.Column<int>(nullable: true),
                    CLASE = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    APLICACION = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    URL = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    ID_JERARQUIA = table.Column<Guid>(nullable: true, defaultValueSql: "('24CDD37B-4CCF-4647-8A38-DD2C39DBD6F9')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_ENTIDAD", x => x.ID_ENTIDAD);
                    table.ForeignKey(
                        name: "FK_WFL_ENTIDAD_WFL_JERARQUIA",
                        column: x => x.ID_JERARQUIA,
                        principalTable: "WFL_JERARQUIA",
                        principalColumn: "ID_JERARQUIA",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WFL_UNIDAD",
                columns: table => new
                {
                    ID_UNIDAD = table.Column<Guid>(nullable: false),
                    ID_PADRE = table.Column<Guid>(nullable: true),
                    ID_JERARQUIA = table.Column<Guid>(nullable: true),
                    NOMBRE = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ORDEN = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    COD_UNIDAD = table.Column<string>(unicode: false, maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WFL_UNIDAD_1", x => x.ID_UNIDAD);
                    table.ForeignKey(
                        name: "FK_WFL_UNIDAD_WFL_JERARQUIA",
                        column: x => x.ID_JERARQUIA,
                        principalTable: "WFL_JERARQUIA",
                        principalColumn: "ID_JERARQUIA",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ARC_ARCHIVO_IdDirectorioPadre",
                table: "ARC_ARCHIVO",
                column: "IdDirectorioPadre");

            migrationBuilder.CreateIndex(
                name: "IX_ARC_DIRECTORIO_IdDirectorioPadre",
                table: "ARC_DIRECTORIO",
                column: "IdDirectorioPadre");

            migrationBuilder.CreateIndex(
                name: "IX_MINIPROFILERCLIENTTIMINGS_ID",
                table: "MINIPROFILERCLIENTTIMINGS",
                column: "ID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MINIPROFILERCLIENTTIMINGS_MINIPROFILERID",
                table: "MINIPROFILERCLIENTTIMINGS",
                column: "MINIPROFILERID");

            migrationBuilder.CreateIndex(
                name: "IX_MINIPROFILERS_ID",
                table: "MINIPROFILERS",
                column: "ID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MINIPROFILERS_USER_HASUSERVIEWED_INCLUDES",
                table: "MINIPROFILERS",
                columns: new[] { "ID", "STARTED", "USER", "HASUSERVIEWED" });

            migrationBuilder.CreateIndex(
                name: "IX_MINIPROFILERTIMINGS_ID",
                table: "MINIPROFILERTIMINGS",
                column: "ID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MINIPROFILERTIMINGS_MINIPROFILERID",
                table: "MINIPROFILERTIMINGS",
                column: "MINIPROFILERID");

            migrationBuilder.CreateIndex(
                name: "PK_PAR_FERIADOS",
                table: "PAR_FERIADOS",
                column: "FEC_FERIADO");

            migrationBuilder.CreateIndex(
                name: "IDX_ACCESOS_FECHA",
                table: "USU_ACCESOS",
                column: "FEC_ACCESO");

            migrationBuilder.CreateIndex(
                name: "IDX_ACCESOS_FUNCIONALIDAD",
                table: "USU_ACCESOS",
                column: "ID_FUNCIONALIDAD");

            migrationBuilder.CreateIndex(
                name: "IDX_ACCESOS_USUARIO",
                table: "USU_ACCESOS",
                column: "ID_USUARIO");

            migrationBuilder.CreateIndex(
                name: "IX_USU_FUNCIONALIDADES_ID_PROCESO_NEGOCIO",
                table: "USU_FUNCIONALIDADES",
                column: "ID_PROCESO_NEGOCIO");

            migrationBuilder.CreateIndex(
                name: "IX_USU_FUNCIONALIDADES_ROLES_ID_ADM_ID_ROL",
                table: "USU_FUNCIONALIDADES_ROLES",
                columns: new[] { "ID_ADM", "ID_ROL" });

            migrationBuilder.CreateIndex(
                name: "IX_USU_HIST_CLAVE_ID_USUARIO_ID_ADM",
                table: "USU_HIST_CLAVE",
                columns: new[] { "ID_USUARIO", "ID_ADM" });

            migrationBuilder.CreateIndex(
                name: "IX_USU_MENUS_FUNCIONALIDADES_ID_FUNCIONALIDAD",
                table: "USU_MENUS_FUNCIONALIDADES",
                column: "ID_FUNCIONALIDAD");

            migrationBuilder.CreateIndex(
                name: "IX_USU_PERFILES_ROLES_ID_ADM_ID_ROL",
                table: "USU_PERFILES_ROLES",
                columns: new[] { "ID_ADM", "ID_ROL" });

            migrationBuilder.CreateIndex(
                name: "IX_USU_RECURSO_RECURSO",
                table: "USU_RECURSO",
                column: "RECURSO");

            migrationBuilder.CreateIndex(
                name: "IX_USU_RECURSO_TIPO",
                table: "USU_RECURSO",
                column: "TIPO");

            migrationBuilder.CreateIndex(
                name: "IX_USU_RECURSO_FUNCIONALIDADES_ID_FUNCIONALIDAD",
                table: "USU_RECURSO_FUNCIONALIDADES",
                column: "ID_FUNCIONALIDAD");

            migrationBuilder.CreateIndex(
                name: "IX_USU_ROLES_ID_PROCESO_NEGOCIO",
                table: "USU_ROLES",
                column: "ID_PROCESO_NEGOCIO");

            migrationBuilder.CreateIndex(
                name: "IX_USU_ROLES_PROC_ID_ADM_ID_ROL",
                table: "USU_ROLES_PROC",
                columns: new[] { "ID_ADM", "ID_ROL" });

            migrationBuilder.CreateIndex(
                name: "IX_USU_USER_CONTROL_FUNCIONALIDAD_ID_FUNCIONALIDAD",
                table: "USU_USER_CONTROL_FUNCIONALIDAD",
                column: "ID_FUNCIONALIDAD");

            migrationBuilder.CreateIndex(
                name: "IDX2_USU_USUARIOS",
                table: "USU_USUARIOS",
                column: "ID_USUARIO");

            migrationBuilder.CreateIndex(
                name: "IDX1_USU_USUARIOS",
                table: "USU_USUARIOS",
                columns: new[] { "ID_USUARIO", "NOMBRE" });

            migrationBuilder.CreateIndex(
                name: "IX_USU_USUARIOS_PERFILES_ID_ADM_ID_PERFIL",
                table: "USU_USUARIOS_PERFILES",
                columns: new[] { "ID_ADM", "ID_PERFIL" });

            migrationBuilder.CreateIndex(
                name: "IX_USU_USUARIOS_PERFILES_ID_USUARIO_ID_ADM",
                table: "USU_USUARIOS_PERFILES",
                columns: new[] { "ID_USUARIO", "ID_ADM" });

            migrationBuilder.CreateIndex(
                name: "IX_USU_USUARIOS_ROLES_ID_ADM_ID_ROL",
                table: "USU_USUARIOS_ROLES",
                columns: new[] { "ID_ADM", "ID_ROL" });

            migrationBuilder.CreateIndex(
                name: "IDX1_ALARMA",
                table: "WFL_ALARMA",
                column: "ACTIVADA");

            migrationBuilder.CreateIndex(
                name: "IDX3_ALARMA",
                table: "WFL_ALARMA",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID" });

            migrationBuilder.CreateIndex(
                name: "IDX2_ALARMA",
                table: "WFL_ALARMA",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "FLECHAID" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_ATR_ENTIDAD_ID_ENTIDAD",
                table: "WFL_ATR_ENTIDAD",
                column: "ID_ENTIDAD");

            migrationBuilder.CreateIndex(
                name: "IDX_UID_WFL_ATRIBUTOS",
                table: "WFL_ATRIBUTOS",
                column: "UID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IDX_ATRIBUTO_REFERENCIA_2",
                table: "WFL_ATRIBUTOS",
                columns: new[] { "PROYID", "REFERENCIA_1" });

            migrationBuilder.CreateIndex(
                name: "IDX_ATRIBUTO_REFERENCIA_3",
                table: "WFL_ATRIBUTOS",
                columns: new[] { "PROYID", "REFERENCIA_3" });

            migrationBuilder.CreateIndex(
                name: "IDX3_WFL_ATRIBUTOS",
                table: "WFL_ATRIBUTOS",
                columns: new[] { "VERSIONID", "LLAVEEXPID" });

            migrationBuilder.CreateIndex(
                name: "WFL_ATRIBUTOS_idx020",
                table: "WFL_ATRIBUTOS",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IDX4_WFL_ATRIBUTOS",
                table: "WFL_ATRIBUTOS",
                columns: new[] { "ASUNTO", "LLAVEEXPID", "PROYID", "VERSIONID", "COPIAEXPID" });

            migrationBuilder.CreateIndex(
                name: "WFL_ATRIBUTOS_idx021",
                table: "WFL_ATRIBUTOS",
                columns: new[] { "ASUNTO", "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_BUSQUEDA_FILTRO_ID_BUSQUEDA",
                table: "WFL_BUSQUEDA_FILTRO",
                column: "ID_BUSQUEDA");

            migrationBuilder.CreateIndex(
                name: "IDX1_COPIAEXP",
                table: "WFL_COPIAEXP",
                column: "ASIGNADA");

            migrationBuilder.CreateIndex(
                name: "IDX5_COPIAEXP",
                table: "WFL_COPIAEXP",
                column: "FECHA");

            migrationBuilder.CreateIndex(
                name: "IDX3_COPIAEXP",
                table: "WFL_COPIAEXP",
                column: "PENDPORDESP");

            migrationBuilder.CreateIndex(
                name: "IDX6_COPIAEXP",
                table: "WFL_COPIAEXP",
                column: "PERSONAID");

            migrationBuilder.CreateIndex(
                name: "IDX2_COPIAEXP",
                table: "WFL_COPIAEXP",
                columns: new[] { "ESDESPACHABLE", "PENDPORDESP" });

            migrationBuilder.CreateIndex(
                name: "IDX8_WFL_COPIAEXP",
                table: "WFL_COPIAEXP",
                columns: new[] { "PERSONAID", "DIAGID", "PROYID" });

            migrationBuilder.CreateIndex(
                name: "IDX10_WFL_COPIAEXP",
                table: "WFL_COPIAEXP",
                columns: new[] { "VERSIONID", "LLAVEEXPID", "PROYID" });

            migrationBuilder.CreateIndex(
                name: "IDX7_COPIAEXP",
                table: "WFL_COPIAEXP",
                columns: new[] { "PERSONAID", "ESDESPACHABLE", "ASIGNADA", "PROYID" });

            migrationBuilder.CreateIndex(
                name: "IDX2_WFL_COPIAEXP",
                table: "WFL_COPIAEXP",
                columns: new[] { "PERSONAID", "VERSIONID", "DIAGID", "PROYID" });

            migrationBuilder.CreateIndex(
                name: "IDX6_WFL_COPIAEXP",
                table: "WFL_COPIAEXP",
                columns: new[] { "PERSONAID", "VERSIONID", "DIAGID", "TAREAID" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_COPIAEXP_PROYID_VERSIONID_DIAGID_TAREAID",
                table: "WFL_COPIAEXP",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID" });

            migrationBuilder.CreateIndex(
                name: "IDX1_WFL_COPIAEXP",
                table: "WFL_COPIAEXP",
                columns: new[] { "VERSIONID", "LLAVEEXPID", "PERSONAID", "DIAGID", "PROYID" });

            migrationBuilder.CreateIndex(
                name: "IDX5_WFL_COPIAEXP",
                table: "WFL_COPIAEXP",
                columns: new[] { "PERSONAID", "TAREAID", "PROYID", "VERSIONID", "DIAGID", "LLAVEEXPID" });

            migrationBuilder.CreateIndex(
                name: "WFL_COPIAEXP_idx020",
                table: "WFL_COPIAEXP",
                columns: new[] { "DIAGID", "TAREAID", "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID", "PERSONAID" });

            migrationBuilder.CreateIndex(
                name: "WFL_COPIAEXP_idx021",
                table: "WFL_COPIAEXP",
                columns: new[] { "COPIAEXPID", "DIAGID", "TAREAID", "ASIGNADA", "ESDESPACHABLE", "FECHA", "DESCRIPCION", "FECHAVENC", "ESFUERZO_ESPERADO", "TIEMPO_ESPERADO", "PROYID", "VERSIONID", "LLAVEEXPID", "PERSONAID" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_COPIASPORPERSONA_PROYID_VERSIONID",
                table: "WFL_COPIASPORPERSONA",
                columns: new[] { "PROYID", "VERSIONID" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_ENTIDAD_ID_JERARQUIA",
                table: "WFL_ENTIDAD",
                column: "ID_JERARQUIA");

            migrationBuilder.CreateIndex(
                name: "IDX_ESCUCHA_PROXIMAESCUCHA",
                table: "WFL_ESCUCHA",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID", "PROXIMAESCUCHA" });

            migrationBuilder.CreateIndex(
                name: "IDX1_EXP",
                table: "WFL_EXP",
                column: "FECHAINICIO");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_EXP_TIPOEXPID",
                table: "WFL_EXP",
                column: "TIPOEXPID");

            migrationBuilder.CreateIndex(
                name: "IDX2_EXP",
                table: "WFL_EXP",
                columns: new[] { "PROYID", "LLAVEEXPID" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IDX2_WFL_EXP",
                table: "WFL_EXP",
                columns: new[] { "LLAVEEXPID", "PERSONAID", "PROYID" });

            migrationBuilder.CreateIndex(
                name: "IDX1_WFL_EXP",
                table: "WFL_EXP",
                columns: new[] { "FECHAINICIO", "FECHAFIN", "PERSONAID", "PROYID" });

            migrationBuilder.CreateIndex(
                name: "WFL_EXP_idx020",
                table: "WFL_EXP",
                columns: new[] { "LLAVEEXPID", "PROYID", "VERSIONID", "PERSONAID" });

            migrationBuilder.CreateIndex(
                name: "WFL_EXP_idx021",
                table: "WFL_EXP",
                columns: new[] { "FECHAINICIO", "FECHAFIN", "PROYID", "VERSIONID", "LLAVEEXPID", "PERSONAID" });

            migrationBuilder.CreateIndex(
                name: "IDX1_FLECHA",
                table: "WFL_FLECHA",
                column: "NOMBRE");

            migrationBuilder.CreateIndex(
                name: "IDX2_FLECHA",
                table: "WFL_FLECHA",
                column: "TIPO");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_FLECHA_PROYID_VERSIONID_DIAGID_TAREAID_ORIGEN",
                table: "WFL_FLECHA",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID_ORIGEN" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_FLECHA_PROYID_VERSIONID_DIAGID_DESTINO_TAREAID_DESTINO",
                table: "WFL_FLECHA",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID_DESTINO", "TAREAID_DESTINO" });

            migrationBuilder.CreateIndex(
                name: "IDX1_FLECHAFLUJO",
                table: "WFL_FLECHAFLUJO",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID", "FLECHAID", "TIPOEXPINIC" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_GRUPO_COD_GRUPO",
                table: "WFL_GRUPO",
                column: "COD_GRUPO",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WFL_GRUPO",
                table: "WFL_GRUPO",
                column: "ID_GRUPO",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WFL_GRUPO_PROCESO_PROCESOID",
                table: "WFL_GRUPO_PROCESO",
                column: "PROCESOID");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_GRUPO_PROCESO",
                table: "WFL_GRUPO_PROCESO",
                columns: new[] { "ID_GRUPO", "PROCESOID" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WFL_GRUPO_REGION_ID_REGION",
                table: "WFL_GRUPO_REGION",
                column: "ID_REGION");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_GRUPO_REGION",
                table: "WFL_GRUPO_REGION",
                columns: new[] { "ID_GRUPO", "ID_REGION" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WFL_GRUPO_USUARIO_ID_USUARIO_ID_ADM",
                table: "WFL_GRUPO_USUARIO",
                columns: new[] { "ID_USUARIO", "ID_ADM" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_GRUPO_USUARIO",
                table: "WFL_GRUPO_USUARIO",
                columns: new[] { "ID_GRUPO", "ID_ADM", "ID_USUARIO" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WFL_JERARQUIA_ID_ENTIDAD",
                table: "WFL_JERARQUIA",
                column: "ID_ENTIDAD");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_JERARQUIA_NOMBRE",
                table: "WFL_JERARQUIA",
                column: "NOMBRE",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "WFL_PROC_idx02",
                table: "WFL_PROC",
                columns: new[] { "PROYID", "PROCESOID" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_PROC_PROYID_VERSIONID_DIAGID",
                table: "WFL_PROC",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_REGISTRO_05",
                table: "WFL_REGISTRO",
                column: "ID_EXTERNO");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_REGISTRO_07",
                table: "WFL_REGISTRO",
                column: "TIPO");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_REGISTRO_04",
                table: "WFL_REGISTRO",
                columns: new[] { "TIPO", "FECHA" });

            migrationBuilder.CreateIndex(
                name: "IDX4_WFL_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID" });

            migrationBuilder.CreateIndex(
                name: "IDX5_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "PROYID", "VERSIONID", "FECHA" });

            migrationBuilder.CreateIndex(
                name: "IDX2_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID" });

            migrationBuilder.CreateIndex(
                name: "IDX1_WFL_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "LLAVEEXPID", "PROYID", "REGISTROID", "VERSIONID" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_REGISTRO_02",
                table: "WFL_REGISTRO",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID" });

            migrationBuilder.CreateIndex(
                name: "IDX1_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID_PADRE" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_REGISTRO_03",
                table: "WFL_REGISTRO",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID", "FECHA" });

            migrationBuilder.CreateIndex(
                name: "IDX4_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "PERSONAID_DESTINO", "ROLID_DESTINO" });

            migrationBuilder.CreateIndex(
                name: "IDX5_WFL_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "PERSONAID_ORIGEN", "REGISTROID" });

            migrationBuilder.CreateIndex(
                name: "IDX3_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "PROYID", "VERSIONID", "LLAVEEXPID", "PERSONAID_ORIGEN", "ROLID_ORIGEN" });

            migrationBuilder.CreateIndex(
                name: "IDX3_WFL_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "COPIAEXPID", "DIAGID", "FECHA", "LLAVEEXPID", "PROYID", "VERSIONID", "TAREAID_ORIGEN" });

            migrationBuilder.CreateIndex(
                name: "IDX2_WFL_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "COPIAEXPID", "DIAGID", "FECHA", "LLAVEEXPID", "PROYID", "REGISTROID", "VERSIONID", "TAREAID_ORIGEN" });

            migrationBuilder.CreateIndex(
                name: "IDX6_WFL_REGISTRO",
                table: "WFL_REGISTRO",
                columns: new[] { "DIAGID", "TIPO", "PROYID", "VERSIONID", "COPIAEXPID", "FECHA", "LLAVEEXPID", "TAREAID_ORIGEN" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_REGISTRO_06",
                table: "WFL_REGISTRO",
                columns: new[] { "DIAGID", "LLAVEEXPID", "COPIAEXPID", "PERSONAID_ORIGEN", "TAREAID_ORIGEN", "ASUNTO", "PROYID", "VERSIONID", "REGISTROID" });

            migrationBuilder.CreateIndex(
                name: "WFL_REGISTRO_idx020",
                table: "WFL_REGISTRO",
                columns: new[] { "REGISTROID", "TIPO", "PERSONAID_ORIGEN", "TIPO_DOC", "NUM_DOC", "ENTIDAD", "MONTO", "CENTRO_COSTO", "REFERENCIA_1", "REFERENCIA_2", "REFERENCIA_3", "PRIORIDAD", "FECHA_1", "FECHA_2", "ESFUERZO_ESPERADO", "TIEMPO_ESPERADO", "ASUNTO", "PROYID", "VERSIONID", "LLAVEEXPID", "COPIAEXPID", "DIAGID", "TAREAID_ORIGEN", "FECHA" });

            migrationBuilder.CreateIndex(
                name: "IDX2_TAREA",
                table: "WFL_TAREA",
                columns: new[] { "PROYID", "VERSIONID", "TIPO" });

            migrationBuilder.CreateIndex(
                name: "IDX1_WFL_TAREA",
                table: "WFL_TAREA",
                columns: new[] { "TAREAID", "PROYID", "VERSIONID" });

            migrationBuilder.CreateIndex(
                name: "IDX2_WFL_TAREA",
                table: "WFL_TAREA",
                columns: new[] { "VERSIONID", "DIAGID", "TAREAID" });

            migrationBuilder.CreateIndex(
                name: "WFL_TAREA_idx010",
                table: "WFL_TAREA",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID", "TIPO" });

            migrationBuilder.CreateIndex(
                name: "IDX3_TAREA",
                table: "WFL_TAREA",
                columns: new[] { "NOMBRE", "PROYID", "VERSIONID", "DIAGID", "TAREAID", "PROCESONEGOCIO", "MODULO" });

            migrationBuilder.CreateIndex(
                name: "IDX_TAREA_REGION_CLASEESCUCHA",
                table: "WFL_TAREA_REGION",
                column: "CLASEESCUCHA");

            migrationBuilder.CreateIndex(
                name: "IDX_TAREA_REGION_POSTCONDICIONMULTIPLE",
                table: "WFL_TAREA_REGION",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID", "TAREAID", "REGION", "POSTCONDICIONMULTIPLE" });

            migrationBuilder.CreateIndex(
                name: "IDX1_TAREACONECTOR",
                table: "WFL_TAREACONECTOR",
                columns: new[] { "PROYID", "VERSIONID", "DIAGID", "FLECHAID_DESTINO" });

            migrationBuilder.CreateIndex(
                name: "IX_WFL_UNIDAD_ID_JERARQUIA",
                table: "WFL_UNIDAD",
                column: "ID_JERARQUIA");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_UNIDAD_ATR_ENTIDAD_ID_ATR_ENTIDAD",
                table: "WFL_UNIDAD_ATR_ENTIDAD",
                column: "ID_ATR_ENTIDAD");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_UNIDAD_ATR_ENTIDAD_ID_UNIDAD",
                table: "WFL_UNIDAD_ATR_ENTIDAD",
                column: "ID_UNIDAD");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_UNIDAD_USUARIO_ID_UNIDAD",
                table: "WFL_UNIDAD_USUARIO",
                column: "ID_UNIDAD");

            migrationBuilder.CreateIndex(
                name: "IX_WFL_UNIDAD_USUARIO_ID_USUARIO_ID_ADM",
                table: "WFL_UNIDAD_USUARIO",
                columns: new[] { "ID_USUARIO", "ID_ADM" });

            migrationBuilder.AddForeignKey(
                name: "FK_WFL_UNIDAD_USUARIO_WFL_UNIDAD",
                table: "WFL_UNIDAD_USUARIO",
                column: "ID_UNIDAD",
                principalTable: "WFL_UNIDAD",
                principalColumn: "ID_UNIDAD",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WFL_UNIDAD_ATR_ENTIDAD_WFL_ATR_ENTIDAD",
                table: "WFL_UNIDAD_ATR_ENTIDAD",
                column: "ID_ATR_ENTIDAD",
                principalTable: "WFL_ATR_ENTIDAD",
                principalColumn: "ID_ATR_ENTIDAD",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WFL_UNIDAD_ATR_ENTIDAD_WFL_UNIDAD",
                table: "WFL_UNIDAD_ATR_ENTIDAD",
                column: "ID_UNIDAD",
                principalTable: "WFL_UNIDAD",
                principalColumn: "ID_UNIDAD",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WFL_ATR_ENTIDAD_WFL_ENTIDAD",
                table: "WFL_ATR_ENTIDAD",
                column: "ID_ENTIDAD",
                principalTable: "WFL_ENTIDAD",
                principalColumn: "ID_ENTIDAD",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WFL_JERARQUIA_ENTIDAD",
                table: "WFL_JERARQUIA",
                column: "ID_ENTIDAD",
                principalTable: "WFL_ENTIDAD",
                principalColumn: "ID_ENTIDAD",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WFL_JERARQUIA_ENTIDAD",
                table: "WFL_JERARQUIA");

            migrationBuilder.DropTable(
                name: "ARC_ARCHIVO");

            migrationBuilder.DropTable(
                name: "MINIPROFILERCLIENTTIMINGS");

            migrationBuilder.DropTable(
                name: "MINIPROFILERS");

            migrationBuilder.DropTable(
                name: "MINIPROFILERTIMINGS");

            migrationBuilder.DropTable(
                name: "PAR_AGENCIAS");

            migrationBuilder.DropTable(
                name: "PAR_AREAS");

            migrationBuilder.DropTable(
                name: "PAR_COMUNAS");

            migrationBuilder.DropTable(
                name: "PAR_DATOS_CLAVE");

            migrationBuilder.DropTable(
                name: "PAR_FERIADOS");

            migrationBuilder.DropTable(
                name: "PAR_GENERAL");

            migrationBuilder.DropTable(
                name: "PAR_TIPOS_CARGOS");

            migrationBuilder.DropTable(
                name: "USU_ACCESOS");

            migrationBuilder.DropTable(
                name: "USU_FUNCIONALIDADES_ROLES");

            migrationBuilder.DropTable(
                name: "USU_HIST_CLAVE");

            migrationBuilder.DropTable(
                name: "USU_MENUS_FUNCIONALIDADES");

            migrationBuilder.DropTable(
                name: "USU_PERFILES_ROLES");

            migrationBuilder.DropTable(
                name: "USU_RECURSO_FUNCIONALIDADES");

            migrationBuilder.DropTable(
                name: "USU_ROLES_PROC");

            migrationBuilder.DropTable(
                name: "USU_SESIONES");

            migrationBuilder.DropTable(
                name: "USU_USER_CONTROL_FUNCIONALIDAD");

            migrationBuilder.DropTable(
                name: "USU_USUARIOS_PERFILES");

            migrationBuilder.DropTable(
                name: "USU_USUARIOS_ROLES");

            migrationBuilder.DropTable(
                name: "WFL_ALARMA");

            migrationBuilder.DropTable(
                name: "WFL_ALMACEN_PWD");

            migrationBuilder.DropTable(
                name: "WFL_APLICACION");

            migrationBuilder.DropTable(
                name: "WFL_ATRIBUTOS");

            migrationBuilder.DropTable(
                name: "WFL_BUSQUEDA_FILTRO");

            migrationBuilder.DropTable(
                name: "WFL_COPIAEXP_COMP");

            migrationBuilder.DropTable(
                name: "WFL_COPIAEXP_LOCK");

            migrationBuilder.DropTable(
                name: "WFL_COPIAEXP_TEMP");

            migrationBuilder.DropTable(
                name: "WFL_COPIASPORPERSONA");

            migrationBuilder.DropTable(
                name: "WFL_ESCUCHA");

            migrationBuilder.DropTable(
                name: "WFL_FLECHAALARMA");

            migrationBuilder.DropTable(
                name: "WFL_FLECHAFLUJO");

            migrationBuilder.DropTable(
                name: "WFL_GRUPO_PROCESO");

            migrationBuilder.DropTable(
                name: "WFL_GRUPO_REGION");

            migrationBuilder.DropTable(
                name: "WFL_GRUPO_USUARIO");

            migrationBuilder.DropTable(
                name: "WFL_MENSAJE");

            migrationBuilder.DropTable(
                name: "WFL_OPCION");

            migrationBuilder.DropTable(
                name: "WFL_OPCION_USUARIO");

            migrationBuilder.DropTable(
                name: "WFL_PARAMETROGENERAL");

            migrationBuilder.DropTable(
                name: "WFL_PLANTILLA");

            migrationBuilder.DropTable(
                name: "WFL_REGISTRO");

            migrationBuilder.DropTable(
                name: "WFL_SUBPROCESO_A_PUBLICAR_EXT");

            migrationBuilder.DropTable(
                name: "WFL_TAREA_REGION");

            migrationBuilder.DropTable(
                name: "WFL_TAREACOMP");

            migrationBuilder.DropTable(
                name: "WFL_TAREACONECTOR");

            migrationBuilder.DropTable(
                name: "WFL_TAREAESCUCHA");

            migrationBuilder.DropTable(
                name: "WFL_TAREAGENERAL");

            migrationBuilder.DropTable(
                name: "WFL_TESTESCUCHA");

            migrationBuilder.DropTable(
                name: "WFL_UNIDAD_ATR_ENTIDAD");

            migrationBuilder.DropTable(
                name: "WFL_UNIDAD_ROL");

            migrationBuilder.DropTable(
                name: "WFL_UNIDAD_USUARIO");

            migrationBuilder.DropTable(
                name: "ARC_DIRECTORIO");

            migrationBuilder.DropTable(
                name: "USU_MENUS");

            migrationBuilder.DropTable(
                name: "USU_RECURSO");

            migrationBuilder.DropTable(
                name: "USU_FUNCIONALIDADES");

            migrationBuilder.DropTable(
                name: "USU_USER_CONTROL");

            migrationBuilder.DropTable(
                name: "USU_PERFILES");

            migrationBuilder.DropTable(
                name: "USU_ROLES");

            migrationBuilder.DropTable(
                name: "WFL_BUSQUEDA");

            migrationBuilder.DropTable(
                name: "WFL_COPIAEXP");

            migrationBuilder.DropTable(
                name: "WFL_FLECHA");

            migrationBuilder.DropTable(
                name: "WFL_PROC");

            migrationBuilder.DropTable(
                name: "WFL_REGION");

            migrationBuilder.DropTable(
                name: "WFL_GRUPO");

            migrationBuilder.DropTable(
                name: "WFL_SUBPROCESO_A_PUBLICAR");

            migrationBuilder.DropTable(
                name: "WFL_ATR_ENTIDAD");

            migrationBuilder.DropTable(
                name: "WFL_UNIDAD");

            migrationBuilder.DropTable(
                name: "USU_USUARIOS");

            migrationBuilder.DropTable(
                name: "USU_PROCESOSNEGOCIO");

            migrationBuilder.DropTable(
                name: "WFL_EXP");

            migrationBuilder.DropTable(
                name: "WFL_TAREA");

            migrationBuilder.DropTable(
                name: "WFL_TIPOEXP");

            migrationBuilder.DropTable(
                name: "WFL_DIAG");

            migrationBuilder.DropTable(
                name: "WFL_PROY");

            migrationBuilder.DropTable(
                name: "WFL_ENTIDAD");

            migrationBuilder.DropTable(
                name: "WFL_JERARQUIA");
        }
    }
}
