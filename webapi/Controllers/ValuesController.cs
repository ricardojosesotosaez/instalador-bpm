﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using webapi.Models;

namespace webapi.Controllers
{
    [EnableCors("AllowCors"), Route("v1/")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        private readonly Services.IdatabaseServices _services;        
        private ILogger _logger;
        Dictionary<string, string> connStrs = new Dictionary<string, string>();


        public ValuesController(Services.IdatabaseServices services, ILogger<ValuesController> logger)
        {
            _services = services;
            _logger = logger;            
        }

        // POST api/values
        [HttpPost]
        [Route("ValidarDatabase")]
        public IActionResult ValidarDatabase([FromBody] InfoDatabase items)
        {
            try
            {
                if (items == null) 
                {
                    return BadRequest("Los items vienen nulos");
                }

                //var databaseItems = _services.databaseItems(items);

                string connectionString = "Data Source=" + items.hostname + ";initial catalog=" + items.database + ";Persist Security Info=False;User ID=" + items.username + ";Password=" + items.password + ";MultipleActiveResultSets=False";

             
                //TODO: el código que sigue se debe pasar a una iterfaz de servicio, ya que no corresponde que vaya esta lógica aquí     
                //TODO: se debe eliminar la tabla luego que se valide la conexión 

                using (SqlConnection connection = new SqlConnection(connectionString))
                {

                    string query = $"CREATE TABLE DUMMY(First_Name char(50),Last_Name char(50),Address char(50)) ";
                    string deleteTable = "DROP TABLE DUMMY";

                    connection.Open();
                    
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.ExecuteNonQuery();
                    }

                    using (SqlCommand command = new SqlCommand(deleteTable, connection))
                    {
                        command.ExecuteNonQuery();
                    }

                    connection.Close();                    
                }

                //Almacena la cadena de conexión de base de datos en un diccionario

                connStrs.Add("CONNSTR1", connectionString);
                DatabaseContext.SetConnectionString(connStrs);

                return Ok(200);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Algún error cuando se intentó conectar a la base de datos: {ex}");
                if (ex.Message.Contains("Login failed")) {
                    return StatusCode(401, "No autorizado");
                }
                else
                {
                    return StatusCode(500, "Internal server error");
                }                               
            }
          
        }

        [HttpGet]
        [Route("InstalarDatabase")]
        public IActionResult Get()
        {

            var context = DatabaseContext.Create("CONNSTR1");

            try
            {                
                //borra y establece la base de datos 
                context.Database.EnsureDeleted();

                //Crea la base de datos si no existe
                context.Database.EnsureDeleted();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Algún error en método InstalarDatabase : {ex}");
                return StatusCode(500, "Internal server error");                      
            }

            finally
            {
                context.Dispose();                
            }

            return Ok(200);
        }

    }


}

