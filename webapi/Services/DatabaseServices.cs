﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Models;

namespace webapi.Services
{
    public class DatabaseServices : IdatabaseServices
    {
        private readonly Dictionary<string, InfoDatabase> _infodatabase;

        public DatabaseServices()
        {
            _infodatabase = new Dictionary<string, InfoDatabase>();
        }

        public InfoDatabase databaseItems(InfoDatabase items)
        {

            if (!_infodatabase.ContainsValue(items))
            {
                _infodatabase.Clear();
                _infodatabase.Add("1", items);                
            }

            

            return items;
        }
    }
}
